package services;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;
import domain.Configuration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class ConfigurationServiceTest extends AbstractTest{

	@Autowired
	private ConfigurationService configurationService;
	
	
	/* 17. An actor who is authenticated as an administrator must be able to:
			1. Manage a list of taboo words.*/
	@Test
	public void driverEdit() {
		Object testingData[][] = {
			//Positivo
			{	"admin","sex","cialis","newspaper",null} ,
			
			//Negativo
			{ "user1","sex","cialis","newspaper",IllegalArgumentException.class} ,
			{ "customer1","sex","cialis","newspaper",IllegalArgumentException.class} 

		};

		for (int i = 0; i < testingData.length; i++) {
			templateEdit((String) testingData[i][0],
					(String) testingData[i][1], (String) testingData[i][2],
					(String) testingData[i][3], (Class<?>) testingData[i][4]);
		}
	}

	protected void templateEdit(String username, String string1,
			String string2, String string3,
			Class<?> expected) {
		Class<?> caught;
		Configuration config;
		List<String> taboo;
		caught = null;
		taboo = new ArrayList<String>();

		
		try {
			this.startTransaction();
			
			super.authenticate(username);
			taboo.add(string1);
			taboo.add(string2);
			taboo.add(string3);
			config= configurationService.findConfiguration();
			config.setTabooWords(taboo);

			configurationService.save(config);
			configurationService.flush();

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		} finally{
			this.rollbackTransaction();
		}

		checkExceptions(expected, caught);
	}

}
