package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;

import com.mchange.v1.util.UnexpectedException;

import domain.Newspaper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class AdministratorServiceTest extends AbstractTest{

	@Autowired
	private AdministratorService administratorService;
	
	@Autowired
	private NewspaperService newspaperService;
	
/* ACME NEWSPAPER	
  7. An actor who is authenticated as an administrator must be able to:
	  3. Display a dashboard with the following information:
			1. The average and the standard deviation of newspapers created per user*/
	@Test
	public void testAvgSdNewspaperPerUser(){
		testAvgSdNewspaperPerUser("admin",null);
		testAvgSdNewspaperPerUser(null,IllegalArgumentException.class); 		//	El usuario no est� logueado
		testAvgSdNewspaperPerUser("user1",IllegalArgumentException.class);		// 	El usuario es un user
		testAvgSdNewspaperPerUser("customer1",IllegalArgumentException.class); 	//	El usuario es un customer
	}
	
/*		2.The average and the standard deviation of articles written by writer..  		
*/
	@Test
	public void testAvgSdArticlesPerWriter(){
		testAvgSdArticlesPerWriter("admin",null);
		testAvgSdArticlesPerWriter(null,IllegalArgumentException.class);			//	El usuario no est� logueado
		testAvgSdArticlesPerWriter("user1",IllegalArgumentException.class);			// 	El usuario es un user
		testAvgSdArticlesPerWriter("customer1",IllegalArgumentException.class); 		//	El usuario es un customer
	}
	
/*		3. The average and the standard deviation of articles per newspaper.	
*/
	@Test
	public void avgSdArticlesPerNewspaper(){
		testAvgSdArticlesPerNewspaper("admin",null);
		testAvgSdArticlesPerNewspaper(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		testAvgSdArticlesPerNewspaper("user1",IllegalArgumentException.class);				// 	El usuario es un user
		testAvgSdArticlesPerNewspaper("customer1",IllegalArgumentException.class); 			//	El usuario es un customer
	}
	
/*		4. The newspapers that have at least 10% more articles than the average.		
*/
	@Test
	public void getNewspaperWith10PercentMoreArticlesThanAvg(){
		getNewspaperWith10PercentMoreArticlesThanAvg("admin",null);
		getNewspaperWith10PercentMoreArticlesThanAvg(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		getNewspaperWith10PercentMoreArticlesThanAvg("user1",IllegalArgumentException.class);				// 	El usuario es un user
		getNewspaperWith10PercentMoreArticlesThanAvg("customer1",IllegalArgumentException.class); 			//	El usuario es un customer
	}

/*		5. The newspapers that have at least 10% more articles than the average.		
*/
	@Test
	public void testNewspaperWith10PercentLessArticlesThanAvg(){
		getNewspaperWith10PercentLessArticlesThanAvg("admin",null);
		getNewspaperWith10PercentMoreArticlesThanAvg(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		getNewspaperWith10PercentMoreArticlesThanAvg("user1",IllegalArgumentException.class);				// 	El usuario es un user
		getNewspaperWith10PercentMoreArticlesThanAvg("customer1",IllegalArgumentException.class); 			//	El usuario es un customer
	}
		
/*		6. The ratio of users who have ever created a newspaper.	
*/
	@Test
	public void ratioOfCreators(){
		ratioOfCreators("admin",null);
		ratioOfCreators(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		ratioOfCreators("user1",IllegalArgumentException.class);			// 	El usuario es un user
		ratioOfCreators("customer1",IllegalArgumentException.class); 		//	El usuario es un customer
	}
	
/*		7. The ratio of users who have ever written an article.	
*/
	@Test
	public void ratioOfWriters(){
		ratioOfWriters("admin",null);
		ratioOfWriters(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		ratioOfWriters("user1",IllegalArgumentException.class);				// 	El usuario es un user
		ratioOfWriters("customer1",IllegalArgumentException.class); 			//	El usuario es un customer
	}
		
/*	17. An actor who is authenticated as an administrator must be able to: 	
		6. Display a dashboard with the following information:
 			1. The average number of follow-ups per article.		
*/
@Test
public void avgFollowUpsPerArticle(){
	avgFollowUpsPerArticle("admin",null);
	avgFollowUpsPerArticle(null,IllegalArgumentException.class);			//	El usuario no est� logueado
	avgFollowUpsPerArticle("user1",IllegalArgumentException.class);			// 	El usuario es un user
	avgFollowUpsPerArticle("customer1",IllegalArgumentException.class); 		//	El usuario es un customer
}	
	
/*			2. The average number of follow-ups per article up to one week after the corresponding newspaper's been published.	
*/	
	@Test
	public void avgFollowupsPerArticlePlus1Week(){
		testAvgFollowupsPerArticlePlus1Week("admin",null);
		testAvgFollowupsPerArticlePlus1Week(null,IllegalArgumentException.class);			//	El usuario no est� logueado
		testAvgFollowupsPerArticlePlus1Week("user1",IllegalArgumentException.class);			// 	El usuario es un user
		testAvgFollowupsPerArticlePlus1Week("customer1",IllegalArgumentException.class); 		//	El usuario es un customer
	}

/*			3. The average number of follow-ups per article up to two weeks after the corresponding newspaper's been published.	
*/	
	@Test
	public void avgFollowupsPerArticlePlus2Weeks(){
		testAvgFollowupsPerArticlePlus2Weeks("admin",null);
		testAvgFollowupsPerArticlePlus2Weeks(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		testAvgFollowupsPerArticlePlus2Weeks("user1",IllegalArgumentException.class);			// 	El usuario es un user
		testAvgFollowupsPerArticlePlus2Weeks("customer1",IllegalArgumentException.class); 		//	El usuario es un customer
	}

/*			4. The average and the standard deviation of the number of chirps per user.	
*/	
	@Test
	public void avgSdChirpsPerUser(){
		testAvgSdChirpsPerUser("admin",null);
		testAvgSdChirpsPerUser(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		testAvgSdChirpsPerUser("user1",IllegalArgumentException.class);				// 	El usuario es un user
		testAvgSdChirpsPerUser("customer1",IllegalArgumentException.class); 			//	El usuario es un customer
	}
	
/*			5. The ratio of users who have posted above 75% the average number of chirps per user.
*/
	@Test
	public void ratioUsersOver75Chirps(){
		testRatioUsersOver75("admin",null);
		testRatioUsersOver75(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		testRatioUsersOver75("user1",IllegalArgumentException.class);				// 	El usuario es un user
		testRatioUsersOver75("customer1",IllegalArgumentException.class); 			//	El usuario es un customer
	}
	
/*	24. An actor who is authenticated as an administrator must be able to:
		1. Display a dashboard with the following information:
			1. The ratio of public versus private newspapers.
*/
	@Test
	public void ratioPublicOverPrivateNewspapers(){
		ratioPublicOverPrivateNewspapers("admin",null);
		ratioPublicOverPrivateNewspapers(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		ratioPublicOverPrivateNewspapers("user1",IllegalArgumentException.class);				// 	El usuario es un user
		ratioPublicOverPrivateNewspapers("customer1",IllegalArgumentException.class); 			//	El usuario es un customer
	}

/*			2. The average number of articles per private newspapers.
*/
	@Test
	public void avgArticlesPerPrivateNewspaper(){
		avgArticlesPerPrivateNewspaper("admin",null);
		avgArticlesPerPrivateNewspaper(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		avgArticlesPerPrivateNewspaper("user1",IllegalArgumentException.class);					// 	El usuario es un user
		avgArticlesPerPrivateNewspaper("customer1",IllegalArgumentException.class); 				//	El usuario es un customer
	}
	
/*			3. The average number of articles per private newspapers.
*/
	@Test
	public void avgArticlesPerPublicNewspaper(){
		avgArticlesPerPublicNewspaper("admin",null);
		avgArticlesPerPublicNewspaper(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		avgArticlesPerPublicNewspaper("user1",IllegalArgumentException.class);				// 	El usuario es un user
		avgArticlesPerPublicNewspaper("customer1",IllegalArgumentException.class); 			//	El usuario es un customer
	}
	
/*The ratio of subscribers per private newspaper versus the total number of customers.*/
	@Test
	public void ratioSubscriberPerPrivateNewspaper(){
		ratioSubscriberPerPrivateNewspaper("admin",null);
		ratioSubscriberPerPrivateNewspaper(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		ratioSubscriberPerPrivateNewspaper("user1",IllegalArgumentException.class);				// 	El usuario es un user
		ratioSubscriberPerPrivateNewspaper("customer1",IllegalArgumentException.class); 			//	El usuario es un customer
	}
	
	
/*			5. The average ratio of private versus public newspapers per publisher.
*/
	@Test
	public void avgRatioPrivateVersusPublic(){
		avgRatioPrivateVersusPublic("admin",null);
		avgRatioPrivateVersusPublic(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		avgRatioPrivateVersusPublic("user1",IllegalArgumentException.class);				// 	El usuario es un user
		avgRatioPrivateVersusPublic("customer1",IllegalArgumentException.class); 			//	El usuario es un customer
	}
	
/*	5. An actor who is authenticated as an administrator must be able to:
		3. Display a dashboard with the following information:		
			1. The ratio of newspapers that have at least one advertisement versus the newspapers that haven't any.	
*/
	@Test
	public void ratioAdvertisedNewspapers(){
		ratioAdvertisedNewspapers("admin",null);
		ratioAdvertisedNewspapers(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		ratioAdvertisedNewspapers("user1",IllegalArgumentException.class);			// 	El usuario es un user
		ratioAdvertisedNewspapers("customer1",IllegalArgumentException.class); 		//	El usuario es un customer
	}
	
/*2. The ratio of advertisements that have taboo words.
*/
	@Test
	public void ratioTabooAdvertisements(){
		ratioTabooAdvertisements("admin",null);
		ratioTabooAdvertisements(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		ratioTabooAdvertisements("user1",IllegalArgumentException.class);			// 	El usuario es un user
		ratioTabooAdvertisements("customer1",IllegalArgumentException.class); 		//	El usuario es un customer
	}
	
/*	11. An actor who is authenticated as an administrator must be able to:
		1. Display a dashboard with the following information:	
			1. The average number of newspapers per volume.
*/
		@Test
		public void avgNewspapersPerVolume(){
			avgNewspapersPerVolume("admin",null);
			avgNewspapersPerVolume(null,IllegalArgumentException.class);				//	El usuario no est� logueado
			avgNewspapersPerVolume("user1",IllegalArgumentException.class);			// 	El usuario es un user
			avgNewspapersPerVolume("customer1",IllegalArgumentException.class); 		//	El usuario es un customer
		}
		
/*			2. The ratio of subscriptions to volumes versus subscriptions to newspapers.
*/
		@Test
		public void ratioVolumeSubscriptionsVersusNewspaperSubscription(){
			ratioVolumeSubscriptionsVersusNewspaperSubscription("admin",null);
			ratioVolumeSubscriptionsVersusNewspaperSubscription(null,IllegalArgumentException.class);				//	El usuario no est� logueado
			ratioVolumeSubscriptionsVersusNewspaperSubscription("user1",IllegalArgumentException.class);			// 	El usuario es un user
			ratioVolumeSubscriptionsVersusNewspaperSubscription("customer1",IllegalArgumentException.class); 		//	El usuario es un customer
		}
		
	protected void testAvgSdNewspaperPerUser(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgSdNewspapersPerUser();
			
			expectedDouble = new Double[] {2.3333,1.6996731715571411};
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void testAvgSdArticlesPerWriter(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgSdArticlesPerUser();
			expectedDouble = new Double[] {2.6667,2.0548046684403958};
			
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void testAvgSdArticlesPerNewspaper(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgSdArticlesPerNewspaper();

			expectedDouble = new Double[] {1.1429,0.3499271080948029};
			
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void ratioOfCreators(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.ratioOfCreators();
			expectedDouble = 0.666700005531311;

			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void ratioOfWriters(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.ratioOfArticleCreators();
			expectedDouble = 0.20000000298023224;

			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgFollowUpsPerArticle(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgFollowUpsPerArticle();

			expectedDouble = 0.25;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void testAvgFollowupsPerArticlePlus1Week(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgFollowupsPerArticlePlus1Week();
			expectedDouble = 0.125;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void testAvgFollowupsPerArticlePlus2Weeks(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgFollowupsPerArticlePlus2Weeks();
			expectedDouble = 0.125;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void testAvgSdChirpsPerUser(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgSdChirpsPerUser();
			expectedDouble = new Double[] {2.0,1.6329931616513278 };
			
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	protected void testRatioUsersOver75(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.ratioUsersWithOver75PercentAvgChirps();
			
			expectedDouble = 0.666700005531311;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void ratioPublicOverPrivateNewspapers(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.ratioPublicOverPrivateNewspapers();
			expectedDouble = 6.0;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgArticlesPerPrivateNewspaper(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgArticlesPerPrivateNewspaper();
			expectedDouble = 1.0;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgArticlesPerPublicNewspaper(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgArticlesPerPublicNewspaper();
			expectedDouble = 1.1667;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgRatioPrivateVersusPublic(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgRatioPrivateOverPublicNewspaperPerUser();
			expectedDouble = 0.5;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	
	protected void getNewspaperWith10PercentMoreArticlesThanAvg(String username, Class<?> expected){

		Class<?> caught;
		Collection<?> testingData; 
		Collection<Newspaper> expectedCollection;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getNewspaperWith10PercentMoreArticlesThanAvg();
			expectedCollection = new ArrayList<Newspaper>();
			expectedCollection.add(newspaperService.findOne(super.getEntityId("newspaper1")));

			compareCollections(testingData, expectedCollection, true);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void ratioSubscriberPerPrivateNewspaper(String username, Class<?> expected){

		Class<?> caught;
		Collection<Object[]> testingData; 
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.ratioOfSubscribersPerNewspaper();
				for(Object[] o:testingData){
					if(!(o[0] instanceof Newspaper)){
						throw new UnexpectedException();
					}
				}
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void getNewspaperWith10PercentLessArticlesThanAvg(String username, Class<?> expected){
		Class<?> caught;
		Collection<?> testingData; 
		Collection<Newspaper> expectedCollection;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getNewspaperWith10PercentLessArticlesThanAvg();
			expectedCollection = new ArrayList<Newspaper>();
			expectedCollection.add(newspaperService.findOne(super.getEntityId("newspaper2")));
			expectedCollection.add(newspaperService.findOne(super.getEntityId("newspaper3")));
			expectedCollection.add(newspaperService.findOne(super.getEntityId("newspaper4")));

			compareCollections(testingData, expectedCollection, true);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void ratioAdvertisedNewspapers(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.ratioOfNewspaperWithAdvertisementsOverOthers();
			
			expectedDouble = 0.16670000553131104;

			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void ratioTabooAdvertisements(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.ratioOfTabooAdvertisements();
			
			expectedDouble = 1.0;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgNewspapersPerVolume(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.avgNewspaperPerVolume();
			expectedDouble = 1.0;

			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	
	protected void ratioVolumeSubscriptionsVersusNewspaperSubscription(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.ratioVolumeSubscriptionsVersusNewspaperSubscription();
			
			expectedDouble = 1.0;

			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	
	protected void compareCollections(Collection<?> queryResult, Collection<?> expectedCollection, Boolean ordered){

		if(!(queryResult.containsAll(expectedCollection)&&queryResult.containsAll(expectedCollection))){
			throw new UnexpectedException();
		}
		if(ordered){
			Iterator<?> queryResultIt;
			
			queryResultIt = queryResult.iterator();
			for(Object obj: expectedCollection){
				if(!obj.equals(queryResultIt.next()))
					throw new UnexpectedException();
				
			}
		}
	}
	
	protected void compareDoubles(Double queryResult, Double expectedDouble){

		if(!queryResult.equals(expectedDouble)){
			throw new UnexpectedException();
		}
	}
}
