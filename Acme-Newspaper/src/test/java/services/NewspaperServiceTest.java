package services;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Article;
import domain.Newspaper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class NewspaperServiceTest extends AbstractTest{

	@Autowired
	private NewspaperService newspaperService;
	
	@Autowired
	private ArticleService articleService;
	
	@Test
	public void listDrive(){
		Object[][] testingData = {
				{ "admin", "newspaper1",false, null, null },
				{ null, "newspaper2",false, null, null },
				{ "admin", "newspaper7",true, null, null },
				{ "user1", "newspaper1",false, "article1", null },


				{ "admin", "newspaper12" ,false, null,  NumberFormatException.class },
				{ null, "newspaper1",true, null, IllegalArgumentException.class },
				{ "user2", "newspaper2",true, null, IllegalArgumentException.class },
				{ "user2", "newspaper1",false, "article3", IllegalArgumentException.class },

				};

		for (int i = 0; i < testingData.length; i++) {
			templateList((String) testingData[i][0],(String) testingData[i][1],
					(Boolean) testingData[i][2],(String) testingData[i][3],(Class<?>) testingData[i][4]);
		}
	}
	
	@Test
	public void listKeywordSearch(){
		Object[][] testingData = {
				{"Newspaper1", null},
				{"Newspaper1", null},
		
				{"Joseba", IllegalArgumentException.class },
				{"Carglass", IllegalArgumentException.class },

				};

		for (int i = 0; i < testingData.length; i++) {
			templateFindByKeyword((String) testingData[i][0], (Class<?>) testingData[i][1]);
		}
	}
	
	@Test
	public void createDrive(){
		Object[][] testingData = {
				{ "user1", "Titulo","Descripcion", null, false, null },
				{ "user2", "Titulo","Descripcion2", null, false, null },


				{ "admin", "Titulo","Descripcion", null, false,IllegalArgumentException.class },
				{ "customer1", "Titulo","Descripcion", null, false, IllegalArgumentException.class },
				 
				};

		for (int i = 0; i < testingData.length; i++) {
			templateCreate((String) testingData[i][0],(String) testingData[i][1],
					(String) testingData[i][2], (String) testingData[i][3],(Boolean) testingData[i][4], (Class<?>) testingData[i][5]);
		}
	}
	
	@Test
	public void publishNewspaper(){
		Object[][] testingData = {
				{ "user1", "newspaper3",null},
				
				{ "admin", "newspaper3",IllegalArgumentException.class},
				{ "user1", "newspaper1", IllegalArgumentException.class},

				};

		for (int i = 0; i < testingData.length; i++) {
			templatePublish((String) testingData[i][0],(String) testingData[i][1],
					(Class<?>) testingData[i][2]);
		}
	}
	
	@Test
	public void driverDelete() {
		Object[][] testingData = {
				{"admin", "newspaper1", null}, //Newspaper

				{"user2", "newspaper1", IllegalArgumentException.class}, //Falta de credenciales (User)
				{"admin", "newspaper3", IllegalArgumentException.class}, //Borrado de draft
		};

		for (int i = 0; i < testingData.length; i++) {
			templateDelete((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}
	
	/* List the newspapers that are published and browse their articles.
	 * List the newspapers that contain taboo words. 
	 * List the newspapers that are published and browse their articles. */
	protected void templateList(String username,String newspaperBean, Boolean taboo, String articleBean, Class<?> expected){
	
		Class<?> caught;

		Collection<Newspaper> newspapers;
		Collection<Article> articles;
		Newspaper newspaper;
		Article article;
		
		caught = null;

		try {
			super.authenticate(username);
			
			newspaper = this.newspaperService.findOne(this.getEntityId(newspaperBean));
			
			if(!taboo)
				newspapers = newspaperService.findAll();
			else
				newspapers = newspaperService.newspapersContainingTabooWords();
			
			if(articleBean!=null){
				article = articleService.findOne(this.getEntityId(articleBean));
				articles = articleService.findArticlesByNewspaperId(newspaper.getId());
				Assert.isTrue(articles.contains(article));
			}

			
			Assert.isTrue(newspapers.contains(newspaper));

			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	/*Search for a published newspaper using a single keyword that must appear somewhere
	in its title or its description.*/
	protected void templateFindByKeyword(String keyword, Class<?> expected){
		
		Class<?> caught;

		Collection<Newspaper> newspapers;
		List<Newspaper> newspaperSearched;
		Newspaper searched;
		Boolean contains = false;
		caught = null;

		try {
			newspaperSearched = (List<Newspaper>) this.newspaperService.searchNewspaperbyKeyword(keyword);
			if(!newspaperSearched.isEmpty()){
				searched = newspaperSearched.get(0);
				newspapers = newspaperService.findAll();
	
				for(Newspaper n : newspapers){
					if(n.equals(searched))
						contains = true;
				}
								
				super.unauthenticate();
			}
			Assert.isTrue(contains);
		} catch (Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	/*Create a newspaper. A user who has created a newspaper is commonly referred to
	as a publisher.*/
	protected void templateCreate(String user, String title, String description, String picture, Boolean isPrivate, Class<?> expected){
		Class<?> caught;
		
		Newspaper newspaper;
		caught = null;

		try {
			super.authenticate(user);

			newspaper = newspaperService.create();
			newspaper.setTitle(title);
			newspaper.setDescription(description);
			newspaper.setPicture(picture);
			newspaper.setIsPrivate(isPrivate);
			
			newspaper = newspaperService.save(newspaper);
			
		} catch (Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	/*Publish a newspaper that he or she's created. Note that no newspaper can be published
     until each of the articles of which it is composed is saved in final mode.*/
	protected void templatePublish(String user, String newspaperBean, Class<?> expected){
		Class<?> caught;
		
		Newspaper newspaper;
		caught = null;

		try {
			super.authenticate(user);

			newspaper = newspaperService.findOne(super.getEntityId(newspaperBean));
	
			newspaperService.publish(newspaper.getId());
			
			for(Article a : articleService.findArticlesByNewspaperId(newspaper.getId())){
				Assert.isTrue(!a.getDraft());
			}
			
		} catch (Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void templateDelete(String username, String newspaperBean, Class<?> expected) {
		Class<?> caught;
	
		Newspaper newspaper;
	
		caught = null;
	
		try {
			super.startTransaction();
			
			super.authenticate(username);
			
			newspaper = newspaperService.findOne(super.getEntityId(newspaperBean));
			Assert.notNull(newspaper);
	
			newspaperService.delete(newspaper);
			newspaperService.flush();
			
			super.unauthenticate();
	
		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}
	
		checkExceptions(expected, caught);
	}
	
}
