package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Customer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class CustomerServiceTest extends AbstractTest{

	@Autowired
	private CustomerService customerService;
	
/*
 	1. The system must support two kinds of actors, namely: administrators and customers. 
 	It must store the following information regarding them: their names, surnames, 
 	postal addresses (optional), phone numbers (optional), and email addresses.
	
	21. An actor who is not authenticated must be able to:
		1. Register to the system as a customer. 
*/
	@Test
	public void driverCreate(){
		Calendar cal;
		Calendar futureCal;
		
		cal = Calendar.getInstance();
		futureCal = Calendar.getInstance();
		cal.set(1998, 11, 19);
		futureCal.set(2030, 11, 12);
		Object[][] testingData = {
				
				//Positivos
				{null, "customerTest", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", null},
				{null, "customerTest1", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", null},
				
				//Negativos

				{null, "cus", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class}, 								// Nombre de usuario (customername) demasiado corto (menos de 4 caracteres)
				{null, "customer1234123412341234123412341234", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},		// Nombre de usuario (customername) demasiado largo (m�s de 32 caracteres)
				{null, "customerTest2", "pass", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},									// Contrase�a (password) demasiado corta (menos de 4 caracteres)
				{null, "customerTest2", "pass1234123412341234123412341234", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},		// Contrase�a (password) demasiado larga (m�s de 32 caracteres)
				{null, "customerTest2", "passwordTest", "", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},									// Nombre(name) en blanco
				{null, "customerTest2", "passwordTest", "nameTest", "", "mail@test.test", "", "", ConstraintViolationException.class},										// Apellido/s (surname) en blanco
				{null, "customerTest2", "passwordTest", "nameTest", "surnameTest", "", "", "", ConstraintViolationException.class},											// Correo electr�nico (email) en blanco
				{null, "customerTest2", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},						// Fecha de nacimiento (birthday) futura
				{null, "customer1", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", ConstraintViolationException.class},								// Nombre de usuario (customername) ya registrado
				{"admin", "customerTest3", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", ConstraintViolationException.class},		// Intento de registro ya logueado como admin
				{"customer1", "customerTest3", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", ConstraintViolationException.class},		// Intento de registro ya logueado como customer
				{"manager1", "customerTest3", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", ConstraintViolationException.class}	// Intento de registro ya logueado como manager
				
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2],
						 (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5],
						 (String) testingData[i][6], (String) testingData[i][7], (Class<?>) testingData[i][8]);
		}
	}

	@Test
	public void driverFindcustomers(){
		Object[][] testingData = {
				{"customer1", null},		//Listar como manager
				{"admin", null},		//Listar como admin
				{"user1", null},		//Listar como user
				{null, null},			//Listar como no autenticado
		};
		
		for(int i = 0; i<testingData.length; i++){
			findCustomers((String) testingData[i][0],(Class<?>) testingData[i][1]);
		}
	}
	protected void findCustomers(String username, Class<?> expected){
		Class<?>  caught;
		caught=null;
		
		try{
			super.authenticate(username);
			customerService.findAll();
			super.unauthenticate();
		}catch (Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void templateCreate(String username, String usernameToBeSetted, String password, String name,String surname, String email, String phone, String address, Class<?> expected){
		Class<?>  caught;
		
		Customer customer;
		UserAccount userAccount;
		Collection<Authority> auths;
		Authority auth;
	
		auth = new Authority();
		auths = new ArrayList<Authority>();
		auth.setAuthority(Authority.CUSTOMER);
		auths.add(auth);
		userAccount = new UserAccount();
		caught = null;
		
		try{			
			super.authenticate(username);
			
			customer = customerService.create();
			Assert.notNull(customer);

			customer.setName(name);
			customer.setSurname(surname);
			customer.setEmail(email);
			customer.setPhone(phone);
			customer.setAddress(address);
			userAccount.setUsername(usernameToBeSetted);
			userAccount.setPassword(password);
			userAccount.setAuthorities(auths);
			customer.setUserAccount(userAccount);
				
			customerService.save(customer);
			customerService.flush();
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}	
}
