<%--
 * display.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authorize access="hasRole('USER')">
	<jstl:if test="${show}">	
		<jstl:if test="${isFollowed}">
			<a href="user/unfollow.do?userId=${user.id}"><spring:message code="display.unfollow"/></a><br/>
		</jstl:if>
		
		<jstl:if test="${!isFollowed}">
			<a href="user/follow.do?userId=${user.id}"><spring:message code="display.follow"/></a><br/>
		</jstl:if>
	</jstl:if>
</security:authorize>

<p>
	<b>
		<spring:message code="user.list.name"/>:
	</b>
	<jstl:out value="${user.getName()}" />
</p>

<p>
	<b>
		<spring:message code="user.list.surname"/>:
	</b>
	<jstl:out value="${user.getSurname()}" />
</p>

<p>
	<b>
		<spring:message code="user.list.email"/>:
	</b>
	<jstl:out value="${user.getEmail()}" />
</p>

<p>
	<jstl:if test="${not empty user.getAddress()}">
		<b>
			<spring:message code="user.list.address" />:
		</b>
		<jstl:out value="${user.getAddress()}" />
	</jstl:if>
</p>

<p>
	<jstl:if test="${not empty user.getPhone()}">
		<b>
			<spring:message code="user.list.phone" />:
		</b>
		<jstl:out value="${user.getPhone()}" />
	</jstl:if>
</p>

<jsp:useBean id="currentDate" class="java.util.Date" />

<display:table name="articles" id="row" requestURI="${requestUri}" pagesize="5">
	
	<display:column property="title" titleKey="user.display.article.title" />

	<display:column property="summary" titleKey="user.display.article.summary" />
	
</display:table>
