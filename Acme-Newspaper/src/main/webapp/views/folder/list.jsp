<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<h1>
	<jstl:out value="${folder.getName()}" />
	<jstl:if test="${folder.getType().getFolderType().equals('USERBOX')}">
		<a href="folder/edit.do?folderId=${folder.getId()}" ><spring:message code="folder.list.edit" /></a>
	</jstl:if>
</h1>

<a href="folder/list.do?folderId=${systemFolders[0].getId()}"><spring:message code="folder.list.inbox" /></a> | 
<a href="folder/list.do?folderId=${systemFolders[1].getId()}"><spring:message code="folder.list.outbox" /></a> | 
<a href="folder/list.do?folderId=${systemFolders[2].getId()}"><spring:message code="folder.list.notificationbox" /></a> | 
<a href="folder/list.do?folderId=${systemFolders[3].getId()}"><spring:message code="folder.list.spambox" /></a> | 
<a href="folder/list.do?folderId=${systemFolders[4].getId()}"><spring:message code="folder.list.trashbox" /></a>

<br />
<br />

<spring:message code="folder.list.parentfolder" />

<a href="folder/list.do?folderId=${parentFolder.getId()}"><jstl:out value="${parentFolder.getName()}" /></a>

<br />
<br />

<spring:message code="folder.list.subfolders" />

<jstl:forEach items="${subFolders}" var="subFolder" varStatus="item" >
	<a href="folder/list.do?folderId=${subFolder.getId()}"><jstl:out value="${subFolder.getName()}" /></a>
	<jstl:if test="${item.index != 0 && item.index % 4 == 0}">
		<br />
	</jstl:if>
	<jstl:if test="${!(item.index != 0 && item.index % 4 == 0) && !item.isLast()}">
		 | 
	</jstl:if>
</jstl:forEach>

<jstl:set value="" var="addNewFolderParam" />
<jstl:if test="${folder.getType().getFolderType().equals('USERBOX')}">
	<jstl:set value="?parentFolderId=${folder.getId()}" var="addNewFolderParam" />
</jstl:if>
<a href="folder/create.do${addNewFolderParam}" ><spring:message code="folder.list.addNewFolder" /></a>

<br />
<br />

<display:table name="messages" id="row" requestURI="/folder/list.do" pagesize="10">
	
	<display:column property="subject" titleKey="message.list.subjectHeader" sortable="false" />
	
	<spring:message code="formatDate" var="formatDate" />
	<display:column property="moment" titleKey="message.list.momentHeader" format="{0,date,${formatDate}}" sortable="true" />
	
	<display:column titleKey="message.list.priorityHeader" sortable="true">
		<jstl:choose>
			<jstl:when test="${row.getPriority().getPriority() == \"HIGH\"}">
				<spring:message code="message.priority.high" />
			</jstl:when>
			<jstl:when test="${row.getPriority().getPriority() == \"NEUTRAL\"}">
				<spring:message code="message.priority.neutral" />
			</jstl:when>
			<jstl:when test="${row.getPriority().getPriority() == \"LOW\"}">
				<spring:message code="message.priority.low" />
			</jstl:when>
		</jstl:choose>
	</display:column>
	
	<display:column title="" sortable="false">
		<spring:url value="message/display.do?messageId=${row.getId()}" var="showURL"/>
		<a href="${showURL}">
			<spring:message code="message.list.show" />
		</a>
	</display:column>
		
</display:table>

<br />

<acme:cancel code="folder.list.newMessage" url="message/create.do" />

<security:authorize access="hasRole('ADMIN')">
	<acme:cancel code="folder.list.broadcastNotification" url="message/create.do?isNotification=true" />
</security:authorize>