<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="volumeSubscription/customer/edit.do?volumeId=${param.volumeId}" modelAttribute="volumeSubscription" >

	<!-- Indicamos los campos de los formularios -->

	<acme:textbox code="volumesubscription.edit.holderName" path="creditCard.holderName" required="required" />
	<br />
	
	<acme:textbox code="volumesubscription.edit.brandName" path="creditCard.brandName" required="required" />
	<br />

	<acme:textbox code="volumesubscription.edit.creditCardNumber" path="creditCard.creditCardNumber" required="required" />
	<br />

	<acme:textbox type="number" code="volumesubscription.edit.expirationMonth" path="creditCard.expirationMonth" required="required" placeholderCode="volumesubscription.edit.expirationMonth.placeholder" />
	<br />
	
	<acme:textbox type="number" code="volumesubscription.edit.expirationYear" path="creditCard.expirationYear" required="required" placeholderCode="volumesubscription.edit.expirationYear.placeholder" />
	<br />
	
	<acme:textbox code="volumesubscription.edit.cvv" path="creditCard.cvv" required="required" placeholderCode="volumesubscription.edit.cvv.placeholder" />
	<br />
	
	<jstl:if test="${creditCardMessage != null && creditCardMessage != ''}">
		<p class="error"><spring:message code="${creditCardMessage}"/></p>
	</jstl:if>
	<br />
	
	<!-- Botones del formulario -->
	<acme:submit name="save" code="volumesubscription.edit.save" />
	<acme:cancel url="volume/display.do?volumeId=${param.volumeId}" code="volumesubscription.edit.cancel" />

</form:form>