<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="newspaperSubscription/customer/create.do?newspaperId=${param.newspaperId}" modelAttribute="newspaperSubscription" >

	<!-- Indicamos los campos de los formularios -->

	<acme:textbox code="edit.holderName" path="creditCard.holderName" required="required" />
	<br />
	
	<acme:textbox code="edit.brandName" path="creditCard.brandName" required="required" />
	<br />

	<acme:textbox code="edit.creditCardNumber" path="creditCard.creditCardNumber" required="required" />
	<br />

	<acme:textbox type="number" code="edit.expirationMonth" path="creditCard.expirationMonth" required="required" placeholderCode="edit.expirationMonth.placeholder" />
	<br />
	
	<acme:textbox type="number" code="edit.expirationYear" path="creditCard.expirationYear" required="required" placeholderCode="edit.expirationYear.placeholder" />
	<br />
	
	<acme:textbox code="edit.cvv" path="creditCard.cvv" required="required" placeholderCode="edit.cvv.placeholder" />
	<br />
	
	<jstl:if test="${creditCardMessage != null && creditCardMessage != ''}">
		<p class="error"><spring:message code="${creditCardMessage}"/></p>
	</jstl:if>
	<br />
	
	<!-- Botones del formulario -->
	<acme:submit name="save" code="edit.save" />
	<acme:cancel url="newspaper/display.do?newspaperId=${param.newspaperId}" code="edit.cancel" />

</form:form>