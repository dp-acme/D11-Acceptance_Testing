package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.NewspaperSubscription;

@Component
@Transactional
public class NewspaperSubscriptionToStringConverter implements Converter<NewspaperSubscription, String> {

	@Override
	public String convert(NewspaperSubscription source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getId());

		return result;
	}

}
