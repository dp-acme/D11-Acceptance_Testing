package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Article;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer>{

	@Query("select a from Article a where a.article.id = ?1 order by a.moment")
	Collection<Article> findFollowUpsByArticleId(int id);
	
	@Query("select a from Article a where a.newspaper.id = ?1")
	Collection<Article> findArticlesByNewspaperId(int id);

	@Query("select a from Article a where a.draft = false and a.newspaper.draft = false  and a.article = null and a.newspaper.isPrivate = false and (a.title like ?1 or a.summary like ?1 or a.body like ?1)")	
	Collection<Article> searchArticlesByKeyword(String keyword);	

	@Query("select a from Article a where a.draft = false and a.newspaper.draft = false and a.article = null and a.newspaper.isPrivate = false")	
	Collection<Article> findPublishedArticles();

	@Query("select a from Article a where a.containsTabooWord = true and a.newspaper.draft = false")	
	Collection<Article> articlesContainsTabooWords();	
}
