package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Advertisement;
import domain.Newspaper;

@Repository
public interface AdvertisementRepository extends JpaRepository<Advertisement, Integer>{
	
	@Query("select a.newspaper from Advertisement a where a.agent.id = ?1")
	Collection<Newspaper> getAdvertisedNewspapers(int id);
	
	@Query("select n from Newspaper n where n NOT IN (select a.newspaper from Advertisement a where a.agent.id = ?1) and n.draft=false")
	Collection<Newspaper> getNotAdvertisedNewspapers(int id);
	
	@Query("select a from Advertisement a where a.containsTabooWord = true")
	Collection<Advertisement> advertisementsContainsTabooWords();
	
	@Query("select a from Advertisement a where a.newspaper.id = ?1")
	Collection<Advertisement> findAdvertisementsByNewspaperId(int id);
			
	//
}