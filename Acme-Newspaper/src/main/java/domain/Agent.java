package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Agent extends Actor {

	// Constructor
	
	public Agent() {
		super();
	}

	// Attributes
	
	// Relationships
	
	private Collection<Advertisement> myAdvertisements;
	
	@Valid
	@NotNull
	@OneToMany(mappedBy = "agent")
	public Collection<Advertisement> getMyAdvertisements() {
		return myAdvertisements;
	}
	public void setMyAdvertisements(Collection<Advertisement> myAdvertisements) {
		this.myAdvertisements = myAdvertisements;
	}	
}
