package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ActorRepository;
import security.LoginService;
import domain.Actor;
import domain.Customer;
import domain.Folder;
import domain.FolderType;
import domain.User;

@Service
@Transactional
public class ActorService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private ActorRepository actorRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private FolderService folderService;
	
	// Constructor ---------------------------------------------------
	
	public ActorService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Actor findOne(int actorId) {
		Assert.isTrue(actorId != 0);
		
		Actor result;
		
		result = actorRepository.findOne(actorId);
		
		return result;
	}
	
	public Collection<Actor> findAll() {		
		Collection<Actor> result;
		
		result = actorRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Actor save(Actor actor) {
		Assert.notNull(actor);
		
		Actor result;
		
		result = actorRepository.save(actor);
		
		return result;
	}
	
	// Other business methods ---------------------------------------------------
	
	public Actor findByUserAccountId(int userAccountId) {
		Assert.isTrue(userAccountId != 0);
		
		Actor result;
		
		result = actorRepository.findByUserAccountId(userAccountId);
		
		return result;
	}

	public Actor findPrincipal() {
		return findByUserAccountId(LoginService.getPrincipal().getId());
	}
	
	//Crear las 5 carpetas b�sicas para un actor (suponiendo que no tiene ninguna de ellas)
	@SuppressWarnings("unused")
	public Actor addSystemFolders(Actor actor) {
		Assert.notNull(actor);
		Folder folder, folderResult;
		FolderType inbox, outbox, spambox, notificationbox, trashbox;
		
		folder = folderService.create(actor);
		inbox = new FolderType();
		outbox = new FolderType();
		spambox = new FolderType();
		notificationbox = new FolderType();
		trashbox = new FolderType();
		inbox.setFolderType(FolderType.INBOX);
		outbox.setFolderType(FolderType.OUTBOX);
		spambox.setFolderType(FolderType.SPAMBOX);
		notificationbox.setFolderType(FolderType.NOTIFICATIONBOX);
		trashbox.setFolderType(FolderType.TRASHBOX);
		
		folder.setName("INBOX");
		folder.setType(inbox);
		folderResult = folderService.save(folder);
		
		folder.setName("OUTBOX");
		folder.setType(outbox);
		folderResult = folderService.save(folder);
		
		folder.setName("SPAMBOX");
		folder.setType(spambox);
		folderResult = folderService.save(folder);
		
		folder.setName("NOTIFICATIONBOX");
		folder.setType(notificationbox);
		folderResult = folderService.save(folder);
		
		folder.setName("TRASHBOX");
		folder.setType(trashbox);
		folderResult = folderService.save(folder);
		
		return actor;
	}
	
	public User checkIsUser() {
		// Comprueba que el usuario logeado es un user
		Actor actor;
		actor = findPrincipal();

		Assert.isInstanceOf(User.class, actor);
		
		return (User) actor;
	}
	
	public Customer checkIsCustomer() {
		// Comprueba que el usuario logeado es un user
		Actor actor;
		actor = findPrincipal();

		Assert.isInstanceOf(Customer.class, actor);
		
		return (Customer) actor;
	}
	
}
