package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.NewspaperSubscriptionRepository;
import domain.Actor;
import domain.Customer;
import domain.Newspaper;
import domain.NewspaperSubscription;

@Service
@Transactional
public class NewspaperSubscriptionService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private NewspaperSubscriptionRepository newspaperSubscriptionRepository;

	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	// Constructor------------------------------------------------------------------------

	public NewspaperSubscriptionService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public NewspaperSubscription create(Newspaper newspaper) {
		Assert.notNull(newspaper);
		
		// Comprobamos que el usuario logeado es un customer
		Actor actor;
		actor = actorService.findPrincipal();
		Assert.isInstanceOf(Customer.class, actor);
		
		Customer customer;
		customer = (Customer) actor;
		
		Assert.isTrue(!newspaper.isDraft());
		Assert.isTrue(newspaper.isIsPrivate());
		Assert.isTrue(newspaper.getPublicationDate().before(new Date()));
		Assert.isNull(getSubscriptionFromCustomerAndNewspaper(customer.getId(), newspaper.getId()));

		// Creamos el resultado
		NewspaperSubscription result;
		result = new NewspaperSubscription();
		
		// Inicializamos los atributos
		result.setCustomer(customer);
		result.setNewspaper(newspaper);

		return result;
	}

	public NewspaperSubscription findOne(int subscriptionId) {
		// Creamos el objeto a devolver
		NewspaperSubscription result;
		
		// Cogemos del repositorio el Subscription que le pasamos como parametro
		result = this.newspaperSubscriptionRepository.findOne(subscriptionId);
		Assert.notNull(result);
		
		return result;
	}

	public Collection<NewspaperSubscription> findAll() {
		// Creamos el objeto a devolver
		Collection<NewspaperSubscription> result;
		
		// Cogemos del repositorio los Subscription
		result = this.newspaperSubscriptionRepository.findAll();
		
		return result;
	}

	public NewspaperSubscription save(NewspaperSubscription subscription) {
		Assert.notNull(subscription);
		Assert.isTrue(!subscription.getNewspaper().isDraft());
		Assert.isTrue(subscription.getNewspaper().isIsPrivate());
		Assert.isTrue(subscription.getNewspaper().getPublicationDate().before(new Date()));
		
		Actor actor;
		actor = actorService.findPrincipal();
		
		Assert.isInstanceOf(Customer.class, actor);
		Assert.isTrue(actor.getUserAccount().equals(subscription.getCustomer().getUserAccount()));
		
		Assert.isNull(getSubscriptionFromCustomerAndNewspaper(actor.getId(), subscription.getNewspaper().getId()));
		
		// Creamos el objeto a devolver
		NewspaperSubscription result;

		// Actualizamos la subscription
		result = this.newspaperSubscriptionRepository.save(subscription);

		return result;
	}

	// Other business methods ---------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	public NewspaperSubscription reconstruct(NewspaperSubscription subscription, BindingResult binding, int newspaperId) {
		Newspaper newspaper;
		Customer customer;
		
		newspaper = newspaperService.findOne(newspaperId);
		Assert.notNull(newspaper);
		
		customer = (Customer) actorService.findPrincipal();
		Assert.notNull(newspaper);
		
		subscription.setNewspaper(newspaper);
		subscription.setCustomer(customer);
		
		validator.validate(subscription, binding);
		
		return subscription;
	}
	
	public NewspaperSubscription getSubscriptionFromCustomerAndNewspaper(int customerId, int newspaperId) {
		NewspaperSubscription result;
		
		result = newspaperSubscriptionRepository.getSubscriptionFromCustomerAndNewspaper(customerId, newspaperId);
		
		return result;
	}
	
	public Collection<NewspaperSubscription> getSubscriptionsFromCustomer(int customerId) {
		Collection<NewspaperSubscription> result;
		
		result = newspaperSubscriptionRepository.getSubscriptionsFromCustomer(customerId);
		
		return result;
	}
	
	public Collection<NewspaperSubscription> getSubscriptionsFromNewspaper(int newspaperId) {
		Collection<NewspaperSubscription> result;
		
		result = newspaperSubscriptionRepository.getSubscriptionsFromNewspaper(newspaperId);
		
		return result;
	}
	
	public void flush() {
		newspaperSubscriptionRepository.flush();
	}
	
}
