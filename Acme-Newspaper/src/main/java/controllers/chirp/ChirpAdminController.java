package controllers.chirp;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ChirpService;
import controllers.AbstractController;
import domain.Chirp;

@Controller
@RequestMapping("/chirp/admin")
public class ChirpAdminController extends AbstractController {

	// Services------------------------------------------------------------

	@Autowired
	private ChirpService chirpService;
	
	// Constructor--------------------------------------------------------------------

	public ChirpAdminController() {
		super();
	}

	
	// List----------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list() {
		// Creamos el objeto a devolver
		ModelAndView result;

//		// Creamos una colleccion para guardar los chirps
//		Collection<Chirp> chirpsWithTabooWords;
		
		// Todos los chirps del repo
		Collection<Chirp> allChirps;
		
		allChirps = this.chirpService.findAll();
//		chirpsWithTabooWords = this.chirpService.chirpsContainsTabooWords();

		result = new ModelAndView("chirp/admin/list");

		// Al modelo y la vista le a�adimos los siguientes atributos
//		result.addObject("chirpsWithTabooWords", chirpsWithTabooWords);
		result.addObject("chirps", allChirps);
		result.addObject("requestURI", "chirp/admin/list.do");

		return result;
	}

	// List chirps with tabooWords----------------------------------------------------------
		@RequestMapping("/chirpsWithTabooWords")
		public ModelAndView chirpsWithTabooWords() {
			// Creamos el objeto a devolver
			ModelAndView result;

			// Creamos una colleccion para guardar los chirps
			Collection<Chirp> chirpsWithTabooWords;
			
			chirpsWithTabooWords = this.chirpService.chirpsContainsTabooWords();

			result = new ModelAndView("chirp/admin/chirpsWithTabooWords");

			// Al modelo y la vista le a�adimos los siguientes atributos
			result.addObject("chirps", chirpsWithTabooWords);
			result.addObject("requestURI", "chirp/admin/chirpsWithTabooWords.do");

			return result;
		}
	
	// Delete--------------------------------------------------------
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam int chirpId) {
		ModelAndView result;

		Chirp chirp = chirpService.findOne(chirpId);

		chirpService.delete(chirp);

		result = new ModelAndView("redirect:/chirp/admin/list.do");

		return result;
	}
}
