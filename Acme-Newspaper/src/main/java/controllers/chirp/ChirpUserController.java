package controllers.chirp;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ChirpService;
import services.UserService;
import controllers.AbstractController;
import domain.Chirp;
import domain.User;
import forms.ChirpForm;

@Controller
@RequestMapping("/chirp/user")
public class ChirpUserController extends AbstractController {

	// Services------------------------------------------------------------

	@Autowired
	private ChirpService chirpService;

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private UserService userService;

	// Constructor--------------------------------------------------------------------

	public ChirpUserController() {
		super();
	}
	
	
	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		ChirpForm chirpForm;
		User user;

		user = (User) actorService.findByUserAccountId(LoginService
				.getPrincipal().getId());
		// Comprobamos
		Assert.notNull(user);
		chirpForm = new ChirpForm();

		result = createEditModelAndView(chirpForm);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value="chirpForm") ChirpForm chirpForm, BindingResult binding) {
		ModelAndView result;
		Chirp chirpRes;
		
		chirpRes = chirpService.reconstruct(chirpForm, binding);
		
		if (binding.hasErrors()) {
			result = createEditModelAndView(chirpRes);
		} else {
			try {
				chirpRes = this.chirpService.save(chirpRes);
				result = new ModelAndView("redirect:/chirp/display.do?chirpId="+chirpRes.getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(chirpForm, "chirp.commit.error");
				if (chirpRes.getMoment().before(new Date()))
					result.addObject("pastDateError", "chirp.edit.pastDateError");
			}
		}

		return result;
	}


	// List My Chirps --------------------------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list() {
		// Creamos el objeto a devolver
		ModelAndView result;

		// Creamos una colleccion para guardar los chirps
		Collection<Chirp> myChirps;

		myChirps = this.userService.myChirps();

		result = new ModelAndView("chirp/user/list");
		
		// Al modelo y la vista le a�adimos los siguientes atributos
		result.addObject("chirps", myChirps);
		result.addObject("requestURI", "chirp/user/list.do");

		return result;
	}
	
	// List chirps posted by all of the users that he or she follows. --------------------------------------------------------------------------
	@RequestMapping("/chirpsUsersFollows")
	public ModelAndView listChirpsUsersFollows() {
		// Creamos el objeto a devolver
		ModelAndView result;

		// Creamos una colleccion para guardar los chirps
		Collection<Chirp> chirps;

		chirps = this.chirpService.chirpsPostedByAllUsersFollows();

		result = new ModelAndView("chirp/user/chirpsUsersFollows");
		
		// Al modelo y la vista le a�adimos los siguientes atributos
		result.addObject("chirps", chirps);
		result.addObject("requestURI", "chirp/user/chirpsUsersFollows.do");

		return result;
	}
	
	private ModelAndView createEditModelAndView(Chirp chirp) {
		ModelAndView result;

		result = createEditModelAndView(chirp, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Chirp chirp,
			String messageCode) {
		ModelAndView result;
		
		result = new ModelAndView("chirp/user/create");
		
		result.addObject("chirp", chirp);
		result.addObject("message", messageCode);

		return result;
	}
	
	private ModelAndView createEditModelAndView(ChirpForm chirpForm) {
		ModelAndView result;

		result = createEditModelAndView(chirpForm, null);

		return result;
	}

	private ModelAndView createEditModelAndView(ChirpForm chirpForm,
			String messageCode) {
		ModelAndView result;
		
		result = new ModelAndView("chirp/user/create");
		
		result.addObject("chirpForm", chirpForm);
		result.addObject("message", messageCode);

		return result;
	}
	
}
