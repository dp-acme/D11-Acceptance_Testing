package controllers.volume;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.NewspaperService;
import services.VolumeService;
import services.VolumeSubscriptionService;
import controllers.AbstractController;
import domain.Actor;
import domain.Volume;
import domain.VolumeSubscription;

@Controller
@RequestMapping("/volume")
public class VolumeController extends AbstractController {

	// Services------------------------------------------------------------

	@Autowired
	private VolumeService volumeService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private VolumeSubscriptionService volumeSubscriptionService;

	// Constructor--------------------------------------------------------------------

	public VolumeController() {
		super();
	}

	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(value = "userId", required = false) Integer userId) {
		ModelAndView result;

		result = new ModelAndView("volume/list");
		
		if(userId != null){ //Listar vol�menes de un usuario
			Assert.isTrue(LoginService.isAuthenticated());
			
			Actor principal;
			
			principal = actorService.findPrincipal();
			
			Assert.isTrue(principal.getId() == userId); //Comprobar credenciales
			
			result.addObject("volumes", volumeService.getVolumesByUserId(userId));			
			
		} else{
			result.addObject("volumes", volumeService.findAll());			
		}

		return result;
	}

	@RequestMapping("/display")
	public ModelAndView display(@RequestParam(value = "volumeId", required = true) Integer volumeId) {
		ModelAndView result;
		VolumeSubscription subscription;
		Actor principal;
		Volume volume;
		
		volume = volumeService.findOne(volumeId);
		Assert.notNull(volume);

		if(LoginService.isAuthenticated()){
			principal = actorService.findPrincipal();
			subscription = volumeSubscriptionService.getSuscriptionByVolumeAndCustomer(volumeId, principal.getId());
			
		} else{
			principal = null;
			subscription = null;
		}
		
		result = new ModelAndView("volume/display");
		result.addObject("volume", volume);
		result.addObject("newspapers", newspaperService.getPublishedNewspapersByVolumeId(volume.getId()));
		result.addObject("freeNewspapers", newspaperService.getNewspapersOutsideVolume(volume.getId(), volume.getCreator().getId())); //Newspapers sin a�adir
		result.addObject("creator", LoginService.isAuthenticated() && principal.equals((Actor) volume.getCreator()));
		result.addObject("subscribed", LoginService.isAuthenticated() && subscription != null);

		return result;
	}
}
