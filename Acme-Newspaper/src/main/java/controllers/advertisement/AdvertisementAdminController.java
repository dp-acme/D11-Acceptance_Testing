package controllers.advertisement;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AdvertisementService;
import controllers.AbstractController;
import domain.Advertisement;

@Controller
@RequestMapping("/advertisement/admin")
public class AdvertisementAdminController extends AbstractController {

	// Services------------------------------------------------------------

	@Autowired
	private AdvertisementService advertisementService;
	
	// Constructor--------------------------------------------------------------------

	public AdvertisementAdminController() {
		super();
	}

	
	// List----------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Advertisement> advertisements;

		advertisements = this.advertisementService.findAll();
		result = new ModelAndView("advertisement/admin/list");

		result.addObject("advertisements", advertisements);
		result.addObject("requestURI", "advertisement/admin/list.do");

		return result;
	}

//	// List chirps with tabooWords----------------------------------------------------------
//	@RequestMapping("/advertisementsWithTabooWords")
//	public ModelAndView chirpsWithTabooWords() {
//		ModelAndView result;
//		Collection<Advertisement> advertisementsWithTabooWords;
//		
//		advertisementsWithTabooWords = this.advertisementService.chirpsContainsTabooWords();
//		result = new ModelAndView("advertisement/admin/advertisementsWithTabooWords");
//
//		result.addObject("advertisements", advertisementsWithTabooWords);
//		result.addObject("requestURI", "advertisement/admin/advertisementsWithTabooWords.do");
//
//		return result;
//	}
	
	// Delete--------------------------------------------------------
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam int advertisementId) {
		ModelAndView result;

		advertisementService.delete(advertisementId);

		result = new ModelAndView("redirect:/advertisement/admin/list.do");
		return result;
	}
}
