package services;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Folder;
import domain.Message;
import domain.Priority;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class MessageServiceTest extends AbstractTest {

	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ActorService actorService;
	

	@Autowired
	private FolderService folderService;

	
/*	13. An actor who is authenticated must be able to:
		1. Exchange messages with other actors and manage them, 
		which includes deleting and moving them from one folder to another folder.
		
	14. An actor who is authenticated as an administrator must be able to:
		1. Broadcast a message to the actors of the system.
			(Lo relacionado unicamente con este requisito est� controlado en el controlador)
*/
	@Test
	public void driverCreate() {
		Object[][] testingData = {
				{"user1","user2","subject","body","USERBOXUser1","message1",false,null}, 
				{"admin",null,"subject","body",null,null,true,null}, 

				
				{"user1","user2","subject","","USERBOXUser1", "message1",false, IllegalArgumentException.class}, 
				{"user1","user1","subject","body","USERBOXUser1","message1",false,IllegalArgumentException.class}, 

		};

		for (int i = 0; i < testingData.length; i++) {
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3],
					(String) testingData[i][4],(String) testingData[i][5],(boolean) testingData[i][6],(Class<?>) testingData[i][7]);
		}
	}
	

	protected void templateCreate(String username, String receiverBean, String subject,  String body,String folderBean,String deletedMessageBean, boolean isNotification, Class<?> expected) {
		Class<?> caught;

		Actor actor;
		Message message;
		Priority prior;
		caught = null;
		Actor receiver;
		

		try {
			super.startTransaction();
			
			super.authenticate(username);
			
			actor = actorService.findOne(super.getEntityId(username));
		

			prior = new Priority();	
			message = messageService.create(actor);
			Assert.notNull(message);

			message.setSubject(subject);
			message.setBody(body);

			if (isNotification) {
				Collection<Actor> recipients;
				
				recipients = actorService.findAll();
				recipients.remove(actorService.findPrincipal());
				
				message.setRecipients(recipients);
			}else{
				receiver = actorService.findOne(super.getEntityId(receiverBean));
				message.getRecipients().add(receiver);
			}
			
			prior.setPriority(Priority.LOW);
			message.setPriority(prior);
			
			
			message = messageService.sendMessage(message,false);
			messageService.flush();
			
			Assert.isTrue(messageService.findAll().contains(message));
			
			if(folderBean!=null){
				Folder folder;
				
				folder = folderService.findOne(super.getEntityId(folderBean));
	
				messageService.changeFolder(message, folder, actor, true);
				Assert.isTrue(folderService.findOne(super.getEntityId(folderBean)).getMessages().contains(message));
				
				messageService.delete(messageService.findOne(super.getEntityId(deletedMessageBean)), actor);
			}
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	


}
