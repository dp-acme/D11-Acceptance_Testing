package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Folder;
import domain.FolderType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class FolderServiceTest extends AbstractTest {

	@Autowired
	private ActorService actorService;
	

	@Autowired
	private FolderService folderService;

	
/*	13. An actor who is authenticated must be able to:
		2. Manage his or her message folders, except for the system folders.
*/
	@Test
	public void driverCreate() {
		Object[][] testingData = {
				{"user1","userbox","USERBOXUser1",true,null}, 
				{"admin","childrenUserbox",null,true,null}, 

				
				{"user1","subject","USERBOXUser1",false, IllegalArgumentException.class}, 
				{"user1","",null,false,IllegalArgumentException.class}, 

		};

		for (int i = 0; i < testingData.length; i++) {
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], 
					(Boolean) testingData[i][3],(Class<?>) testingData[i][4]);
		}
	}
	

	protected void templateCreate(String username, String name,String parentFolderBean, Boolean userbox, Class<?> expected) {
		Class<?> caught;

		Actor actor;
		Folder folder;
		FolderType type;
		caught = null;

		try {
			super.startTransaction();
			
			super.authenticate(username);
			
			actor = actorService.findOne(super.getEntityId(username));
		

			folder = folderService.create(actor);
			Assert.notNull(folder);
	
			folder.setName(name);
			
			if(parentFolderBean != null)
				folder.setParentFolder(folderService.findOne(super.getEntityId(parentFolderBean)));
			
			type = new FolderType();
			if(!userbox)
				type.setFolderType(FolderType.INBOX);
			else
				type.setFolderType(FolderType.USERBOX);
			folder.setType(type);
			

			folder = folderService.save(folder);
			folderService.flush();
			
			Assert.isTrue(folderService.findAll().contains(folder));
			
	
			folderService.delete(folder); 
			Assert.isTrue(!folderService.findAll().contains(folder));
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	
	


}
