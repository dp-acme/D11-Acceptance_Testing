package services;

import java.util.Collection;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;

import com.mchange.v1.util.UnexpectedException;

import domain.Actor;
import domain.Newspaper;
import domain.User;
import domain.Volume;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class VolumeServiceTest extends AbstractTest {

	@Autowired
	private VolumeService volumeService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private NewspaperService newspaperService;

	
	
/*	8. An actor who is not authenticated must be able to:
		1. List the volumes in the system and browse their newspapers 
		as long as they are authorised (for instance, 
		a private newspaper cannot be fully displayed 
		to unauthenticated actors).
 */
	@Test
	public void listDrive(){
		Object[][] testingData = {
				{ null,  null },
				{ "user1",  null },
				{ "admin",  null },
				};

		for (int i = 0; i < testingData.length; i++) {
			templateList((String) testingData[i][0],(Class<?>) testingData[i][1]);
		}
	}
/*	10. An actor who is authenticated as a user must be able to:
		1. Create a volume with as many published newspapers as he or she wishes. 
		Note that the newspapers in a volume can be added or removed at any time. 
		The same newspaper may be used to create different volumes.
*/
	@Test
	public void driverCreate() {
		Object[][] testingData = {
				{"user1","title","descrption",2021,"user1", "newspaper1",null}, 
		
				{null, "title","descrption",2021, "user1", "newspaper1", IllegalArgumentException.class}, 
				{"user1", "","descrption",2021,"user1", "newspaper1",  ConstraintViolationException.class}, 

		};

		for (int i = 0; i < testingData.length; i++) {
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Integer) testingData[i][3],
					(String) testingData[i][4],(String) testingData[i][5],(Class<?>) testingData[i][6]);
		}
	}
	
	protected void templateList(String username, Class<?> expected){
		
		Class<?> caught;

		Collection<Volume> volumes;
		
		caught = null;

		try {
			super.authenticate(username);
			
			volumes = volumeService.findAll();
			if(volumes==null) throw new UnexpectedException();
			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	protected void templateCreate(String username, String title, String description, Integer year, String creator, String newspaperBean,Class<?> expected) {
		Class<?> caught;

		Actor actor;
		Volume volume;
		Newspaper newspaper;

		caught = null;

		try {
			super.startTransaction();
			
			super.authenticate(username);
			
			actor = actorService.findOne(super.getEntityId(creator));
			newspaper = newspaperService.findOne(super.getEntityId(newspaperBean));
			
			volume = volumeService.create((User) actor);
			Assert.notNull(volume);

			volume.setTitle(title);
			volume.setDescription(description);
			volume.setYear(year);

			volume = volumeService.save(volume);
			volumeService.flush();
			
			Assert.isTrue(volumeService.findAll().contains(volume));
			
			volumeService.addNewspaperToVolume(volume, newspaper);
			
			Assert.isTrue(volumeService.findOne(volume.getId()).getNewspapers().contains(newspaper));
			
			volumeService.deleteNewspaperFromVolume(volume, newspaper);
			
			Assert.isTrue(!volumeService.findOne(volume.getId()).getNewspapers().contains(newspaper));

			
			super.unauthenticate();

		} catch (Throwable e) {
			caught = e.getClass();
		
		} finally{
			super.rollbackTransaction();	
		}

		checkExceptions(expected, caught);
	}
	


}
