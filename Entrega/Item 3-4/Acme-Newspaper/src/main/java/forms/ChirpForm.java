package forms;

import org.hibernate.validator.constraints.NotBlank;

public class ChirpForm {
	
	// Propiedades que vamos a mostrar en la vista
	private String editedTitle;
	private String editedDescription;
	
	@NotBlank
	public String getEditedTitle() {
		return editedTitle;
	}
	public void setEditedTitle(String editedTitle) {
		this.editedTitle = editedTitle;
	}
	@NotBlank
	public String getEditedDescription() {
		return editedDescription;
	}
	public void setEditedDescription(String editedDescription) {
		this.editedDescription = editedDescription;
	}
	
	
}
