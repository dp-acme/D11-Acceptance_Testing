package repositories;


import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Chirp;

@Repository
public interface ChirpRepository extends JpaRepository<Chirp, Integer>{
	
	// Display a stream with the chirps posted by all of the users that he or she follows.
	@Query("select c from Chirp c, User u where c.user in elements(u.myFollowings) AND u.id=?1")
	Collection<Chirp> chirpsPostedByAllUsersFollows(int userId);
	
	// List the chirps that contain taboo words.
	@Query("select c from Chirp c where c.containsTabooWord = true")
	Collection<Chirp> chirpsContainsTabooWords();

}
