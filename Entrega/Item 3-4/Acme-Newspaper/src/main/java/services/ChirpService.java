package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ChirpRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Administrator;
import domain.Chirp;
import domain.User;
import forms.ChirpForm;

@Service
@Transactional
public class ChirpService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private ChirpRepository chirpRepository;

	// Supporting services ---------------------------------------------------
	@Autowired
	private ConfigurationService configurationService;

	@Autowired
	private ActorService actorService;

	// Constructor ---------------------------------------------------

	public ChirpService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Chirp create() {
		// Comprobamos que el usuario logeado es un user
		this.checkUser();
		// Si es asi lo cogemos
		UserAccount principal;
		principal = LoginService.getPrincipal();

		User user;
		user = (User) this.actorService.findByUserAccountId(principal.getId());

		// Creamos el resultado
		Chirp result;
		result = new Chirp();

		// Creamos los atributos y las relaciones
		Date moment;

		// Inicializamos los atributos
		// TODO: COMPROBAR EL MOMENT
		moment = new Date(System.currentTimeMillis() - 1);

		// Asignamos los atributos
		result.setContainsTabooWord(false);
		result.setUser(user);
		result.setMoment(moment);

		// Devolvemos el resultado
		return result;
	}

	public Chirp findOne(int chirpId) {
		// Comprobamos que el objeto que le pasamos no es nulo
		Assert.isTrue(chirpId != 0);
		// Creamos el objeto a devolver
		Chirp result;
		// Cogemos del repositorio el chirp que le pasamos como parametro
		result = this.chirpRepository.findOne(chirpId);
		// Devolvemos el resultado
		return result;
	}

	public Collection<Chirp> findAll() {
		// Creamos el objeto a devolver
		Collection<Chirp> result;
		// Cogemos del repositorio los chirps
		result = this.chirpRepository.findAll();
		// Devolvemos el resultado
		return result;
	}

	public Chirp save(Chirp chirp) {
		// Comprobamos que el chirp que le pasamos no es nulo
		Assert.notNull(chirp);

		// Comprobamos que el usuario logeado es un user
		this.checkUser();

		// Introducimos la fecha actual del sistema
		Date moment;
		moment = new Date(System.currentTimeMillis() - 1);
		chirp.setMoment(moment);

		// Actualizamos valor de boolean isTabooWord
		chirp.setContainsTabooWord(this.configurationService
				.hasTabooWords(chirp.getTitle() + " " + chirp.getDescription()));

		// Creamos el objeto a devolver
		Chirp result;

		// Actualizamos el chirp
		result = this.chirpRepository.save(chirp);

		// Devolvemos el resultado
		return result;
	}

	public void delete(Chirp chirp) {
		// Comprobamos que el chirp pasado como parametro no es nulo
		Assert.notNull(chirp);

		// Comprobamos que el usuario logeado es un admin
		this.checkAdministrator();

		// Eliminamos
		this.chirpRepository.delete(chirp);
	}

	// Other methods----------------------------------------------------------

	// Display a stream with the chirps posted by all of the users that he or
	// she follows.
	public Collection<Chirp> chirpsPostedByAllUsersFollows() {
		Collection<Chirp> result;

		// Comprobamos que el usuario logeado es un user
		this.checkUser();

		// Si es asi lo cogemos
		UserAccount principal;
		principal = LoginService.getPrincipal();

		User user;
		user = (User) this.actorService.findByUserAccountId(principal.getId());

		result = this.chirpRepository.chirpsPostedByAllUsersFollows(user
				.getId());

		// Devolvemos el resultado
		return result;
	}

	private void checkUser() {
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Actor actor;
		actor = this.actorService.findByUserAccountId(principal.getId());
		Assert.isInstanceOf(User.class, actor);
	}

	private void checkAdministrator() {
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Actor actor;
		actor = this.actorService.findByUserAccountId(principal.getId());
		Assert.isInstanceOf(Administrator.class, actor);
	}

	// List the chirps that contain taboo words.
	public Collection<Chirp> chirpsContainsTabooWords() {
		// Creamos el objeto a devolver
		Collection<Chirp> result;

		// Inicializamos el objeto
		result = this.chirpRepository.chirpsContainsTabooWords();

		// Devolvemos el resultado
		return result;
	}

	@Autowired
	private Validator validator;

	public Chirp reconstruct(ChirpForm chirpForm, BindingResult binding) {

		Chirp result;
		result = create();

		result.setTitle(chirpForm.getEditedTitle());
		result.setDescription(chirpForm.getEditedDescription());

		validator.validate(chirpForm, binding);

		return result;
	}

	public void refreshContainsTabooWords(Collection<String> tabooWords) {
		Collection<Chirp> chirps;

		chirps = chirpRepository.findAll();

		for (Chirp chirp : chirps) {
			boolean result;
			String content;

			content = chirp.getTitle() + " " + chirp.getDescription();

			result = false;
			content = content.toLowerCase();
			for (String word : tabooWords) {
				word = word.toLowerCase();
				if (content.contains(word)) {
					result = true;
					break;
				}
			}

			chirp.setContainsTabooWord(result);
		}
	}

	public void flush() {
		chirpRepository.flush();
	}

}
