package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.AdvertisementRepository;
import domain.Actor;
import domain.Administrator;
import domain.Advertisement;
import domain.Agent;
import domain.Newspaper;

@Service
@Transactional
public class AdvertisementService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private AdvertisementRepository advertisementRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private AgentService agentService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	@Autowired
	private ConfigurationService configurationService;
	
	// Validator ---------------------------------------------------

	@Autowired
	Validator validator;
	
	// Constructor ---------------------------------------------------
	
	public AdvertisementService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Advertisement create(Integer agentId, Integer newspaperId){
		Advertisement result;
		
		result = new Advertisement();
		result.setAgent(agentService.findOne(agentId));
		result.setNewspaper(newspaperService.findOne(newspaperId));
		
		return result;
	}
	
	public Advertisement findOne(int advertisement) {
		Assert.isTrue(advertisement != 0);
		
		Advertisement result;
		
		result = advertisementRepository.findOne(advertisement);
		
		return result;
	}
	
	public Collection<Advertisement> findAll() {		
		Collection<Advertisement> result;
		
		try{
			result = advertisementRepository.findAll();
		}
		catch(Throwable e){
			result = null;
		}
		
		Assert.notNull(result);
		
		return result;
	}
	
	public Advertisement save(Advertisement advertisement) {
		Assert.notNull(advertisement);
		
		Advertisement result;
		Agent agent;
		
		Assert.isTrue(actorService.findPrincipal().equals((Actor) advertisement.getAgent())); //Debes estar autenticado como el agent
		Assert.isTrue(!advertisement.getNewspaper().isDraft());
		
		advertisement.setContainsTabooWord(configurationService.hasTabooWords(advertisement.getTitle()));
		
		agent = advertisement.getAgent();
		agent.getMyAdvertisements().add(advertisement);
		agentService.save(agent);
		
		result = advertisementRepository.save(advertisement);
		
		return result;
	}
	
	public void delete(int advertisementId){
		Actor principal;
		Advertisement advertisement;
		Agent agent;
		
		advertisement = this.findOne(advertisementId);
		Assert.notNull(advertisement);

		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Administrator);
		
		agent = advertisement.getAgent();
		agent.getMyAdvertisements().remove(advertisement);
		agentService.save(agent);
		
		advertisementRepository.delete(advertisement);
	}
	
	
	// Other business methods ---------------------------------------------------

	public Collection<Advertisement> advertisementsContainsTabooWords() {
		Collection<Advertisement> result;

		Assert.isInstanceOf(Administrator.class, actorService.findPrincipal());

		result = this.advertisementRepository.advertisementsContainsTabooWords();

		return result;
	}
	
	public void refreshContainsTabooWords(Collection<String> tabooWords) {
		Collection<Advertisement> advertisements;
		
		advertisements = advertisementRepository.findAll();
		
		for (Advertisement advertisement : advertisements) {
			boolean result;
			String content;
			
			content = advertisement.getTitle();
			
			result = false;
			content = content.toLowerCase();
			for(String word : tabooWords) {
				word = word.toLowerCase();
				if(content.contains(word)){
					result = true;
					break;
				}
			}
			
			advertisement.setContainsTabooWord(result);
		}		
	}
	
	public Advertisement reconstruct(Advertisement advertisement, Integer newspaperId, BindingResult binding) {
		Advertisement result;
		
		result = advertisement;
		Newspaper newspaper;
		newspaper = this.newspaperService.findOne(newspaperId);
		result.setNewspaper(newspaper);
		result.setAgent((Agent) actorService.findPrincipal());
		validator.validate(result, binding);
		
		return result;
	}
	
	public Collection<Newspaper> getAdvertisedNewspapers(Integer agentId) {		
		Collection<Newspaper> result;
		
		Assert.notNull(agentId);
		Actor actor;

		actor = actorService.findPrincipal();
		Assert.isTrue(actor instanceof Agent && actor.getId()==agentId);
		result = advertisementRepository.getAdvertisedNewspapers(agentId);
		Assert.notNull(result);
		
		return result;
	}
	
//	public Collection<Newspaper> searchAdvertisedNewspaperbyKeyword(String keyword, int agentId){
//		return advertisementRepository.searchAdvertisedNewspaperbyKeyword(agentId ,"%"+keyword+"%");
//	}
	
	public Collection<Newspaper> getNotAdvertisedNewspapers(Integer agentId) {		
		Collection<Newspaper> result;
		
		Assert.notNull(agentId);
		Actor actor;

		actor = actorService.findPrincipal();
		Assert.isTrue(actor instanceof Agent && actor.getId()==agentId);
		Assert.isTrue(actorService.findPrincipal() instanceof Agent);

		result = advertisementRepository.getNotAdvertisedNewspapers(agentId);
		Assert.notNull(result);
		
		return result;
	}

	public void deleteNewspaperAdvertisements(int id) {
		for(Advertisement a:(advertisementRepository.findAdvertisementsByNewspaperId(id))){
			this.delete(a.getId());
		}
	}
	
	public void flush() {
		advertisementRepository.flush();
	}
	
	@SuppressWarnings("deprecation")
	public boolean checkCreditCard(int month, int year){
		Date date = new Date();
		year += 2000;
		int nowMonth = date.getMonth() + 1;
		int nowYear = date.getYear() + 1900;

		return year > nowYear || (month > nowMonth && year == nowYear);
	}
	
//	public Collection<Newspaper> searchNotAdvertisedNewspaperbyKeyword(String keyword, int agentId){
//		return advertisementRepository.searchNotAdvertisedNewspaperbyKeyword(agentId ,"%"+keyword+"%");
//	}
}
