package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.VolumeSubscriptionRepository;
import domain.Actor;
import domain.Customer;
import domain.Volume;
import domain.VolumeSubscription;

@Service
@Transactional
public class VolumeSubscriptionService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private VolumeSubscriptionRepository volumeSubscriptionRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private VolumeService volumeService;
	
	// Validator ---------------------------------------------------

	@Autowired
	Validator validator;
	
	// Constructor ---------------------------------------------------
	
	public VolumeSubscriptionService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public VolumeSubscription create(Customer customer, Volume volume){
		VolumeSubscription result;
		
		result = new VolumeSubscription();
		result.setCustomer(customer);
		result.setVolume(volume);
		
		return result;
	}
	
	public VolumeSubscription findOne(int volumeSuscriptionId) {
		Assert.isTrue(volumeSuscriptionId != 0);
		
		VolumeSubscription result;
		
		result = volumeSubscriptionRepository.findOne(volumeSuscriptionId);
		
		return result;
	}
	
	public Collection<VolumeSubscription> findAll() {		
		Collection<VolumeSubscription> result;
		
		result = volumeSubscriptionRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public VolumeSubscription save(VolumeSubscription volumeSuscription) {
		Assert.notNull(volumeSuscription);
		
		VolumeSubscription result;
		
		Assert.isTrue( actorService.findPrincipal().equals((Actor) volumeSuscription.getCustomer())); //Debes estar autenticado como el customer
		Assert.isNull(getSuscriptionByVolumeAndCustomer(volumeSuscription.getVolume().getId(), volumeSuscription.getCustomer().getId())); //No se admiten suscripciones repetidas
		
		result = volumeSubscriptionRepository.save(volumeSuscription);
		
		return result;
	}
	
	// Other business methods ---------------------------------------------------
	
	public VolumeSubscription getSuscriptionByVolumeAndCustomer(Integer volumeId, Integer customerId) {		
		VolumeSubscription result;
		
		result = volumeSubscriptionRepository.getSuscriptionByVolumeAndCustomer(volumeId, customerId);
		
		return result;
	}

	public Collection<VolumeSubscription> getVolumeSubscriptionsForNewspaperId(Integer newspaperId, Integer customerId) {		
		Collection<VolumeSubscription> result;
		
		result = volumeSubscriptionRepository.getVolumeSubscriptionsForNewspaperId(newspaperId, customerId);
		Assert.notNull(result);
		
		return result;
	}	
	
	public Collection<VolumeSubscription> getByCustomerId(Integer customerId) {
		Collection<VolumeSubscription> result;
		
		result = volumeSubscriptionRepository.getByCustomerId(customerId);
		Assert.notNull(result);
		
		return result;
	}
	
	public VolumeSubscription reconstruct(VolumeSubscription volumeSubscription, Integer volumeId, BindingResult binding) {
		VolumeSubscription result;
		
		result = volumeSubscription;
		
		if(result.getId() == 0){ //Caso de creaci�n
			Customer customer;
			Volume volume;

			volume = volumeService.findOne(volumeId);
			customer = actorService.checkIsCustomer();
			Assert.notNull(customer);
			Assert.notNull(volume);
			
			result.setCustomer(customer);
			result.setVolume(volume);
		}
		
		validator.validate(result, binding);
		
		return result;
	}

	public void flush() {
		volumeSubscriptionRepository.flush();
	}
}
