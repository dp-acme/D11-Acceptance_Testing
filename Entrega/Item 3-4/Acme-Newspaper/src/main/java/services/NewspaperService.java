package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.NewspaperRepository;
import domain.Actor;
import domain.Administrator;
import domain.Advertisement;
import domain.Article;
import domain.Customer;
import domain.Newspaper;
import domain.User;

@Service
@Transactional
public class NewspaperService {

	@Autowired
	private NewspaperRepository newspaperRepository;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private ArticleService articleService;

	@Autowired
	private AdvertisementService advertisementService;
	

	@Autowired
	private VolumeService volumeService;
	
	@Autowired
	private Validator validator;
	
	public Newspaper create(){
		Newspaper result;
		User user;
		
		user = actorService.checkIsUser();
		result = new Newspaper();
		result.setCreator(user);
		result.setDraft(true);
		result.setContainsTabooWord(false);

		return result;
	}
	
	public Newspaper findOne(int id){
		Assert.isTrue(id != 0);
		
		Newspaper result;
		
		result = newspaperRepository.findOne(id);
		
		return result;
	}
	
	public Collection<Newspaper> findAll() {		
		Collection<Newspaper> result;
		
		result = newspaperRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Newspaper save(Newspaper newspaper){
		Newspaper result;
		User creator;
		String words;
		
		Assert.notNull(newspaper);
		creator = actorService.checkIsUser();
		
		Assert.isTrue(newspaper.getCreator().equals(creator));
		Assert.isTrue(newspaper.isDraft());
		
		words = newspaper.getTitle() + " " + newspaper.getDescription();
		newspaper.setContainsTabooWord(configurationService.hasTabooWords(words));
		
		result = newspaperRepository.save(newspaper);
		
		if(newspaper.getId() == 0){
			creator.getMyNewspapers().add(result);
			userService.save(creator);
		}
		
		return result;
	}
	
	public Newspaper publish(int newspapaperId){
		Newspaper result;
		User creator;

		Assert.isTrue(newspapaperId != 0);
		
		result = findOne(newspapaperId);
		
		Assert.isTrue(result.isDraft());

		creator = actorService.checkIsUser();
		Assert.isTrue(result.getCreator().equals(creator));
		
		for(Article a : articleService.findArticlesByNewspaperId(newspapaperId)){ 
			Assert.isTrue(!a.getDraft());
			articleService.publishArticle(a);
		}
		
		result.setDraft(false);
		result.setPublicationDate(new Date());
		
		result = newspaperRepository.save(result);
		
		return result;
	}
	
	public void delete(Newspaper newspaper){
		Actor principal;
		
		Assert.notNull(newspaper);

		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Administrator);
		Assert.isTrue(!newspaper.isDraft());
		
		articleService.deleteNewspaperArticles(newspaper.getId());
		advertisementService.deleteNewspaperAdvertisements(newspaper.getId());
		volumeService.refreshNewspaperVolumes(newspaper.getId());

		
		newspaperRepository.delete(newspaper);
	}
	
	public Collection<Newspaper> getFinalPublicNewspapers(){
		return newspaperRepository.getFinalPublicNewspapers();
	}
	
	public Collection<Newspaper> getFinalPrivateNewspapers(){
		return newspaperRepository.getFinalPrivateNewspapers();
	}
	
	public Collection<Newspaper> getNewspapersSuscribed(Customer c){
		return newspaperRepository.getNewspapersSubscribed(c.getId());
	}
	
	public Newspaper reconstruct(Newspaper newspaper,  BindingResult binding){
		Newspaper result;
		Newspaper newNewspaper;
		
		if(newspaper.getId() == 0){
			newNewspaper = create();
			
			newspaper.setCreator(newNewspaper.getCreator());
			newspaper.setDraft(newNewspaper.isDraft());

			result = newNewspaper;
		}else{
			result = newspaperRepository.findOne(newspaper.getId());	
			
			newspaper.setContainsTabooWord(result.isContainsTabooWord());
			newspaper.setCreator(result.getCreator());
			newspaper.setDraft(result.isDraft());
			newspaper.setPublicationDate(result.getPublicationDate());
			newspaper.setVersion(result.getVersion());
		}
		
		newspaper.setTitle(newspaper.getTitle());
		newspaper.setDescription(newspaper.getDescription());
		newspaper.setIsPrivate(newspaper.isIsPrivate());
		newspaper.setPicture(newspaper.getPicture());
		newspaper.setId(result.getId());

		validator.validate(newspaper, binding);
		
		return newspaper;
	}
	
	public void refreshContainsTabooWords(Collection<String> tabooWords) {
		Collection<Newspaper> newspapers;
		
		newspapers = newspaperRepository.findAll();
		
		for (Newspaper newspaper : newspapers) {
			boolean result;
			String content;
			
			content = newspaper.getTitle() + " " + newspaper.getDescription();
			
			result = false;
			content = content.toLowerCase();
			for(String word : tabooWords) {
				word = word.toLowerCase();
				if(content.contains(word)){
					result = true;
					break;
				}
			}
			
			newspaper.setContainsTabooWord(result);
		}		
	}
	
	public Collection<Newspaper> searchNewspaperbyKeyword(String keyword){
		return newspaperRepository.searchNewspaperbyKeyword("%"+keyword+"%");
	}
	
	public Collection<Newspaper> getPublishedNewspapersByVolumeId(Integer volumeId) {
		Collection<Newspaper> result;
		
		result = newspaperRepository.getPublishedNewspapersByVolumeId(volumeId);
		Assert.notNull(result);
		
		return result;
	}
	
	public Collection<Newspaper> getNewspapersOutsideVolume(Integer volumeId, Integer userId) {
		Collection<Newspaper> result;
		
		result = newspaperRepository.getNewspapersOutsideVolume(volumeId, userId);
		Assert.notNull(result);
		
		return result;
	}
	
	public Advertisement getRandomAdvertisement(Integer newspaperId) {
		List<Advertisement> advertisements;
		Advertisement result;
		
		advertisements = new ArrayList<Advertisement>(newspaperRepository.getAdvertisements(newspaperId));
		Assert.notNull(advertisements);

		if (!advertisements.isEmpty()){
			result = advertisements.get(ThreadLocalRandom.current().nextInt(0, advertisements.size()));
			return result;
		}else{
			return null;
		}
	}

	public Collection<Newspaper> newspapersContainingTabooWords() {
		Collection<Newspaper> result;
		Actor principal;

		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Administrator);
		
		result = newspaperRepository.newspapersContainingTabooWords();
		Assert.notNull(result);
		
		return result;
	}
	
	public void flush() {
		newspaperRepository.flush();
	}
}
