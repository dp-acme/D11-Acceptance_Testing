package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.UserRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Article;
import domain.Chirp;
import domain.Folder;
import domain.Newspaper;
import domain.User;
import forms.RegisterCustomer;

@Service
@Transactional
public class UserService {
	
	// Managed repository ---------------------------------------------------
	
	@Autowired
	private UserRepository userRepository;

	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;

	// Constructor ---------------------------------------------------

	public UserService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------
	
	public User create() {

		User result;

		result = new User();
		result.setMyFollowings(new ArrayList<User>());
		result.setMyNewspapers(new ArrayList<Newspaper>());
		result.setFolders(new ArrayList<Folder> ());

		return result;
	}

	public User findOne(int userId) {
		Assert.isTrue(userId != 0);

		User result;

		result = userRepository.findOne(userId);

		return result;
	}

	public Collection<User> findAll() {
		Collection<User> result;

		result = userRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public User save(User user) {
		Assert.notNull(user);
		User result;
		UserAccount userAccount;

		if (user.getId() != 0) {
			userAccount = LoginService.getPrincipal();
			Assert.isTrue(user.getUserAccount().equals(userAccount));
			Assert.isTrue(!user.getMyFollowings().contains(actorService.findByUserAccountId(userAccount.getId())));
		} else {
			String role;
			Md5PasswordEncoder encoder;

			if (SecurityContextHolder.getContext().getAuthentication() != null
					&& SecurityContextHolder.getContext().getAuthentication()
							.getAuthorities().toArray().length > 0) {
				role = SecurityContextHolder.getContext().getAuthentication()
						.getAuthorities().toArray()[0].toString();
				Assert.isTrue(role.equals("ROLE_ANONYMOUS"));
			}
			encoder = new Md5PasswordEncoder();
			user.getUserAccount().setPassword(
					encoder.encodePassword(user.getUserAccount().getPassword(),
							null));
		}

		result = userRepository.save(user);

		if (user.getId() == 0) {
			result = (User)actorService.addSystemFolders(result); //Cuando se persiste por primera vez se a�aden los folders del sistema
		}
		
		result = userRepository.save(result);
		
		return result;
	}

	// Other business methods -------------------------------------------------

	public List<User> getFollowers(User user) {
		return userRepository.getFollowers(user);
	}

	public List<Article> getArticles(int userId) {
		return userRepository.getArticles(userId);
	}

	@Autowired
	private Validator validator;

	public User reconstruct(RegisterCustomer formObject, BindingResult binding) {
		User result;
		Authority authority;

		authority = new Authority();
		result = create();
		result.setAddress(formObject.getAddress());
		result.setEmail(formObject.getEmail());
		result.setName(formObject.getName());
		result.setPhone(formObject.getPhone());
		result.setSurname(formObject.getSurname());
		authority.setAuthority(Authority.USER);
		formObject.getUserAccount().addAuthority(authority);
		result.setUserAccount(formObject.getUserAccount());

		validator.validate(formObject, binding);

		return result;
	}

	public void flush() {
		userRepository.flush();
	}

	// Chirps de un user
	public Collection<Chirp> myChirps() {
		Collection<Chirp> result;

		// Obtenemos el usuario logeado
		UserAccount principal;
		principal = LoginService.getPrincipal();

		// Comprobamos que el usuario logeado es un user
		this.actorService.checkIsUser();

		// Si es asi lo cogemos
		User user;
		user = (User) this.actorService.findByUserAccountId(principal.getId());

		result = this.userRepository.myChirps(user.getId());

		// Devolvemos el resultado
		return result;
	}

}
