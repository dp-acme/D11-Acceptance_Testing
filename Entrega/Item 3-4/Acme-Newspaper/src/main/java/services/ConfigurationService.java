package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ConfigurationRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Configuration;

@Service
@Transactional
public class ConfigurationService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private ConfigurationRepository configurationRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ArticleService articleService;
	
	@Autowired
	private ChirpService chirpService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	@Autowired
	private AdvertisementService advertisementService;
	
	// Constructor ---------------------------------------------------
	
	public ConfigurationService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Configuration findConfiguration() {
		Configuration result;
		
		result = configurationRepository.findConfiguration();
		
		return result;		
	}
	
	public Configuration save(Configuration configuration) {
		Assert.notNull(configuration);
		
		Configuration result;
		Authority auth;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		
		Assert.isTrue(principal.getAuthorities().contains(auth));
		
		result = configurationRepository.save(configuration);
		
		RefreshObjectsTaboo (result.getTabooWords());
		
		return result;
	}
	
	// Other business methods -------------------------------------------------
	
	public void flush(){
		configurationRepository.flush();
	}
	
	public void RefreshObjectsTaboo (Collection<String> tabooWords){
		articleService.refreshContainsTabooWords(tabooWords);
		chirpService.refreshContainsTabooWords(tabooWords);
		newspaperService.refreshContainsTabooWords(tabooWords);
		advertisementService.refreshContainsTabooWords(tabooWords);
	}
	
	public boolean hasTabooWords(String content){
		Collection<String> tabooWords;
		boolean result;
		
		result = false;
		if (content != null){
			tabooWords = findConfiguration().getTabooWords();
			content = content.toLowerCase();
			for(String word : tabooWords) {
				word = word.toLowerCase();
				if(content.contains(word)){
					result = true;
					break;
				}
			}
		}
		
		return result;
	}
	
}
