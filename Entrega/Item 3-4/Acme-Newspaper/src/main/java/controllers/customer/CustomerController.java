/*
 * CustomerController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.CustomerService;
import domain.Customer;
import forms.RegisterCustomer;

@Controller
@RequestMapping("/customer")
public class CustomerController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private CustomerService customerService;
		
	// Constructors -----------------------------------------------------------

	public CustomerController() {
		super();
	}

	// --------------------------------------------------------------------
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		RegisterCustomer formObject;
		
		formObject = new RegisterCustomer();
		result = this.createEditModelAndView(formObject);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("userEditForm") RegisterCustomer customerEditForm, BindingResult binding){
		ModelAndView result;
		Customer customer;
		
		if(!customerEditForm.getTermsAccepted()){
			result = createEditModelAndView(customerEditForm);
			result.addObject("showMessageTerms", true);
			return result;
		}
		
		customer = customerService.reconstruct(customerEditForm, binding);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(customerEditForm);
			
		} else { 
			try {
				customerService.save(customer);
				result = new ModelAndView("redirect:/");
				
			} catch (Throwable oops) {
				result = createEditModelAndView(customerEditForm, "user.commit.error");
			}
		}
		return result;
	}
	
	protected ModelAndView createEditModelAndView(RegisterCustomer userEditForm) {
		ModelAndView result; 
		
		result = createEditModelAndView(userEditForm, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(RegisterCustomer userEditForm, String messageCode) {
		ModelAndView result;
		result = new ModelAndView("actor/customer/create");
		result.addObject("userEditForm", userEditForm);
		result.addObject("message", messageCode);
		result.addObject("requestURI", "customer/create.do");
		
		return result;
	}
	

}
