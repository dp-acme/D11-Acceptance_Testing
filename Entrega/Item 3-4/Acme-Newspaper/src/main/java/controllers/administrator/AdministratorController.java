package controllers.administrator;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.AdvertisementService;
import services.ArticleService;
import services.ChirpService;
import services.NewspaperService;
import controllers.AbstractController;
import domain.Advertisement;
import domain.Article;
import domain.Chirp;
import domain.Newspaper;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Services------------------------------------------------------------

	@Autowired
	private ChirpService chirpService;

	@Autowired
	private NewspaperService newspaperService;

	@Autowired
	private ArticleService articleService;
	
	@Autowired
	private AdvertisementService advertisementService;

	// Constructor--------------------------------------------------------------------

	public AdministratorController() {
		super();
	}


	// List chirps with tabooWords----------------------------------------------------------
		@RequestMapping("/tabooList")
		public ModelAndView chirpsWithTabooWords() {
			ModelAndView result;

			Collection<Chirp> chirpsWithTabooWords;
			Collection<Newspaper> newspapersWithTabooWords;
			Collection<Article> articlesWithTabooWords;
			Collection<Advertisement> advertisementsWithTabooWords;


			
			chirpsWithTabooWords = this.chirpService.chirpsContainsTabooWords();
			newspapersWithTabooWords = newspaperService.newspapersContainingTabooWords();
			articlesWithTabooWords = articleService.articlesContainsTabooWords();
			advertisementsWithTabooWords = advertisementService.advertisementsContainsTabooWords();

			result = new ModelAndView("administrator/tabooList");

			// Al modelo y la vista le a�adimos los siguientes atributos
			result.addObject("chirps", chirpsWithTabooWords);
			result.addObject("newspapers", newspapersWithTabooWords);
			result.addObject("articles", articlesWithTabooWords);
			result.addObject("advertisements",advertisementsWithTabooWords);
			result.addObject("requestURI", "administrator/tabooList.do");

			return result;
		}
}
