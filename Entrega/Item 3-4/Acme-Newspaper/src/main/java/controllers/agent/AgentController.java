/*
 * CustomerController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.agent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.AgentService;
import controllers.AbstractController;
import domain.Agent;
import forms.RegisterCustomer;

@Controller
@RequestMapping("/agent")
public class AgentController extends AbstractController {

	
	// Services ---------------------------------------------------------------
	
		@Autowired
		private AgentService agentService;
		
	// Constructors -----------------------------------------------------------
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		RegisterCustomer formObject;
		
		formObject = new RegisterCustomer();
		result = this.createEditModelAndView(formObject);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("userEditForm") RegisterCustomer customerEditForm, BindingResult binding){
		ModelAndView result;
		Agent agent;
		
		if(!customerEditForm.getTermsAccepted()){
			result = createEditModelAndView(customerEditForm);
			result.addObject("showMessageTerms", true);
			return result;
		}
		
		agent = agentService.reconstruct(customerEditForm, binding);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(customerEditForm);
			
		} else { 
			try {
				agentService.save(agent);
				result = new ModelAndView("redirect:/");
				
			} catch (Throwable oops) {
				result = createEditModelAndView(customerEditForm, "user.commit.error");
			}
		}
		return result;
	}
	
	protected ModelAndView createEditModelAndView(RegisterCustomer userEditForm) {
		ModelAndView result; 
		
		result = createEditModelAndView(userEditForm, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(RegisterCustomer userEditForm, String messageCode) {
		ModelAndView result;
		result = new ModelAndView("actor/agent/create");
		result.addObject("userEditForm", userEditForm);
		result.addObject("message", messageCode);
		result.addObject("requestURI", "agent/create.do");
		
		return result;
	}
	

}
