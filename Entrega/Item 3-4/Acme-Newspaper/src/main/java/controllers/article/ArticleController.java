/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.article;


import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ArticleService;
import services.NewspaperSubscriptionService;
import services.VolumeSubscriptionService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;
import domain.Article;
import domain.NewspaperSubscription;
import domain.VolumeSubscription;

@Controller
@RequestMapping("/article")
public class ArticleController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private ArticleService articleService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private NewspaperSubscriptionService newspaperSubscriptionService;
	
	@Autowired
	private VolumeSubscriptionService volumeSubscriptionService;
	
	// Constructors -----------------------------------------------------------

	public ArticleController() {
		super();
	}
	
	// Display ---------------------------------------------------------------
	@RequestMapping("/display")
	public ModelAndView display(@RequestParam(required = true) Integer articleId) {
		Actor principal;
		Article article;
		Boolean isCreator;
		ModelAndView result;
		
		principal = null;
		
		article = articleService.findOne(articleId);
		Assert.notNull(article);
		Assert.notNull(article.getNewspaper()); //Debe ser un art�culo, no un follow-up
		
		if(LoginService.isAuthenticated()){
			principal = actorService.findPrincipal();			
		}
		
		isCreator = LoginService.isAuthenticated() && principal.equals(article.getNewspaper().getCreator());
		
		Assert.isTrue(isCreator || !article.getDraft()); //Tan solo el creador puede ver los borradores
		
		if(article.getNewspaper().isIsPrivate()){ //L�gica de suscripciones
			Assert.isTrue(LoginService.isAuthenticated()); //Se debe estar autenticado
			
			NewspaperSubscription newspaperSubscription; //Suscripciones al peri�dico
			newspaperSubscription = newspaperSubscriptionService.getSubscriptionFromCustomerAndNewspaper(principal.getId(), article.getNewspaper().getId());
			
			Collection<VolumeSubscription> volumeSubscriptions; //Suscripciones a un volumen con este peri�dico
			volumeSubscriptions = volumeSubscriptionService.getVolumeSubscriptionsForNewspaperId(article.getNewspaper().getId(), principal.getId());
			
			Assert.isTrue(principal.equals(article.getNewspaper().getCreator()) || newspaperSubscription != null || 
						  volumeSubscriptions.size() > 0 || principal instanceof Administrator); //O eres el creador o tienes una suscripci�n de cualquier tipo
		}
		
		result = new ModelAndView("article/display");
		result.addObject("article", article);
		result.addObject("followUps", articleService.findFollowUpsByArticleId(articleId));
		result.addObject("isFollowable", isCreator && !article.getDraft());
		result.addObject("isEditable", isCreator && article.getDraft());
		result.addObject("isDeleteable", LoginService.isAuthenticated() && principal instanceof Administrator);

		return result;
	}
	
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(required = false) String keyword) {
		ModelAndView result;
		
		result = new ModelAndView("article/list");

		if(keyword == null){
			result.addObject("articles", articleService.findPublishedArticles());			
			
		} else{
			result.addObject("articles", articleService.searchArticlesByKeyword(keyword));			
		}
		
		return result;
	}
}
