package controllers.volume;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.NewspaperService;
import services.UserService;
import services.VolumeService;
import controllers.AbstractController;
import domain.Actor;
import domain.Newspaper;
import domain.User;
import domain.Volume;

@Controller
@RequestMapping("/volume/user")
public class VolumeUserController extends AbstractController {

	// Services------------------------------------------------------------

	@Autowired
	private VolumeService volumeService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	@Autowired
	private ActorService actorService;

	@Autowired
	private UserService userService;

	// Constructor--------------------------------------------------------------------

	public VolumeUserController() {
		super();
	}

	@RequestMapping("/create")
	public ModelAndView create(@RequestParam(value = "userId", required = true) Integer userId) {
		ModelAndView result;

		User user;
		
		user = userService.findOne(userId);
		Assert.notNull(user);
		
		Assert.isTrue(LoginService.isAuthenticated() && actorService.findPrincipal().equals((Actor) user)); //Comprobar credenciales
		
		result = new ModelAndView("volume/user/edit");
		result.addObject("volume", volumeService.create(user));
		
		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public ModelAndView edit(@RequestParam(value = "userId", required = true) Integer userId,
							 Volume volume, BindingResult binding) {
		ModelAndView result;

		volume = volumeService.reconstruct(volume, userId, binding);
		
		if(binding.hasErrors()){
			result = new ModelAndView("volume/user/edit");
			result.addObject("volume", volume);
			
		} else{
			try{
				volume = volumeService.save(volume);
				
				result = new ModelAndView("redirect:/volume/list.do?userId=" + userId);
				
			} catch (Throwable oops) {
				result = new ModelAndView("volume/user/edit");
				result.addObject("volume", volume);
				result.addObject("message", "volume.commit.error");
			}
		}
		
		return result;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(@RequestParam(value = "newspaperId", required = true) Integer newspaperId,
							@RequestParam(value = "volumeId", required = true) Integer volumeId) {
		ModelAndView result;
		
		Volume volume;
		Newspaper newspaper;
		
		volume = volumeService.findOne(volumeId);
		newspaper = newspaperService.findOne(newspaperId);
		
		Assert.notNull(volume);
		Assert.notNull(newspaper);

		result = new ModelAndView("redirect:/volume/display.do?volumeId=" + volume.getId());

		try{
			volumeService.addNewspaperToVolume(volume, newspaper);
						
		} catch (Throwable oops) {
			result.addObject("message", "volume.commit.error");
		}
	
		return result;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(value = "newspaperId", required = true) Integer newspaperId,
							   @RequestParam(value = "volumeId", required = true) Integer volumeId) {
		ModelAndView result;
		
		Volume volume;
		Newspaper newspaper;
		
		volume = volumeService.findOne(volumeId);
		newspaper = newspaperService.findOne(newspaperId);
		
		Assert.notNull(volume);
		Assert.notNull(newspaper);

		result = new ModelAndView("redirect:/volume/display.do?volumeId=" + volume.getId());

		try{
			volumeService.deleteNewspaperFromVolume(volume, newspaper);
						
		} catch (Throwable oops) {
			result.addObject("message", "volume.commit.error");
		}
	
		return result;
	}
}
