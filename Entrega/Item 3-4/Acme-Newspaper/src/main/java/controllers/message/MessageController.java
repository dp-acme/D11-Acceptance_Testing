/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.message;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.AdministratorService;
import services.FolderService;
import services.MessageService;
import controllers.AbstractController;
import domain.Actor;
import domain.Folder;
import domain.Message;

@Controller
@RequestMapping("/message")
public class MessageController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private FolderService			folderService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private MessageService			messageService;


	// Constructors -----------------------------------------------------------

	public MessageController() {
		super();
	}

	// Listing -----------------------------------------------------------
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam(required = true, value = "messageId") int messageId) {
		ModelAndView result;
		Message myMessage;
		Folder currentFolder;
		Collection<Folder> userFolders;

		UserAccount principal;
		Actor actor;

		principal = LoginService.getPrincipal();
		Assert.notNull(principal);
		actor = this.actorService.findByUserAccountId(principal.getId());
		Assert.notNull(actor);

		myMessage = this.messageService.findOne(messageId);
		Assert.notNull(myMessage);
		Assert.isTrue(myMessage.getSender().equals(actor) || myMessage.getRecipients().contains(actor));

		currentFolder = this.folderService.getFolderByActorAndMessage(actor.getId(), myMessage.getId());
		Assert.notNull(currentFolder);

		userFolders = this.folderService.getUserFolders(actor.getId());
		userFolders.remove(currentFolder);

		result = new ModelAndView("message/display");
		result.addObject("myMessage", myMessage);
		result.addObject("currentFolder", currentFolder);
		result.addObject("userFolders", userFolders);

		return result;
	}

	// Creation -----------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(defaultValue = "false", required = false) boolean isNotification) {
		ModelAndView result;
		Message message;
		Actor actor;
		UserAccount principal;

		principal = LoginService.getPrincipal();
		actor = this.actorService.findByUserAccountId(principal.getId());

		message = this.messageService.create(actor);

		result = this.createEditModelAndView(message);
		result.addObject("isNotification", isNotification);

		return result;
	}

	@RequestMapping(value = "/reply", method = RequestMethod.GET)
	public ModelAndView reply(@RequestParam(required = true, value = "messageId") int messageId) {
		ModelAndView result;
		Message message;
		Actor actor;
		UserAccount principal;

		principal = LoginService.getPrincipal();
		actor = this.actorService.findByUserAccountId(principal.getId());

		final Message oldMessage = this.messageService.findOne(messageId);
		Assert.notNull(oldMessage);
		Assert.isTrue(oldMessage.getRecipients().contains(actor));

		message = this.messageService.create(actor);

		final List<Actor> recipients = new ArrayList<Actor>();
		recipients.add(oldMessage.getSender());
		message.setRecipients(recipients);

		message.setSubject("Re: " + oldMessage.getSubject());

		result = this.createEditModelAndView(message);
		result.addObject("isNotification", false);

		return result;
	}

	// Edition -----------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "moveToFolder")
	public ModelAndView moveToFolder(HttpServletRequest request) {
		ModelAndView result;
		int messageId;
		Message message;
		Actor actor;
		UserAccount principal;
		Folder newFolder;
		int newFolderId;

		principal = LoginService.getPrincipal();
		Assert.notNull(principal);
		actor = this.actorService.findByUserAccountId(principal.getId());
		Assert.notNull(actor);

		messageId = Integer.parseInt(request.getParameter("messageId"));
		message = this.messageService.findOne(messageId);
		Assert.notNull(message);

		newFolderId = Integer.parseInt(request.getParameter("newFolder"));
		newFolder = this.folderService.findOne(newFolderId);
		Assert.notNull(newFolder);

		this.messageService.changeFolder(message, newFolder, actor);

		result = new ModelAndView("redirect:display.do?messageId=" + message.getId());

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "send")
	public ModelAndView save(HttpServletRequest request, @ModelAttribute(value = "myMessage") Message message, BindingResult binding) {
		ModelAndView result;
		boolean isNotification;
		
		isNotification = Boolean.parseBoolean(request.getParameter("isNotification"));
		
		message = messageService.reconstruct(message, binding, isNotification);

		if (binding.hasErrors())
			result = this.createEditModelAndView(message);
		else
			try {
				if (isNotification)
					this.administratorService.broadcastNotification(message.getSubject(), message.getBody(), message.getPriority(), message.getSender());
				else
					this.messageService.sendMessage(message, false);

				result = new ModelAndView("redirect:/folder/list.do");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(message, "message.commit.error");
			}
		result.addObject("isNotification", request.getParameter("isNotification"));

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(Message message, BindingResult binding) {
		ModelAndView result;
		Actor principal;
		Folder folder;
		
		message = messageService.findOne(message.getId());

		principal = this.actorService.findPrincipal();
		folder = this.folderService.getFolderByActorAndMessage(principal.getId(), message.getId());

		try {
			this.messageService.delete(message, principal);
			result = new ModelAndView("redirect:/folder/list.do?folderId=" + folder.getId());
		} catch (final Throwable oops) {
			result = new ModelAndView("redirect:display.do?messageId=" + message.getId());
		}

		return result;
	}

	// Ancillary methods -------------------------------------------------

	protected ModelAndView createEditModelAndView(Message message) {
		ModelAndView result;

		result = this.createEditModelAndView(message, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(Message myMessage, String messageCode) {
		ModelAndView result;
		Actor actor;
		Collection<Actor> actors;
		UserAccount principal;

		principal = LoginService.getPrincipal();
		actor = this.actorService.findByUserAccountId(principal.getId());

		actors = this.actorService.findAll();
		actors.remove(actor);

		result = new ModelAndView("message/edit");
		result.addObject("myMessage", myMessage);
		result.addObject("actors", actors);

		result.addObject("message", messageCode);

		return result;
	}

}
