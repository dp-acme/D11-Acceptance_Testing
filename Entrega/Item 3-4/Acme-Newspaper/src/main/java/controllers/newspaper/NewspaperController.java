package controllers.newspaper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ArticleService;
import services.NewspaperService;
import services.VolumeSubscriptionService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;
import domain.Advertisement;
import domain.Article;
import domain.Customer;
import domain.Newspaper;
import domain.User;

@Controller
@RequestMapping("/newspaper")
public class NewspaperController extends AbstractController{

	@Autowired
	private NewspaperService newspaperService;
	
	@Autowired
	private ArticleService articleService;
	
	@Autowired
	private VolumeSubscriptionService volumeSubscriptionService;
	
	@Autowired
	private ActorService actorService;
	
	@RequestMapping(value = "/list", method=RequestMethod.GET)
	public ModelAndView list(@RequestParam(required = false) String keyword){
		ModelAndView result;
		Set<Newspaper> newspapers = new HashSet<Newspaper>();
		
		result = new ModelAndView("newspaper/list");

		if(keyword == null){
			if(LoginService.isAuthenticated()) {
				Actor principal;
				
				principal = actorService.findPrincipal();
				
				if(principal instanceof User){
					result.addObject("myNewspaper", ((User) principal).getMyNewspapers());
				}
			}
			
			newspapers.addAll(newspaperService.getFinalPublicNewspapers());
			newspapers.addAll(newspaperService.getFinalPrivateNewspapers());
			
		}else{
			newspapers.addAll(newspaperService.searchNewspaperbyKeyword(keyword));
		}
		
		result.addObject("newspaper", new ArrayList<Newspaper>(newspapers));
		
		return result;
	}
	
	@RequestMapping(value = "/display", method=RequestMethod.GET)
	public ModelAndView display(@RequestParam int newspaperId){
		ModelAndView result;
		Newspaper newspaper;
		boolean suscribedCustomer = false;
		Collection<Article> articles; 
		Advertisement advertisement;
		
		newspaper = newspaperService.findOne(newspaperId);
		articles = articleService.findArticlesByNewspaperId(newspaper.getId());
		
		result = new ModelAndView("newspaper/display");

		if(LoginService.isAuthenticated()) {
				
				Actor principal;
				
				principal = actorService.findPrincipal();
		
				if(principal instanceof User){
					if(!newspaper.getCreator().equals((User) principal)){
						Assert.isTrue(!newspaper.isDraft());
					}else{
						result.addObject("creator" , true);
						for(Article a : articleService.findArticlesByNewspaperId(newspaperId)){
							if(a.getDraft()){
								result.addObject("allFinal", false);
								break;
							}
						}
					}
					
				} else if(principal instanceof Customer){
					Assert.isTrue(!newspaper.isDraft());
					if(newspaperService.getNewspapersSuscribed((Customer) principal).contains(newspaper) ||
					   volumeSubscriptionService.getVolumeSubscriptionsForNewspaperId(newspaperId, principal.getId()).size() > 0)
						suscribedCustomer = true;
					result.addObject("suscribed", suscribedCustomer);
					
				} else if(principal instanceof Administrator){
					Assert.isTrue(!newspaper.isDraft());
				}
				
		}else{
			Assert.isTrue(!newspaper.isDraft());
		}
		
		
		advertisement = newspaperService.getRandomAdvertisement(newspaperId);
		if(advertisement != null){
			result.addObject("banner", advertisement.getBanner());
			result.addObject("target", advertisement.getTarget());
		}else{
			result.addObject("banner", "");
			result.addObject("target", "");
		}
		
		result.addObject("newspaper", newspaper);
		result.addObject("articles", articles);

		return result;
	}
	
	@RequestMapping("/user/create")
	public ModelAndView create(){
		ModelAndView result;

		result = new ModelAndView("newspaper/user/edit");
		result.addObject("newspaper", newspaperService.create());

		return result;
	}
	
	@RequestMapping("/user/edit")
	public ModelAndView edit(@RequestParam(required = false) Integer newspaperId) {
		Newspaper newspaper;
		ModelAndView result;
		
		newspaper = newspaperService.findOne(newspaperId);
		
		Assert.notNull(newspaper);
		Assert.isTrue(LoginService.isAuthenticated() && actorService.findPrincipal().equals(newspaper.getCreator()));
		Assert.isTrue(newspaper.isDraft());
		
		result = new ModelAndView("newspaper/user/edit");
		result.addObject("newspaper", newspaper);

		return result;
	}
	
	@RequestMapping(value = "/user/edit", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute(value="newspaper") Newspaper newspaper, BindingResult binding){
		ModelAndView result;
		Newspaper newspaperRes;
		
		newspaperRes = newspaperService.reconstruct(newspaper, binding);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(newspaperRes);
		} else { 
			try {
				newspaperRes = newspaperService.save(newspaperRes);
				
				result = new ModelAndView("redirect:/newspaper/display.do?newspaperId=" + newspaperRes.getId());
				
			} catch (Throwable oops) {
				result = createEditModelAndView(newspaper, "newspaper.commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping("/user/publish")
	public ModelAndView publish(@RequestParam(required = true) Integer newspaperId) {
		Newspaper newspaper;
		ModelAndView result;
		
		newspaper = newspaperService.findOne(newspaperId);
		
		Assert.notNull(newspaper);
		Assert.isTrue(newspaper.isDraft()); //Debe estar en draft
		
		Assert.isTrue(LoginService.isAuthenticated() && actorService.findPrincipal().equals(newspaper.getCreator())); //Comprobar credenciales
		
		result = new ModelAndView("redirect:/newspaper/display.do?newspaperId=" + newspaper.getId());	

		try{
			newspaperService.publish(newspaper.getId());
			
		} catch (Throwable oops) {
			result = createEditModelAndView(newspaper, "newspaper.commit.error");
		}

		return result;
	}
	
	@RequestMapping("/admin/delete")
	public ModelAndView delete(@RequestParam(required = true) Integer newspaperId) {
		Newspaper newspaper;
		ModelAndView result;
		
		newspaper = newspaperService.findOne(newspaperId);
		Assert.notNull(newspaper);
		
		Assert.isTrue(LoginService.isAuthenticated() && actorService.findPrincipal() instanceof Administrator); //Comprobar credenciales
		
		try{
			newspaperService.delete(newspaper);
			result = new ModelAndView("redirect:/newspaper/list.do");	
			
		} catch (Throwable oops) {
			result = new ModelAndView("redirect:/newspaper/display.do?newspaperId=" + newspaper.getId());	
			result = createEditModelAndView(newspaper, "newspaper.commit.error");
		}

		return result;
	}
	
	protected ModelAndView createEditModelAndView(Newspaper newspaper) {
		ModelAndView result;
		
		result = createEditModelAndView(newspaper, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(Newspaper newspaper, String messageCode) {
		ModelAndView result;
		
		result = new ModelAndView("newspaper/user/edit");
		result.addObject("newspaper", newspaper);
		result.addObject("message", messageCode);
		
		return result;
	}
}
