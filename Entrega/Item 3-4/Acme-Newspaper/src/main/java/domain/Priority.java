
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

@Embeddable
@Access(AccessType.PROPERTY)
public class Priority {

	// Constructors -----------------------------------------------------------

	public Priority() {
		super();
	}

	// Values -----------------------------------------------------------------

	public static final String	HIGH	= "HIGH";
	public static final String	NEUTRAL	= "NEUTRAL";
	public static final String	LOW		= "LOW";

	// Attributes -------------------------------------------------------------

	private String				priority;

	@NotBlank
	@Pattern(regexp = "^" + Priority.HIGH + "|" + Priority.NEUTRAL + "|" + Priority.LOW + "$")
	public String getPriority() {
		return this.priority;
	}
	public void setPriority(final String priority) {
		this.priority = priority;
	}
}
