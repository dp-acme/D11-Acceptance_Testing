<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="advertisement/agent/edit.do?newspaperId=${param.newspaperId}" modelAttribute="advertisement" >

	<!-- Indicamos los campos de los formularios -->


	<acme:textbox code="advertisement.edit.title" path="title" required="required" />
	<br />
	
	<acme:textbox code="advertisement.edit.banner" path="banner" required="required" />
	<br />
	
	<acme:textbox code="advertisement.edit.target" path="target" required="required" />
	<br />

	<acme:textbox code="advertisement.edit.holderName" path="creditCard.holderName" required="required" />
	<br />
	
	<acme:textbox code="advertisement.edit.brandName" path="creditCard.brandName" required="required" />
	<br />

	<acme:textbox code="advertisement.edit.creditCardNumber" path="creditCard.creditCardNumber" required="required" />
	<br />

	<acme:textbox type="number" code="advertisement.edit.expirationMonth" path="creditCard.expirationMonth" required="required" placeholderCode="advertisement.edit.expirationMonth.placeholder" />
	<br />
	
	<acme:textbox type="number" code="advertisement.edit.expirationYear" path="creditCard.expirationYear" required="required" placeholderCode="advertisement.edit.expirationYear.placeholder" />
	<br />
	
	<acme:textbox code="advertisement.edit.cvv" path="creditCard.cvv" required="required" placeholderCode="advertisement.edit.cvv.placeholder" />
	
	<jstl:if test="${showMessageCreditCard !=null && showMessageCreditCard}">
			<p class="error"><spring:message code="advertisement.edit.notValidCreditCard"/></p>
	</jstl:if>
	<br />
	
	<!-- Botones del formulario -->
	<acme:submit name="save" code="advertisement.edit.save" />
	<acme:cancel url="newspaper/display.do?newspaperId=${param.newspaperId}" code="advertisement.edit.cancel" />

</form:form>