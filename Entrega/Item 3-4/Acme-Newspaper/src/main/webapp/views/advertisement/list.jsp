<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<display:table name="advertisements" id="row" requestURI="${requestURI}"
	pagesize="5">


	<display:column property="title" titleKey="advertisements.list.title" />

	<display:column titleKey="advertisement.list.newspaper">
		<jstl:out value="${row.newspaper.title}" />
	</display:column>

	<display:column titleKey="advertisement.list.user">
		<jstl:out value="${row.agent.name}" />
	</display:column>

	<%--Mostramos la columna de display --%>

	<security:authorize access="hasRole('ADMIN')">
		<jstl:if test="${requestURI == 'advertisement/admin/list.do'}">
				<display:column title="" sortable="false">
					<a href="advertisement/admin/delete.do?advertisementId=${row.getId()}"> <spring:message
							code="advertisement.list.delete" />
					</a>
				</display:column>
		</jstl:if>
	</security:authorize>

</display:table>
<br />
