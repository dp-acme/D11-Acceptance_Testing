<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jstl:set value="${not empty param.userId}" var="hasUser"/>

<display:table name="volumes" id="row" requestURI="volume/list.do"> 
	<jstl:if test="${!hasUser}">
		<spring:message code="volume.creator"  var="creatorHeader"/>
		<display:column title="${creatorHeader}" property="creator.name"/>
	</jstl:if>
	
	<spring:message code="volume.table.title"  var="titleHeader"/>
	<display:column title="${titleHeader}">
		<a href="volume/display.do?volumeId=${row.id}">
			<jstl:out value="${row.title}" />
		</a>
	</display:column>
	
	<spring:message code="volume.table.description"  var="descriptionHeader"/>
	<display:column title="${descriptionHeader}" property="description"/>
	
	<spring:message code="volume.table.year"  var="yearHeader"/>
	<display:column title="${yearHeader}" property="year"/>
</display:table>

<jstl:if test="${hasUser}">
	<spring:message code="volume.button.create" var="createButton" />
	<input type="button" value="${createButton}" onclick="relativeRedir('volume/user/create.do?userId=${param.userId}')" />
</jstl:if>