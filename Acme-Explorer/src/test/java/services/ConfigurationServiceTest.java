
package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Configuration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class ConfigurationServiceTest extends AbstractTest {

	@Autowired
	private ConfigurationService configurationService;
	
	@Test
	public void findConfigurationTest() {
		Configuration config;
		
		config = configurationService.findConfiguration();
		
		Assert.notNull(config);
	}
			
	@Test
	public void saveConfigurationTestAdmin() {
		Configuration config;
		Double vatStart, vatEnd;
		
		authenticate("admin");
		
		config = configurationService.findConfiguration();
		vatStart = config.getVat();
		config.setVat(.50);
		
		configurationService.save(config);
		
		config = configurationService.findConfiguration();
		vatEnd = config.getVat();
		
		Assert.isTrue(vatStart != vatEnd && vatEnd == .50);
	}
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void saveConfigurationTestNotAdmin() {
		Configuration config;
		Double vatStart, vatEnd;
		
		authenticate("explorer1");
		
		config = configurationService.findConfiguration();
		vatStart = config.getVat();
		config.setVat(50.);
		
		configurationService.save(config);
		
		config = configurationService.findConfiguration();
		vatEnd = config.getVat();
		
		Assert.isTrue(vatStart != vatEnd && vatEnd == 50.);
	}
	
}
