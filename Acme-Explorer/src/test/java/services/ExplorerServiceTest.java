package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;
import domain.ApplyFor;
import domain.Explorer;
import domain.Status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class ExplorerServiceTest extends AbstractTest{
	
	@Autowired 
	private ExplorerService explorerService;
	
	
	@Test
	public void saveNewExplorer(){
		Explorer result;
		UserAccount userAccount;
		Authority auth;
		Collection<Authority> auths;
		
		auth = new Authority();
		userAccount = new UserAccount();
		auths = new ArrayList<Authority>();
		auth.setAuthority(Authority.EXPLORER);
		auths.add(auth);
		userAccount.setAuthorities(auths);
		userAccount.setUsername("testExplorer1");
		userAccount.setPassword("test1");
		
		result = explorerService.create();
		result.setUserAccount(userAccount);
		result.setName("Explorer1");
		result.setSurname("Explorer1");
		result.setEmail("explorer@acme.es");
		result = explorerService.save(result);
		
		Assert.isTrue(explorerService.findAll().contains(result));
		Assert.isTrue(result.getFolders().size()==5);
	}
	
	@Test
	public void editExplorerAuth(){
		Explorer result;
		List<Explorer> explorers;
		
		this.authenticate("explorer1");
		explorers = (List<Explorer>) explorerService.findAll();
		result = explorers.get(0); // Es el explorer1;
		
		result = explorerService.save(result);
		
		Assert.isTrue(explorerService.findAll().contains(result));
	}
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void editExplorerBadAuth(){
		Explorer result;
		List<Explorer> explorers;
		
		this.authenticate("explorer1");
		explorers = (List<Explorer>) explorerService.findAll();
		result = explorers.get(1); // No es el explorer1;
		result = explorerService.save(result);
	}
	
	@Test
	public void findAllExplorerTest(){
		List<Explorer> explorers;
		
		explorers = (List<Explorer>) explorerService.findAll();

		Assert.notNull(explorers);	
	}
	
	@Test
	public void findOneExplorerTest(){
		List<Explorer> explorers;
		Explorer explorer;
		
		explorers = (List<Explorer>) explorerService.findAll();
		Assert.notNull(explorers);
		explorer = explorers.get(0);

		Assert.isTrue(explorerService.findAll().contains(explorerService.findOne(explorer.getId())));
	}
	
	//Other methods
	
	@Test
	public void cancelApplyFor(){
		ApplyFor result;
		Explorer explorer;
		
		this.authenticate("explorer2");
		explorer = null;
		result = null;
		for(Explorer e: explorerService.findAll()){
			for(ApplyFor ap : e.getApplications()){
				if(ap.getTrip().getStartDate().after(new Date())&&ap.getStatus().equals(Status.ACCEPTED)){
					result = ap;
					explorer = e;
					break;
				}
			}
			if(result!=null){
				break;
			}
		}
		if(explorer!=null&&result!=null){
		super.authenticate(explorer.getUserAccount().getUsername().toString());
		
		result = explorerService.cancelApplyFor(result.getTrip().getId());
		Assert.isTrue(result.getStatus().getStatus().equals(Status.CANCELLED));
		}
	}

}
