
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.LoginService;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Auditor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AuditorServiceTest extends AbstractTest {

	@Autowired
	private AuditorService	auditorService;
	
	@Autowired
	private ActorService	actorService;

	@Test
	public void createAuditorTest() {
		Auditor result;
		UserAccount userAccount;
		Authority auth;
		Collection<Authority> auths;
		
		authenticate("admin");
		
		auth = new Authority();
		userAccount = new UserAccount();
		auths = new ArrayList<Authority>();
		auth.setAuthority(Authority.AUDITOR);
		auths.add(auth);
		userAccount.setPassword("auditorPass");
		userAccount.setUsername("auditorusername");
		userAccount.setAuthorities(auths);
		
		result = auditorService.create();
		result.setName("Auditor1");
		result.setSurname("Auditor1");
		result.setEmail("auditor@acme.es");
		result.setUserAccount(userAccount);
		
		result = auditorService.save(result);
		
		Assert.isTrue(auditorService.findAll().contains(result));
	}
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void createAuditorTestNoAdmin() {
		Auditor result;
		UserAccount userAccount;
		Authority auth;
		Collection<Authority> auths;
		
		auth = new Authority();
		userAccount = new UserAccount();
		auths = new ArrayList<Authority>();
		auth.setAuthority(Authority.AUDITOR);
		auths.add(auth);
		userAccount.setAuthorities(auths);
		userAccount.setUsername("testAuditor1");
		userAccount.setPassword("test1");
		
		result = auditorService.create();
		result.setName("Auditor1");
		result.setSurname("Auditor1");
		result.setEmail("auditor@acme.es");
		
		result = auditorService.save(result);
		
		Assert.isTrue(auditorService.findAll().contains(result));
		Assert.isTrue(result.getFolders().size()==5);
	}
	
	@Test
	public void findAllTest() {
		Collection<Auditor> auditors;
		
		auditors = auditorService.findAll();
		
		Assert.notNull(auditors);
		Assert.notEmpty(auditors);
	}
	
	@Test
	public void findOneTest() {
		Auditor auditor;
		List<Auditor> auditors;
		
		auditors = (List<Auditor>)auditorService.findAll();
		auditor = auditors.get(0);
		
		Assert.isTrue(auditorService.findOne(auditor.getId()).equals(auditor));		
	}
	
	@Test
	public void saveAuditorTest() {
		Auditor auditor, result;
		String address, phone;
		
		authenticate("auditor1");
		
		auditor = (Auditor)actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		address = auditor.getAddress();
		phone = auditor.getPhone();
		
		auditor.setAddress("Test address");
		auditor.setPhone("616666666");
		result = auditorService.save(auditor);
		
		Assert.isTrue(auditorService.findAll().contains(result));
		Assert.isTrue(address != result.getAddress() && phone != result.getPhone());
	}
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void saveAuditorTestNoPrincipal() {
		Auditor auditor, result;
		String address, phone;
		
		authenticate("auditor1");
		
		auditor = (Auditor)actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		authenticate(null);
		authenticate("explorer1");
		
		address = auditor.getAddress();
		phone = auditor.getPhone();
		
		auditor.setAddress("Test address");
		auditor.setPhone("616666666");
		result = auditorService.save(auditor);
		
		Assert.isTrue(auditorService.findAll().contains(result));
		Assert.isTrue(address != result.getAddress() && phone != result.getPhone());
	}
	
}
