package services;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.ApplyFor;
import domain.Explorer;
import domain.Status;
import domain.Story;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class StoryServiceTest extends AbstractTest{

	@Autowired 
	private StoryService storyService;
	
	@Autowired 
	private ApplyForService applyForService;
	
	//***CRUD TEST*****
	
	@Test
	public void testFindAll(){
		Collection<Story> result;
		
		result = storyService.findAll();
		
		Assert.notNull(result);
	}
	
	@Test
	public void testFindOne(){
		List<Story> all;
		Story storyFromList, storyFromRepository;
		
		all = (List<Story>) storyService.findAll();
		Assert.notEmpty(all);
		storyFromList = all.get(0);
		
		storyFromRepository = storyService.findOne(storyFromList.getId());
		
		Assert.notNull(storyFromRepository);
		Assert.isTrue(storyFromList.equals(storyFromRepository));
	}
	
	@Test
	public void testSaveGoodApplication(){
		List<ApplyFor> applications;
		Status status;
		Trip trip;
		Explorer explorer;
		Story story;
		
		status = new Status();
		status.setStatus(Status.ACCEPTED);
		
		applications = (List<ApplyFor>) applyForService.filterApplicationsByStatus(status);
		
		Assert.notEmpty(applications);
		
		explorer = applications.get(0).getExplorer();
		trip = applications.get(0).getTrip();
		
		this.authenticate(explorer.getUserAccount().getUsername());
		
		story = storyService.create(explorer, trip);
		story.setTitle("T�tulo");
		story.setText("Texto");
		
		story = storyService.save(story);
		
		this.unauthenticate();

		Assert.isTrue(storyService.findAll().contains(story));
		Assert.isTrue(explorer.getStories().contains(story));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSaveBadApplication(){
		List<ApplyFor> applications;
		Status status;
		Trip trip;
		Explorer explorer;
		Story story;
		
		status = new Status();
		status.setStatus(Status.ACCEPTED);
		
		applications = (List<ApplyFor>) applyForService.filterApplicationsByDifferentStatus(status);
		
		Assert.notEmpty(applications);
		
		explorer = applications.get(0).getExplorer();
		trip = applications.get(0).getTrip();
		
		this.authenticate(explorer.getUserAccount().getUsername());
		
		story = storyService.create(explorer, trip);
		story.setTitle("T�tulo");
		story.setText("Texto");
		
		story = storyService.save(story);
				
		this.unauthenticate();

		Assert.isTrue(storyService.findAll().contains(story));
		Assert.isTrue(explorer.getStories().contains(story));
	}
}
