package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.LoginService;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Actor;
import domain.Manager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class ActorServiceTest  extends AbstractTest{

	@Autowired 
	private ActorService actorService;
	
	@Autowired 
	private ManagerService managerService;
	
	
	@Test
	public void testFindAll(){
		Collection<Actor> result;
		
		result = actorService.findAll();
		
		Assert.notNull(result);
		Assert.notEmpty(result);
	}
	
	@Test
	public void testFindOne(){
		List<Actor> all;
		Actor actorFromList, actorFromRepository;
		
		all = (List<Actor>) actorService.findAll();
		Assert.notEmpty(all);
		actorFromList = all.get(0);
		
		actorFromRepository = actorService.findOne(actorFromList.getId());
		
		Assert.notNull(actorFromRepository);
		Assert.isTrue(actorFromList.equals(actorFromRepository));
	}
	
	@Test
	public void testSave(){
		Actor actor;
		List<Actor> actors;
		String nameOld, nameNew;
		
		actors = (List<Actor>)actorService.findAll();
		actor = actors.get(0);
		
		nameOld = actor.getName();
		
		actor.setName("newName");
		actorService.save(actor);
		
		actor = actorService.findOne(actor.getId());
		nameNew = actor.getName();
		
		Assert.isTrue(!nameOld.equals(nameNew));
		Assert.isTrue(nameNew.equals("newName"));
	}
	
	@Test
	public void findByUserAccountIdTest(){
		Actor actor, result;
		UserAccount principal;
		List<Actor> actors;
		
		actors = (List<Actor>)actorService.findAll();
		actor = actors.get(0);
		
		authenticate(actor.getUserAccount().getUsername());
		
		principal = LoginService.getPrincipal();
		
		result = actorService.findByUserAccountId(principal.getId());
		
		Assert.isTrue(actor.equals(result));
	}
	
	@Test
	public void addSystemFoldersTest(){
		int foldersOld, foldersNew;		
		Manager manager, result;
		List<Authority> authorities;
		Authority auth;
		UserAccount userAccount;
		
		authenticate("admin");

		authorities = new ArrayList<Authority>();
		auth = new Authority();
		auth.setAuthority(Authority.MANAGER);
		authorities.add(auth);
		userAccount = new UserAccount();
		
		userAccount.setUsername("testUsername");
		userAccount.setPassword("testPassword");
		userAccount.setAuthorities(authorities);
		
		manager = managerService.create();
		manager.setAddress("address");
		manager.setEmail("email@acme.es");
		manager.setName("testManager");
		manager.setPhone("616111111");
		manager.setSurname("testSurname");
		manager.setUserAccount(userAccount);
		
		foldersOld = manager.getFolders().size();
		
		result = managerService.save(manager);
		
		result = managerService.findOne(result.getId());
		foldersNew = result.getFolders().size();

		Assert.isTrue(foldersOld+5 == foldersNew);
	}
}
