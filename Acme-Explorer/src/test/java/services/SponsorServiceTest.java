package services;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Actor;
import domain.Sponsor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/datasource.xml",
		"classpath:spring/config/packages.xml" })
@Transactional
public class SponsorServiceTest extends AbstractTest {
	@Autowired
	private SponsorService sponsorService;

	@Autowired
	private ActorService actorService;
	
	
	@Test
	public void testSaveNewSponsor(){
		Sponsor sponsor, result;
		UserAccount userAccount;
		List<Sponsor> sponsors;
		Authority auth;
		List<Authority> auths;
		
		super.authenticate("admin");
		auth = new Authority();
		userAccount = new UserAccount();
		auths = new ArrayList<Authority>();
		auth.setAuthority(Authority.SPONSOR);
		auths.add(auth);
		userAccount.setAuthorities(auths);
		userAccount.setUsername("sponsorTest");
		userAccount.setPassword("sponsorTest");
		
		sponsor = sponsorService.create();
		sponsor.setUserAccount(userAccount);
		sponsor.setName("SponsorTest");
		sponsor.setSurname("SponsorTest");
		sponsor.setEmail("sponsortest@email.com");

		result = sponsorService.save(sponsor);
		sponsors = new ArrayList<Sponsor>(sponsorService.findAll());
		Assert.isTrue(sponsors.contains(result));
		Assert.isTrue(result.getFolders().size()==5);
		super.authenticate(null);
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void testSaveNewSponsorBadAuth(){
		Sponsor sponsor, result;
		UserAccount userAccount;
		List<Sponsor> sponsors;
		Authority auth;
		List<Authority> auths;
		
		super.authenticate("manager1");
		auth = new Authority();
		userAccount = new UserAccount();
		auths = new ArrayList<Authority>();
		auth.setAuthority(Authority.SPONSOR);
		auths.add(auth);
		userAccount.setAuthorities(auths);
		userAccount.setUsername("sponsorTest");
		userAccount.setPassword("sponsorTest");
		
		sponsor = sponsorService.create();
		sponsor.setUserAccount(userAccount);
		sponsor.setName("SponsorTest");
		sponsor.setSurname("SponsorTest");
		sponsor.setEmail("sponsortest@email.com");

		result = sponsorService.save(sponsor);
		sponsors = new ArrayList<Sponsor>(sponsorService.findAll());
		Assert.isTrue(sponsors.contains(result));
		Assert.isTrue(result.getFolders().size()==5);
		super.authenticate(null);
	}
	
	@Test
	public void testSaveExistingSponsor(){
		Sponsor sponsor, result;
		List<Actor> actors;
		List<Sponsor> sponsors;
		
		sponsors = (List<Sponsor>) sponsorService.findAll();
		sponsor = sponsors.get(0);
		
		super.authenticate(sponsor.getUserAccount().getUsername());
		
		
		sponsor.setAddress("Calle prueba");
		sponsor.setName("sponsorTestCambiado");
		sponsor.setSurname("sponsorTestCambiado");
		
		result= sponsorService.save(sponsor);
		
		actors = new ArrayList<Actor>(actorService.findAll());
		Assert.isTrue(actors.contains(result));
		Assert.isTrue(result.getAddress().equals("Calle prueba"));
		Assert.isTrue(result.getName().equals("sponsorTestCambiado"));
		
		super.authenticate(null);
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void testSaveExistingSponsorBadAuth(){
		Sponsor sponsor, result;
		List<Actor> actors;
		List<Sponsor> sponsors;
		
		sponsors = (List<Sponsor>) sponsorService.findAll();
		sponsor = sponsors.get(0);
		
		super.authenticate("manager1");
		
		
		sponsor.setAddress("Calle prueba");
		sponsor.setName("sponsorTestCambiado");
		sponsor.setSurname("sponsorTestCambiado");
		
		result= sponsorService.save(sponsor);
		
		actors = new ArrayList<Actor>(actorService.findAll());
		Assert.isTrue(actors.contains(result));
		Assert.isTrue(result.getAddress().equals("Calle prueba"));
		Assert.isTrue(result.getName().equals("sponsorTestCambiado"));
		
		super.authenticate(null);
	}
	
	@Test
	public void testFindOneSponsor(){
		List<Sponsor> sponsors;
		Sponsor sponsor;
		
		sponsors= (List<Sponsor>) sponsorService.findAll();
		sponsor = sponsorService.findOne(sponsors.get(0).getId());
		
		Assert.isTrue(sponsors.contains(sponsor));
		
	}
	
	@Test
	public void testFindAll(){
		List<Sponsor> sponsors;
		
		sponsors = (List <Sponsor>)sponsorService.findAll();
		Assert.isTrue(sponsors.size()==2);
	}
	
	
	

}
