package services;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Record;

import utilities.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class RecordServiceTest extends AbstractTest{
	
	
	@Autowired
	private RecordService recordService;
	
	@Test
	public void findAllRecordsTest(){
		List<Record> records;
		
		records = (List<Record>) recordService.findAll();

		Assert.notNull(records);	
	}
	
	@Test
	public void findRecordTest(){
		List<Record> records;
		Record record;
		
		records = (List<Record>) recordService.findAll();
		Assert.notNull(records);
		record = records.get(0);

		Assert.isTrue(recordService.findAll().contains(recordService.findOne(record.getId())));
	}

}
