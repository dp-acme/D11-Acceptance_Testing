package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.EducationRepository;
import security.LoginService;
import security.UserAccount;
import domain.Curriculum;
import domain.Education;

@Service
@Transactional
public class EducationService {
	
	// Managed repository ---------------------------------------------------
	
	@Autowired
	private EducationRepository educationRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private CurriculumService curriculumService;
	
	// Constructor ---------------------------------------------------
	
	public EducationService(){
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------

	public Education create(Curriculum curriculum) {
		UserAccount principal;
		Education result;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(curriculum.getRanger().getUserAccount().equals(principal));
		
		result = new Education();
		result.setCurriculum(curriculum);
		
		return result;
	}
	
	public Collection<Education> findAll() {
		Collection<Education> result;
		
		result = educationRepository.findAll();

		Assert.notNull(result);

		return result;
	}

	public Education findOne(int educationId) {
		Assert.isTrue(educationId != 0);		

		Education result;
		
		result = educationRepository.findOne(educationId);
		
		return result;
	}
	
	public Education save(Education education){
		Assert.notNull(education);
		
		UserAccount principal;
		Education result;
		Curriculum curriculum;
		
		curriculum = education.getCurriculum();
		principal = LoginService.getPrincipal();
		Assert.isTrue(education.getCurriculum().getRanger().getUserAccount().equals(principal));
		
		if (education.getStudiesEnd() != null){
			Assert.isTrue(education.getStudiesStart().before(education.getStudiesEnd()));			
		}

		result = educationRepository.save(education);

		if (education.getId() == 0) {
			curriculum.getEducationRecords().add(result);
			curriculumService.save(curriculum);
		}
				
		return result;
	}
	
	public void delete(Education education){
		Assert.notNull(education);
		
		UserAccount principal;
		Curriculum curriculum;
		
		curriculum = education.getCurriculum();
		principal = LoginService.getPrincipal();
		Assert.isTrue(curriculum.getRanger().getUserAccount().equals(principal));
		Assert.isTrue(educationRepository.exists(education.getId()));
		
		curriculum.getEducationRecords().remove(education);
		curriculumService.save(curriculum);
		
		educationRepository.delete(education);
	}
	
	public void deleteAllRecordsOfCurriculum(Curriculum c){
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(c.getRanger().getUserAccount().equals(principal));

		educationRepository.deleteInBatch(c.getEducationRecords());
	}
}
