package services;

import java.text.DecimalFormat;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ConfigurationRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.ApplyFor;
import domain.AuditRecord;
import domain.Auditor;
import domain.Configuration;
import domain.Curriculum;
import domain.Education;
import domain.Endorser;
import domain.Explorer;
import domain.Folder;
import domain.Manager;
import domain.Message;
import domain.Miscellaneous;
import domain.Note;
import domain.Professional;
import domain.Ranger;
import domain.SocialIdentity;
import domain.Sponsor;
import domain.Sponsorship;
import domain.Stage;
import domain.Status;
import domain.SurvivalClass;
import domain.Trip;

@Service
@Transactional
public class ConfigurationService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private ConfigurationRepository configurationRepository;
	
	// Supporting services ---------------------------------------------------

	@Autowired
	private MessageService messageService;
	
	@Autowired
	private SurvivalClassService survivalClassService;
	
	@Autowired
	private ApplyForService applyForService;
	
	@Autowired
	private NoteService noteService;

	// Constructor ---------------------------------------------------
	
	public ConfigurationService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Configuration findConfiguration() {
		Configuration result;
		
		result = configurationRepository.findConfiguration();
		
		return result;		
	}
	
	public Configuration save(Configuration configuration) {
		Assert.notNull(configuration);
		
		Configuration result;
		Authority auth;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.ADMINISTRATOR);
		
		Assert.isTrue(principal.getAuthorities().contains(auth));
		
		result = configurationRepository.save(configuration);
		
		return result;
	}
	
	// Other business methods ---------------------------------------------------
	
	public String roundTo(Double n, Integer decimals){
		DecimalFormat format;
		
		format = new DecimalFormat();
		format.setMaximumFractionDigits(decimals);
		
		return format.format(n);
	}
	
	public String[] roundTo(Double[] ns, Integer decimals){
		String[] result;
		
		result = new String[ns.length];
		
		for(int i = 0; i < result.length; i ++){
			result[i] = roundTo(ns[i], decimals);
		}
		
		return result;
	}
	
	public boolean isSpam(String content){ //Comprueba si content es spam
		Collection<String> spamWords;
		boolean result;
		
		if(content == null){
			result = false;
			
		} else{
			spamWords = findConfiguration().getSpamWords();
			result = false;
			content = content.toLowerCase();
			for(String word : spamWords){
				word = word.toLowerCase();
				if(content.contains(word)){
					result = true;
					break;
				}
			}	
		}
		
		return result;
	}
	
	public boolean hasSpam(Actor actor){
		boolean result;
		
		result = isSpam(actor.getName());
		
		if(!result){
			result = isSpam(actor.getSurname());
		}
		
		if(!result){
			result = isSpam(actor.getPhone()); //Spam en el tel�fono POR SI ACASO
		}
		
		if(!result){
			result = isSpam(actor.getEmail());
		}
		
		if(!result){
			result = isSpam(actor.getAddress());
		}
		
		if(!result){
			result = isSpam(actor.getUserAccount().getUsername());
		}
		
		if(!result){
			for(Folder f : actor.getFolders()){
				if(isSpam(f.getName())){
					result = true;
					break;
				}
			}
		}
		
		if(!result){
			for(SocialIdentity s : actor.getSocialIdentities()){
				if(isSpam(s.getNetName()) || isSpam(s.getLink()) || isSpam(s.getNick()) || isSpam(s.getPhoto())){
					result = true;
					break;
				}
			}
		}
		
		if(!result){
			Collection<Message> messages;
			
			messages = messageService.getMessagesBySenderId(actor.getId());
			
			for(Message m : messages){ //Iterar sobre los mensajes que haya enviado
				if(isSpam(m.getSubject()) || isSpam(m.getBody())){
					result = true;
					break;
				}
			}
		}
		
		if(!result && actor instanceof Explorer){ //Caso de Explorer
			result = hasSpam((Explorer) actor);
			
		} else if(!result && actor instanceof Explorer){ //Caso de Manager
			result = hasSpam((Manager) actor);
			
		} else if(!result && actor instanceof Sponsor){ //Caso de Sponsor
			result = hasSpam((Sponsor) actor);
			
		} else if(!result && actor instanceof Ranger){ //Caso de Ranger
			result = hasSpam((Ranger) actor);
			
		} else if(!result && actor instanceof Ranger){ //Caso de Auditor
			result = hasSpam((Auditor) actor);
		}
			
		return result;
	}
	
	public boolean hasSpam(Explorer explorer){	
		boolean result;
		
		result = false;
		
		for(ApplyFor a : explorer.getApplications()){
			if(isSpam(a.getComments())){ //Demos por hecho que no vas a meter spam en la tarjeta de cr�dito
				result = true;
				break;
			}
		}
				
		return result;
	}
	
	public boolean hasSpam(Manager manager){	
		boolean result;
		
		result = false;
		
		for(Trip t : manager.getTrips()){ //Iterar sobre los Trips del manager
			for(Stage s : t.getStages()){
				if(isSpam(s.getTitle()) || isSpam(s.getDescription())){ //Comprobar las Stages
					result = true;
					break;
				}
			}
			
			if(!result){
				for(SurvivalClass s : survivalClassService.getSurvivalClassesbyTripId(t.getId())){ //TODO: QUery
					if(isSpam(s.getTitle()) || isSpam(s.getDescription())){ //Comprobar las SuvivalClasses
						result = true;
						break;
					}
				}	
			}
			
			if(!result){
				for(Note n : noteService.getNotesByTrip(t.getId())){ //Comprobar las notas contestadas
					if(n.getManagerReply() != null && isSpam(n.getManagerReply())){
						result = true;
						break;
					}
				}	
			}
			
			if(!result && isSpam(t.getTitle()) || isSpam(t.getDescription()) || isSpam(t.getRequirements()) //Comprobar atributos b�sicos del Trip
					|| (t.getCancellationReason() != null && isSpam(t.getCancellationReason()))){
				result = true;
			}
			
			if(!result){
				for(ApplyFor a : applyForService.getApplicationsByTrip(t.getId())){ //Comprobar las rejectedReason de los ApplyFor
					if(a.getStatus().getStatus().equals(Status.REJECTED) && isSpam(a.getRejectedReason())){
						result = true;
						break;
					}
				}				
			}
			
			if(result){
				break;
			}
		}
				
		return result;
	}
	
	public boolean hasSpam(Sponsor sponsor){	
		boolean result;
		
		result = false;
		
		for(Sponsorship s : sponsor.getSponsorships()){
			if(isSpam(s.getInfo()) || isSpam(s.getBanner())){
				result = true;
				break;
			}
		}
				
		return result;
	}
	
	public boolean hasSpam(Ranger ranger){	
		boolean result;
		Curriculum curriculum;
		
		result = false;
		curriculum = ranger.getCurriculum();
		
		if(curriculum != null){
			if(isSpam(curriculum.getEmail()) || isSpam(curriculum.getName()) || isSpam(curriculum.getPhone()) 
					|| isSpam(curriculum.getPhoto()) || isSpam(curriculum.getProfile())){
				result = true;
			}
					
			if(!result){
				for(Education e : curriculum.getEducationRecords()){
					if(isSpam(e.getAttachment()) || isSpam(e.getComments()) || isSpam(e.getInstitution()) 
							|| isSpam(e.getTitle())){
						result = true;
						break;
					}
				}
			}
			
			if(!result){
				for(Endorser e : curriculum.getEndorserRecords()){
					if(isSpam(e.getFullName()) || isSpam(e.getComments()) || isSpam(e.getEmail()) 
							|| isSpam(e.getPhone()) || isSpam(e.getProfile())){
						result = true;
						break;
					}
				}
			}
			
			if(!result){
				for(Miscellaneous e : curriculum.getMiscellaneousRecords()){
					if(isSpam(e.getAttachment()) || isSpam(e.getComments()) || isSpam(e.getTitle())){
						result = true;
						break;
					}
				}
			}
			
			if(!result){
				for(Professional e : curriculum.getProfessionalRecords()){
					if(isSpam(e.getAttachment()) || isSpam(e.getComments()) || isSpam(e.getCompanyName())
							 || isSpam(e.getRole())){
						result = true;
						break;
					}
				}
			}	
		}
		
		return result;
	}
	
	public boolean hasSpam(Auditor auditor){	
		boolean result;
		
		result = false;
		
		for(AuditRecord a : auditor.getAuditRecords()){
			for(String attachment : a.getAttachments()){
				if(isSpam(attachment)){
					result = true;
					break;
				}
			}
			
			if(!result && isSpam(a.getTitle()) || isSpam(a.getDescription())){
				result = true;
				break;
			}
		}
		
		if(!result){
			for(Note n : auditor.getNotes()){
				if(isSpam(n.getAuditorRemark())){
					result = true;
					break;
				}
			}
		}
				
		return result;
	}
}
