
package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.NoteRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Auditor;
import domain.Manager;
import domain.Note;
import domain.Trip;

@Service
@Transactional
public class NoteService {

	// Managed repository --------------------------------------

	@Autowired
	private NoteRepository	noteRepository;

	// Supporting services -------------------------------------

	@Autowired
	private ActorService	actorService;

	@Autowired
	private ManagerService	managerService;

	@Autowired
	private AuditorService	auditorService;

	@Autowired
	private TripService		tripService;


	// Constructor ---------------------------------------------------

	public NoteService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Note create(final Integer tripId) {
		Assert.isTrue(tripId != 0);
		Auditor auditor;
		Trip trip;

		UserAccount principal;
		Authority aut;

		aut = new Authority();
		aut.setAuthority(Authority.AUDITOR);
		principal = LoginService.getPrincipal();
		auditor = (Auditor) this.actorService.findByUserAccountId(principal.getId());
		trip = this.tripService.findOne(tripId);

		Assert.isTrue(auditor.getUserAccount().getAuthorities().contains(aut));
		Assert.notNull(trip);
		Assert.notNull(auditor);

		return this.create(auditor, trip);
	}

	public Note create(final Auditor auditor, final Trip trip) {
		Assert.notNull(auditor);
		Assert.notNull(trip);

		Note result;

		result = new Note();
		result.setAuditor(auditor);
		result.setTrip(trip);
		result.setTimeAuditor(new Date(System.currentTimeMillis() - 1));

		return result;
	}

	public Note findOne(final int noteId) {
		Assert.isTrue(noteId != 0);

		UserAccount principal;
		Authority aut;
		Note result;

		principal = LoginService.getPrincipal();

		aut = new Authority();
		aut.setAuthority(Authority.MANAGER);

		result = this.noteRepository.findOne(noteId);

		Assert.isTrue(result.getAuditor().getUserAccount().equals(principal) || principal.getAuthorities().contains(aut));

		return result;
	}

	public Collection<Note> findAll() {
		Collection<Note> result;
		UserAccount principal;
		Authority aut;

		principal = LoginService.getPrincipal();

		aut = new Authority();
		aut.setAuthority(Authority.MANAGER);

		Assert.isTrue(principal.getAuthorities().contains(aut));

		result = this.noteRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Note save(final Note note) {
		Assert.notNull(note);
		Assert.isTrue(note.getTrip().getPublicationDate().before(new Date()));

		Note result;
		UserAccount principal;

		principal = LoginService.getPrincipal();

		if (note.getId() == 0) {
			Auditor auditor;

			Assert.isTrue(note.getAuditor().getUserAccount().equals(principal));

			auditor = (Auditor) this.actorService.findByUserAccountId(principal.getId());
			note.setTimeAuditor(new Date(System.currentTimeMillis() - 1));

			result = this.noteRepository.save(note);

			auditor.getNotes().add(result);

			this.auditorService.save(auditor);

		} else {
			Assert.notNull(note.getManagerReply());
			Assert.isTrue(note.getManagerReply() != "");

			Manager manager;

			Assert.isTrue(note.getTrip().getManager().getUserAccount().equals(principal));

			manager = (Manager) this.actorService.findByUserAccountId(principal.getId());

			note.setTimeManager(new Date(System.currentTimeMillis() - 1));

			result = this.noteRepository.save(note);

			this.managerService.save(manager);
		}

		return result;
	}

	// Other business methods ---------------------------------------------------

	// Consultable por un manager
	public Collection<Note> getNotesByAuditorAndManager(final Integer auditorId, final Integer managerId) {
		Collection<Note> result;

		result = this.noteRepository.getNotesByAuditorAndManager(auditorId, managerId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Note> getNotesByTrip(final Integer tripId) {
		Collection<Note> result;
		Trip trip;
		UserAccount principal;

		principal = LoginService.getPrincipal();
		trip = this.tripService.findOne(tripId);
		Assert.isTrue(trip.getManager().getUserAccount().equals(principal));

		result = this.noteRepository.getNotesByTrip(tripId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Note> getNotesByAuditor() {
		Collection<Note> result;
		UserAccount principal;

		principal = LoginService.getPrincipal();

		result = this.noteRepository.getNotesByAuditor(principal.getId());

		return result;
	}

	public Collection<Note> getNotesByTripManager() {
		Collection<Note> result;
		UserAccount principal;

		principal = LoginService.getPrincipal();

		result = this.noteRepository.getNotesByTripManager(principal.getId());

		return result;
	}
}
