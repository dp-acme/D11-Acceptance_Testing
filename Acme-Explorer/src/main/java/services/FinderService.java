package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.FinderRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Explorer;
import domain.Finder;
import domain.Trip;

@Service
@Transactional
public class FinderService {

	// Managed repository --------------------------------------
	
	@Autowired
	private FinderRepository finderRepository;

	// Supporting services -------------------------------------
	
	@Autowired
	private TripService tripService;
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ExplorerService explorerService;
	
	// Constructor ---------------------------------------------------

	public FinderService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Finder create() {
		UserAccount userAccount;
		Actor actor;
		Finder result;
		
		userAccount = LoginService.getPrincipal();
		actor = actorService.findByUserAccountId(userAccount.getId());
		Assert.isInstanceOf(Explorer.class, actor);
		
		result = new Finder();
		
		result.setExplorer((Explorer)actor);
		result.setTrips(new ArrayList<Trip>());
		result.setLastSaved(new Date(System.currentTimeMillis() - 1));
		
		return result;
	}

	public Finder findOne(int finderId) {
		Assert.isTrue(finderId != 0);

		Finder result;

		result = finderRepository.findOne(finderId);

		return result;
	}
	
	public Collection<Finder> findAll() {
		Collection<Finder> result;

		result = finderRepository.findAll();

		return result;
	}

	public Finder save(Finder finder) {
		Assert.notNull(finder);
		
		Finder result;
		Collection<Trip> trips;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		
		Assert.isTrue(finder.getExplorer().getUserAccount().equals(principal));
				
		if (finder.getPriceEnd() != null && finder.getPriceStart() != null)
			Assert.isTrue(finder.getPriceEnd() >= finder.getPriceStart());
		if (finder.getDateEnd() != null && finder.getDateStart() != null)
			Assert.isTrue(finder.getDateEnd().after(finder.getDateStart()));
	
		trips = new ArrayList<Trip>(tripService.filterTripsByCriteria(finder));
		finder.setTrips(trips);
		
		finder.setLastSaved(new Date(System.currentTimeMillis() - 1));

		result = finderRepository.save(finder);
		
		if (finder.getId() == 0) {
			Explorer explorer;
			
			explorer = (Explorer)actorService.findByUserAccountId(principal.getId());
			explorer.setFinder(result);
			
			explorerService.save(explorer);
		}
		
		return result;
	}
	
	public void delete(Finder finder) {
		Assert.notNull(finder);
		
		UserAccount userAccount;
		
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(finder.getExplorer().getUserAccount().equals(userAccount));
		Assert.isTrue(finderRepository.exists(finder.getId()));
		
		finder.getExplorer().setFinder(null);

		explorerService.save(finder.getExplorer());
		
		finderRepository.delete(finder);
	}
	
	//Other business methods -----------------------------------
	
	private boolean sameFinderData(Finder finder1, Finder finder2){
		boolean res = true;

		if(finder1.getDateStart() != null){
			res = finder1.getDateStart().equals(finder2.getDateStart());
		
		} else{
			res = finder2.getDateStart() == null;
		}
		
		if(res){
			if(finder1.getDateEnd() != null){
				res = finder1.getDateEnd().equals(finder2.getDateEnd());
				
			} else{
				res = finder2.getDateEnd() == null;
			}	
		}
		
		if(res){
			if(finder1.getPriceStart() != null){
				res = finder1.getPriceStart().equals(finder2.getPriceStart());
				
			} else{
				res = finder2.getPriceStart() == null;
			}	
		}
		
		if(res){
			if(finder1.getPriceEnd() != null){
				res = finder1.getPriceEnd().equals(finder2.getPriceEnd());
				
			} else{
				res = finder2.getPriceEnd() == null;
			}	
		}
		
		if(res){
			if(finder1.getKeyWord() != null){
				res = finder1.getKeyWord().equals(finder2.getKeyWord());
				
			} else{
				res = finder2.getKeyWord() == null;
			}	
		}
		
		return res;
	}
	
	public Finder updateFinder(Finder finder, Finder newFinder){
		Finder result;
		
		result = finder;
		
		if(sameFinderData(finder, newFinder)){ //Si tiene los mismos t�rminos de b�squeda
			Date lastMoment, newMoment;
			Integer interval;
			
			interval = configurationService.findConfiguration().getFinderCacheTime();
			
			lastMoment = DateUtils.addHours(finder.getLastSaved(), interval);
			newMoment = newFinder.getLastSaved();
									
			if(lastMoment.after(newMoment)){
				result = this.save(finder);
			}
			
		} else{ //Actualizar finder
			finder.setDateStart(newFinder.getDateStart());
			finder.setDateEnd(newFinder.getDateEnd());
			finder.setPriceStart(newFinder.getPriceStart());
			finder.setPriceEnd(newFinder.getPriceEnd());
			finder.setKeyWord(newFinder.getKeyWord());
			
			result = this.save(finder);			
		}
		
		return result;
	}
}
