package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.MiscellaneousRepository;
import security.LoginService;
import security.UserAccount;
import domain.Curriculum;
import domain.Miscellaneous;

@Service
@Transactional
public class MiscellaneousService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private MiscellaneousRepository miscellaneousRepository;

	// Supporting services ---------------------------------------------------

	@Autowired
	private CurriculumService curriculumService;

	// Constructor ---------------------------------------------------

	public MiscellaneousService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Miscellaneous create(Curriculum curriculum) {
		UserAccount principal;
		Miscellaneous result;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(curriculum.getRanger().getUserAccount().equals(principal));
		
		result = new Miscellaneous();
		result.setCurriculum(curriculum);
		
		return result;
	}

	public Miscellaneous findOne(int miscellaneousId) {
		Assert.isTrue(miscellaneousId != 0);

		Miscellaneous result;

		result = miscellaneousRepository.findOne(miscellaneousId);

		return result;
	}

	public Collection<Miscellaneous> findAll() {
		Collection<Miscellaneous> result;

		result = miscellaneousRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Miscellaneous save(Miscellaneous miscellaneous) {
		Assert.notNull(miscellaneous);
		
		UserAccount principal;
		Miscellaneous result;
		Curriculum curriculum;
		
		curriculum = miscellaneous.getCurriculum();
		principal = LoginService.getPrincipal();
		Assert.isTrue(miscellaneous.getCurriculum().getRanger().getUserAccount().equals(principal));
		
		result = miscellaneousRepository.save(miscellaneous);
		
		if (miscellaneous.getId() == 0) {
			curriculum.getMiscellaneousRecords().add(result);
			curriculumService.save(curriculum);
		}
		
		return result;
	}

	public void delete(Miscellaneous miscellaneous) {
		Assert.notNull(miscellaneous);
		
		UserAccount principal;
		Curriculum curriculum;
		
		curriculum = miscellaneous.getCurriculum();
		principal = LoginService.getPrincipal();
		Assert.isTrue(curriculum.getRanger().getUserAccount().equals(principal));
		Assert.isTrue(miscellaneousRepository.exists(miscellaneous.getId()));
		
		curriculum.getMiscellaneousRecords().remove(miscellaneous);
		curriculumService.save(curriculum);
		
		miscellaneousRepository.delete(miscellaneous);
	}
	
	public void deleteAllRecordsOfCurriculum(Curriculum c){
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(c.getRanger().getUserAccount().equals(principal));

		miscellaneousRepository.deleteInBatch(c.getMiscellaneousRecords());
	}
}
