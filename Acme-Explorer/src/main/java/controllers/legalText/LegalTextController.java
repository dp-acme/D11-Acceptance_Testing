/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.legalText;


import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.LegalTextService;
import services.TripService;
import controllers.AbstractController;
import domain.LegalText;
import domain.Trip;

@Controller
@RequestMapping("/legalText")
public class LegalTextController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private LegalTextService legalTextService;
	
	@Autowired
	private TripService tripService;

	// Constructors -----------------------------------------------------------

	public LegalTextController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<LegalText> legalTexts;
		
			legalTexts = legalTextService.findAll();
		
		result = new ModelAndView("legalText/list");
		result.addObject("legalTexts", legalTexts);
		result.addObject("requestURI", "legalText/list.do");

		return result;
	}
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam int tripId) {
		ModelAndView result;
		LegalText legalText;
		Trip trip;
	
		trip = tripService.findOne(tripId);
		legalText = trip.getLegalText();
		
		result = new ModelAndView("legalText/display");
		result.addObject("legalText", legalText);

		return result;
	}
}
