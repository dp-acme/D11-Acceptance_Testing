package controllers.applyFor;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import security.LoginService;
import services.ActorService;
import services.ApplyForService;
import services.TripService;
import domain.ApplyFor;
import domain.Explorer;
import domain.Status;
import domain.Trip;

@Controller
@RequestMapping("/applyfor")
public class ApplyForExplorerController extends AbstractController{

	@Autowired
	private ApplyForService applyForService;
	@Autowired
	private ActorService actorService;
	@Autowired
	private TripService tripService;

	@RequestMapping("/explorer/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<ApplyFor> applications;
		Explorer explorer;
		
		explorer = (Explorer) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		applications = explorer.getApplications();
		
		result = new ModelAndView("applyfor/explorer/list");
		result.addObject("applications", applications);

		return result;
	}
	
	@RequestMapping("/explorer/display")
	public ModelAndView display(@RequestParam int applyForId) {
		ModelAndView result;
		ApplyFor application;
		Explorer explorer;
				
		application = applyForService.findOne(applyForId);
		explorer = (Explorer) actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		Assert.notNull(application);
		Assert.isTrue(application.getExplorer().equals(explorer));
		
		result = new ModelAndView("applyfor/explorer/display");
		result.addObject("application", application);

		return result;
	}
	
	@RequestMapping("/explorer/edit")
	public ModelAndView edit(@RequestParam int applyForId) {
		ModelAndView result;
		ApplyFor application;
		Explorer explorer;

		application = applyForService.findOne(applyForId);
		explorer = (Explorer) actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		Assert.notNull(application);
		Assert.isTrue(application.getExplorer().equals(explorer));

		result = createEditModelAndView(application);

		return result;
	}
	

	@RequestMapping(value = "/explorer/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid @ModelAttribute("applyfor") ApplyFor applyFor, BindingResult binding) {
		ModelAndView result;
		
		if(binding.hasErrors()){
			result = createEditModelAndView(applyFor);
		}else{
			try{
				applyForService.save(applyFor);
				result = new ModelAndView("redirect:list.do");
			}catch(Throwable oops){
				result = createEditModelAndView(applyFor, "applyfor.commit.error");
			}
		}

		return result;
	}
	
	private ModelAndView createEditModelAndView(ApplyFor applyFor) {
		ModelAndView result;

		result = createEditModelAndView(applyFor, null);

		return result;
	}

	private ModelAndView createEditModelAndView(ApplyFor applyFor, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("applyfor/explorer/edit");
		
		result.addObject("applyfor", applyFor);
		result.addObject("message", messageCode);

		return result;
	}
	
	@RequestMapping("/explorer/create")
	public ModelAndView create(@RequestParam int tripId) {
		ModelAndView result;
		ApplyFor applyFor;
		Trip trip;
		
		trip = tripService.findOne(tripId);
		applyFor = applyForService.create(trip);
				
		result = createEditModelAndView(applyFor);
		
		return result;
	}
	
	@RequestMapping("/explorer/cancel")
	public ModelAndView cancel(@RequestParam int applyForId) {
		ModelAndView result;
		ApplyFor application;
		Explorer explorer;
		Status status;
		
		application = applyForService.findOne(applyForId);
		explorer = (Explorer) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		Assert.notNull(application);
		Assert.isTrue(application.getExplorer().equals(explorer));
		
		status = new Status();
		status.setStatus(Status.CANCELLED);
		application.setStatus(status);
		
		applyForService.save(application);
		
		result = new ModelAndView("redirect:list.do");
		
		return result;
	}
	
	
}
