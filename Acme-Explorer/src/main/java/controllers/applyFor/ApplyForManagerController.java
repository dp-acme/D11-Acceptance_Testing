package controllers.applyFor;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import security.LoginService;
import services.ActorService;
import services.ApplyForService;
import domain.ApplyFor;
import domain.Manager;
import domain.Status;
import domain.Trip;

@Controller
@RequestMapping("/applyfor")
public class ApplyForManagerController extends AbstractController{

	@Autowired
	private ApplyForService applyForService;
	@Autowired
	private ActorService actorService;

	@RequestMapping("/manager/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<ApplyFor> applications;
		Collection<Trip> trips;
		Manager manager;
		
		manager = (Manager) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		trips = manager.getTrips();
		applications = new ArrayList<ApplyFor>();

		for(Trip t : trips)	{
			applications.addAll(applyForService.getApplicationsByTrip(t.getId()));
		}
		
		result = new ModelAndView("applyfor/manager/list");
		result.addObject("applications", applications);

		return result;
	}
	
	@RequestMapping("/manager/display")
	public ModelAndView display(@RequestParam int applyForId) {
		ModelAndView result;
		ApplyFor application;
		Manager manager;
		
		application = applyForService.findOne(applyForId);
		manager = (Manager) actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		Assert.notNull(application);
		Assert.isTrue(manager.getTrips().contains(application.getTrip()));
		
		result = new ModelAndView("applyfor/manager/display");
		result.addObject("application", application);

		return result;
	}
	
	@RequestMapping("/manager/edit")
	public ModelAndView edit(@RequestParam int applyForId) {
		ModelAndView result;
		ApplyFor applyFor;
		Manager manager;

		applyFor = applyForService.findOne(applyForId);

		manager = (Manager) actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		Assert.notNull(applyFor);
		Assert.isTrue(manager.getTrips().contains(applyFor.getTrip()));

		result = createEditModelAndView(applyFor);

		return result;
	}
	
	
	@RequestMapping(value = "/manager/edit", method = RequestMethod.POST, params = "reject")
	public ModelAndView reject(@Valid ApplyFor applyFor, BindingResult binding) {
		ModelAndView result;
		Status status;
		
		status = new Status();
		status.setStatus(Status.REJECTED);
		applyFor.setStatus(status);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(applyFor);
		}else{
			try{
				applyFor = applyForService.changeStatus(applyFor);
				result = new ModelAndView("redirect:list.do");
			}catch(Throwable oops){
				result = createEditModelAndView(applyFor, "applyfor.commit.error");
			}
		}

		return result;
	}
	
	@RequestMapping("/manager/due")
	public ModelAndView due(@RequestParam int applyForId) {
		ModelAndView result;
		ApplyFor application;
		Manager manager;
		Status status;
		
		application = applyForService.findOne(applyForId);
		manager = (Manager) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		Assert.notNull(application);
		Assert.isTrue(application.getTrip().getManager().equals(manager));
		
		status = new Status();
		status.setStatus(Status.DUE);
		application.setStatus(status);
		
		applyForService.changeStatus(application);
		
		result = new ModelAndView("redirect:list.do");
		
		return result;
	}
	
	private ModelAndView createEditModelAndView(ApplyFor applyFor) {
		ModelAndView result;

		result = createEditModelAndView(applyFor, null);

		return result;
	}

	private ModelAndView createEditModelAndView(ApplyFor applyFor, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("applyfor/manager/edit");
		
		result.addObject("applyfor", applyFor);
		result.addObject("message", messageCode);

		return result;
	}
	

	
	
}
