
package controllers.survivalClass;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.SurvivalClassService;
import controllers.AbstractController;
import domain.Explorer;
import domain.SurvivalClass;

@Controller
@RequestMapping("/survivalClass")
public class SurvivalClassExplorerController extends AbstractController {

	//Servicios-----------------------------------------------------------------
	@Autowired
	private SurvivalClassService	survivalClassService;
	@Autowired
	private ActorService			actorService;


	//Contructor-------------------------------------------------------------------
	public SurvivalClassExplorerController() {
		super();
	}

	//List--------------------------------------------------------------------
	@RequestMapping("/explorer/list")
	public ModelAndView list(@RequestParam int tripId) {
		ModelAndView result;
		Collection<SurvivalClass> survivalClasses;
		Explorer explorer;
		
		explorer = (Explorer) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		survivalClasses = survivalClassService.getSurvivalClassesbyTripId(tripId);
		
		result = new ModelAndView("survivalClass/explorer/list");
		result.addObject("survivalClasses", survivalClasses);
		result.addObject("principal", explorer);

		return result;

	}
	
	@RequestMapping("/explorer/mysurvivals")
	public ModelAndView listMySC() {
		ModelAndView result;
		Collection<SurvivalClass> survivalClasses;
		Explorer explorer;
		
		explorer = (Explorer) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		survivalClasses = explorer.getSurvivalClasses();
		
		result = new ModelAndView("survivalClass/explorer/list");
		result.addObject("survivalClasses", survivalClasses);

		return result;

	}
	
	@RequestMapping("/explorer/unjoin")
	public ModelAndView unjoin(@RequestParam int survivalClassId) {
		ModelAndView result;
		Explorer explorer;
		SurvivalClass survivalClass;

		survivalClass = this.survivalClassService.findOne(survivalClassId);
		explorer = (Explorer) this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		Assert.isTrue(explorer.getSurvivalClasses().contains(survivalClass));

		survivalClassService.dropSurvival(explorer, survivalClass);
		
		result = new ModelAndView("redirect:mysurvivals.do");
		
		return result;

	}

	//Join --------------------------------------------------------------------------------------------
	@RequestMapping(value = "/explorer/join", method = RequestMethod.GET)
	public ModelAndView join(@RequestParam int survivalClassId) {
		ModelAndView result;
		Explorer explorer;
		SurvivalClass survivalClass;

		survivalClass = this.survivalClassService.findOne(survivalClassId);
		explorer = (Explorer) this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		Assert.isTrue(!explorer.getSurvivalClasses().contains(survivalClass));

		survivalClassService.joinSurvivalClass(explorer, survivalClass);
		
		result = new ModelAndView("redirect:mysurvivals.do");
		
		return result;
	}

}
