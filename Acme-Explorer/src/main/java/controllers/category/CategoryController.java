/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.CategoryService;
import domain.Category;

@Controller
@RequestMapping("/category")
public class CategoryController extends AbstractController {
	
	// Sevices
	@Autowired
	CategoryService categoryService;

	// Constructors -----------------------------------------------------------

	public CategoryController() {
		super();
	}

	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(value = "categoryId", required = false) Integer categoryId) {
		ModelAndView result;
		Category rootCategory;
		Category parentCategory;
		
		result = new ModelAndView("category/list");
		
		if(categoryId == null){
			rootCategory = categoryService.findRootcategory();			
			
		} else{
			rootCategory = categoryService.findOne(categoryId);
		}
			
		Assert.notNull(rootCategory);
		
		parentCategory = categoryService.findParentCategory(rootCategory);
		
		result.addObject("category", rootCategory);
		result.addObject("parentCategory", parentCategory);
				
		return result;
	}
}
