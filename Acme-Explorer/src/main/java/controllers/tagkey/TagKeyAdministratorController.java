/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.tagkey;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.TagKeyService;
import controllers.AbstractController;
import domain.TagKey;

@Controller
@RequestMapping("/tagkey/administrator")
public class TagKeyAdministratorController extends AbstractController {
	
	// Sevices
	@Autowired
	TagKeyService tagKeyService;

	// Constructors -----------------------------------------------------------

	public TagKeyAdministratorController() {
		super();
	}
	
	@RequestMapping("/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<TagKey> tags;
		
		result = new ModelAndView("tagkey/administrator/list");
							
		tags = tagKeyService.findAll();
		
		result.addObject("tags", tags);
				
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		TagKey tag;
		
		result = new ModelAndView("tagkey/administrator/create");
							
		tag = tagKeyService.create();
		
		result.addObject("tagKey", tag);
				
		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam(value = "tagKeyId", required = true) Integer tagKeyId) {
		ModelAndView result;
		TagKey tag;
		
		result = new ModelAndView("tagkey/administrator/edit");
							
		tag = tagKeyService.findOne(tagKeyId);
		
		Assert.notNull(tag);
		
		result.addObject("tagKey", tag);
				
		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid TagKey tagKey, BindingResult binding) {
		ModelAndView result;
		
		if(binding.hasErrors()){
			if(tagKey.getId() == 0){
				result = createCreateModelAndView(tagKey, null);
				
			} else{				
				result = createEditModelAndView(tagKey, null);
			}
			
		}else{
			try{								
				tagKeyService.save(tagKey);
				
				result = new ModelAndView("redirect:/tagkey/administrator/list.do");
				
			}catch(Throwable oops){
				if(tagKey.getId() == 0){
					result = createCreateModelAndView(tagKey, "tagkey.commit.error");
					
				} else{				
					result = createEditModelAndView(tagKey, "tagkey.commit.error");
				}
			}
		}

		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid TagKey tagKey, BindingResult binding) {
		ModelAndView result;
		
		try{								
			tagKeyService.delete(tagKey);
			
			result = new ModelAndView("redirect:/tagkey/administrator/list.do");
			
		}catch(Throwable oops){
			result = createEditModelAndView(tagKey, "tagkey.commit.error");
		}

		return result;
	}
	
	protected ModelAndView createEditModelAndView(TagKey tagKey, String message) {
		ModelAndView result;
		
		result = new ModelAndView("tagkey/administrator/edit");
		result.addObject("tagKey", tagKey);
		result.addObject("message", message);
		
		return result;
	}
	
	protected ModelAndView createCreateModelAndView(TagKey tagKey, String message) {
		ModelAndView result;
		
		result = new ModelAndView("tagkey/administrator/create");
		result.addObject("tagKey", tagKey);
		result.addObject("message", message);
		
		return result;
	}
}
