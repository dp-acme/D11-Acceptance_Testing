package controllers.trip;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.TripService;
import domain.Sponsorship;
import domain.Trip;

@Controller
@RequestMapping("/trip/auditor")
public class TripAuditorController extends AbstractController{
	// Services------------------------------
	@Autowired
	private TripService tripService;
	
	// Constructor---------------------------
	public TripAuditorController() {
		super();
	}
	
	@RequestMapping(value = "/display", method=RequestMethod.GET)
	public ModelAndView display(@RequestParam("tripId") int tripId){
		ModelAndView result;
		Trip trip;
		Sponsorship sponsorship;
		String bannerAux;
		
		trip = tripService.findOne(tripId);
		result = new ModelAndView("trip/display");
		
		Assert.notNull(trip);
		Assert.isTrue(!trip.getPublicationDate().after(new Date()));
		
		sponsorship = tripService.getRandomSponsorship(trip.getId());
		bannerAux = "";
		
		result.addObject("trip", trip);
		result.addObject("requestURI", "trip/auditor/display.do");
		if(sponsorship != null){
			result.addObject("bannerSponsor", sponsorship.getBanner());
			result.addObject("infoSponsor", sponsorship.getInfo());
		}
		else{
			result.addObject("bannerSponsor", bannerAux);
		}
		return result;
	}

}
