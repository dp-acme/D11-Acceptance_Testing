package controllers.trip;

import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.CategoryService;
import services.LegalTextService;
import services.RangerService;
import services.TripService;
import domain.Actor;
import domain.Category;
import domain.LegalText;
import domain.Manager;
import domain.Ranger;
import domain.Sponsorship;
import domain.Trip;

@Controller
@RequestMapping("/trip/manager")
public class TripManagerController extends AbstractController{
	// Services------------------------------
	@Autowired
	private TripService tripService;
	
	@Autowired
	private RangerService rangerService;
	
	@Autowired
	private CategoryService categoriesService;
	
	@Autowired
	private LegalTextService legalTextService;
	
	@Autowired
	private ActorService actorService;
	
	// Constructor---------------------------
	public TripManagerController() {
		super();
	}

	//Listing--------------------------------
	@RequestMapping(value = "/list")
	public ModelAndView list(HttpServletRequest request){
		ModelAndView result;
		Collection<Trip> trips;
		
		trips = tripService.getTripsPublished();
		
		for(Trip t:tripService.findByManager()){
			if(!trips.contains(t)){
				trips.add(t);
			}
		}
		
		result = new ModelAndView("trip/list");
		
		String keyword = request.getParameter("keyword");
		if(keyword != "" && keyword != null){
			trips = tripService.filterTripsByCriteria(keyword, null, null, null, null);
			trips.addAll(tripService.filterTripsByCriteriaManager(keyword, null, null, null, null));
		}
		
		result.addObject("trips", trips);
		result.addObject("requestURI", "trip/manager/list.do");
		return result;
	}
	
	@RequestMapping(value = "/display", method=RequestMethod.GET)
	public ModelAndView display(@RequestParam("tripId") int tripId){
		ModelAndView result;
		Trip trip;
		Sponsorship sponsorship;
		String bannerAux;
		
		UserAccount userAccount;
		Actor actor;

		userAccount = LoginService.getPrincipal();
		actor = actorService.findByUserAccountId(userAccount.getId());
		Assert.isInstanceOf(Manager.class, actor);
		
		trip = tripService.findOne(tripId);
		result = new ModelAndView("trip/display");
		
		Assert.notNull(trip);
		Assert.isTrue(!(trip.getPublicationDate().after(new Date()) && !trip.getManager().equals(actor)));
		
		sponsorship = tripService.getRandomSponsorship(trip.getId());
		bannerAux = "";
		result.addObject("trip", trip);
		
		if(sponsorship != null){
			result.addObject("bannerSponsor", sponsorship.getBanner());
			result.addObject("infoSponsor", sponsorship.getInfo());
		}
		else{
			result.addObject("bannerSponsor", bannerAux);
		}
		
		result.addObject("requestURI", "trip/manager/display.do");
		
		return result;
	}
	
	@RequestMapping(value = "/create", method=RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		Trip trip;
		
		trip = this.tripService.create();
		
		result = this.createEditModelAndView(trip);

		return result;
	}
	
	
	@RequestMapping(value = "/edit", method=RequestMethod.GET)
	public ModelAndView edit(@RequestParam int tripId){
		ModelAndView result;
		Trip trip;
		
		UserAccount userAccount;
		Actor actor;

		userAccount = LoginService.getPrincipal();
		actor = actorService.findByUserAccountId(userAccount.getId());
		Assert.isInstanceOf(Manager.class, actor);
		
		trip = this.tripService.findOne(tripId);
		result = this.createEditModelAndView(trip);
		
		Assert.notNull(trip);
		Assert.isTrue(!(trip.getPublicationDate().after(new Date()) && !trip.getManager().equals(actor)));
		
		return result;
	}
	
	@RequestMapping(value = "/cancel", method=RequestMethod.GET)
	public ModelAndView cancelShow(@RequestParam int tripId){
		ModelAndView result;
		Trip trip;
		
		trip = this.tripService.findOne(tripId);
		Assert.notNull(trip);
		result = this.cancelModelAndView(trip);
		
		return result;
	}
	
	@RequestMapping(value = "/cancel", method=RequestMethod.POST, params="save")
	public ModelAndView cancel(@Valid Trip trip, BindingResult binding){
		ModelAndView result;
		
		if(binding.hasErrors()){
			result = cancelModelAndView(trip);
		}else{
			try{
				tripService.cancel(trip.getId(), trip.getCancellationReason());
				result = new ModelAndView("redirect:list.do");
			}catch(Throwable oops){
				result = cancelModelAndView(trip, "trip.commit.error");
			}
		}
		
		return result;
	}
	
	@RequestMapping(value = "/edit", method=RequestMethod.POST, params="save")
	public ModelAndView save(@Valid Trip trip, BindingResult binding){
		ModelAndView result;
		
		if(binding.hasErrors()){
			result = createEditModelAndView(trip);
		}else{
			try{
				tripService.save(trip);
				result = new ModelAndView("redirect:list.do");
			}catch(Throwable oops){
				result = createEditModelAndView(trip, "trip.commit.error");
			}
		}
		
		return result;
	}
	
	@RequestMapping(value = "/edit", method=RequestMethod.POST, params="delete")
	public ModelAndView delete(@Valid Trip trip, BindingResult binding){
		ModelAndView result;
		
		try{
			tripService.delete(trip);
			result = new ModelAndView("redirect:list.do");
		}catch(Throwable oops){
			result = createEditModelAndView(trip, "trip.commit.error");
		}
		return result;
	}
	
	
	protected ModelAndView createEditModelAndView(Trip trip){
		ModelAndView result;
		
		result = createEditModelAndView(trip, null);
		
		return result;
	}
	
	protected ModelAndView cancelModelAndView(Trip trip){
		ModelAndView result;
		
		result = cancelModelAndView(trip, null);
		
		return result;
	}
	
	protected ModelAndView createEditModelAndView(Trip  trip, String message){
		ModelAndView result;
		Collection<Ranger> rangers;
		Collection<LegalText> legalTexts;
		Collection<Category> categories;
		
		rangers = this.rangerService.findAll();
		legalTexts = this.legalTextService.getLegalTextFinalMode();
		categories = this.categoriesService.findAll();
		
		result = new ModelAndView("trip/manager/edit");
		result.addObject("trip", trip);
		result.addObject("rangers", rangers);
		result.addObject("categories", categories);
		result.addObject("legalTexts", legalTexts);
		result.addObject("message", message);
		return result;
 	}
	
	protected ModelAndView cancelModelAndView(Trip  trip, String message){
		ModelAndView result;
		Collection<Ranger> rangers;
		Collection<LegalText> legalTexts;
		Collection<Category> categories;
		
		rangers = this.rangerService.findAll();
		legalTexts = this.legalTextService.findAll();
		categories = this.categoriesService.findAll();
		
		result = new ModelAndView("trip/manager/cancel");
		result.addObject("trip", trip);
		result.addObject("rangers", rangers);
		result.addObject("categories", categories);
		result.addObject("legalTexts", legalTexts);
		result.addObject("message", message);
		return result;
 	}
	
}
