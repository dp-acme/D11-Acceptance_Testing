package controllers.trip;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.TripService;
import domain.Sponsorship;
import domain.Trip;

@Controller
@RequestMapping("/trip")
public class TripController extends AbstractController{
	// Services------------------------------
	@Autowired
	private TripService tripService;
	
	// Constructor---------------------------
	public TripController() {
		super();
	}

	//Listing--------------------------------
	@RequestMapping(value = "/list")
	public ModelAndView list(HttpServletRequest request, @RequestParam(value="categoryId", required=false) Integer categoryId){
		ModelAndView result;
		Collection<Trip> trips;
		trips = new ArrayList<>();
		
		result = new ModelAndView("trip/list");
		
		String keyword = request.getParameter("keyword");
		
		if((keyword != "" && keyword != null) || categoryId != null){
			if(keyword != "" && keyword != null){
				trips = tripService.filterTripsByCriteria(keyword, null, null, null, null);
			}
			
			if(categoryId != null && keyword == null){
				for(Trip t : tripService.getTripsByCategory(categoryId)){
					if(!trips.contains(t)){
						trips.add(t);
					}
				}
			}
		}else{
			trips = tripService.getTripsPublished();
		}
		
		result.addObject("trips", trips);
		result.addObject("requestURI", "trip/list.do");
		
		return result;
	}
	@RequestMapping(value = "/display", method=RequestMethod.GET)
	public ModelAndView display(@RequestParam("tripId") int tripId){
		ModelAndView result;
		Trip trip;
		Sponsorship sponsorship;
		String bannerAux;
		
		trip = tripService.findOne(tripId);
		result = new ModelAndView("trip/display");
		
		Assert.notNull(trip);
		Assert.isTrue(!trip.getPublicationDate().after(new Date()));
		
		sponsorship = tripService.getRandomSponsorship(trip.getId());
		bannerAux="";
		
		result.addObject("trip", trip);

		result.addObject("requestURI", "trip/display.do");
		if(sponsorship != null){
			result.addObject("bannerSponsor", sponsorship.getBanner());
			result.addObject("infoSponsor", sponsorship.getInfo());
		}
		else{
			result.addObject("bannerSponsor", bannerAux);
		}
		return result;
	}
}