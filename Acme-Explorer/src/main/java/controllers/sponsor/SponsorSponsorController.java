/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.sponsor;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ConfigurationService;
import services.SponsorService;
import controllers.AbstractController;
import domain.Actor;
import domain.Sponsor;

@Controller
@RequestMapping("/sponsor/sponsor")
public class SponsorSponsorController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private SponsorService sponsorService;
	
	@Autowired
	private ActorService actorService;

	@Autowired
	private ConfigurationService configurationService;
	
	// Constructors -----------------------------------------------------------

	public SponsorSponsorController() {
		super();
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.GET)
	public ModelAndView edit(){
		ModelAndView result;
		Actor principal;
		Sponsor sponsor;
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Sponsor);
		sponsor = (Sponsor) principal;
		Assert.notNull(sponsor);
		result = this.createEditModelAndView(sponsor);

	
		
		return result;
	}
	
	
	
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="save")
	public ModelAndView save(@Valid Sponsor sponsor, BindingResult binding){
		ModelAndView result;
		if(binding.hasErrors()){
			result = createEditModelAndView(sponsor);
		} else { 
			try {
				sponsorService.save(sponsor);
				result = new ModelAndView("redirect:/");
			} catch (Throwable oops) {
				result = createEditModelAndView(sponsor, "sponsor.commit.error");
			}
		}
		return result;
	}
	
	protected ModelAndView createEditModelAndView(Sponsor sponsor) {
		ModelAndView result; 
		
		result = createEditModelAndView(sponsor, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(Sponsor sponsor,
			String messageCode) {
		ModelAndView result;
		result = new ModelAndView("sponsor/sponsor/edit");
		result.addObject("sponsor", sponsor);
		result.addObject("actorId", sponsor.getId());
		result.addObject("socialIdentities", sponsor.getSocialIdentities());
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());

		result.addObject("message", messageCode);
		
		return result;
	}

}
