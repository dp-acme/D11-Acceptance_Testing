package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Note extends DomainEntity {

	// Constructor
	
	public Note() {
		super();
	}
	
	// Attributes
	
	private Date timeAuditor;
	private String auditorRemark;
	private Date timeManager;
	private String managerReply;
	
	@Past
	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTimeAuditor() {
		return timeAuditor;
	}
	public void setTimeAuditor(Date timeAuditor) {
		this.timeAuditor = timeAuditor;
	}
	
	@NotBlank
	public String getAuditorRemark() {
		return auditorRemark;
	}
	public void setAuditorRemark(String auditorRemark) {
		this.auditorRemark = auditorRemark;
	}
	
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTimeManager() {
		return timeManager;
	}
	public void setTimeManager(Date timeManager) {
		this.timeManager = timeManager;
	}
	
	public String getManagerReply() {
		return managerReply;
	}
	public void setManagerReply(String managerReply) {
		this.managerReply = managerReply;
	}
	
	// Relationships
	
	private Auditor auditor;
	private Trip trip;

	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public Auditor getAuditor() {
		return auditor;
	}
	public void setAuditor(Auditor auditor) {
		this.auditor = auditor;
	}
		
	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public Trip getTrip() {
		return trip;
	}
	public void setTrip(Trip trip) {
		this.trip = trip;
	}
}
