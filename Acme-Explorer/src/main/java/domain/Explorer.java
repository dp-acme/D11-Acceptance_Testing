package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Explorer extends Actor {

	// Constructor
	
	public Explorer() {
		super();
	}
	
	// Attributes
	
	// Relationships
	
	private Collection<EmergencyContact> emergencyContacts;
	private Finder finder;
	private Collection<Story> stories;
	private Collection<ApplyFor> applications;
	private Collection<SurvivalClass> survivalClasses;
	
	@NotNull
	@Valid
	@OneToMany
	public Collection<EmergencyContact> getEmergencyContacts() {
		return emergencyContacts;
	}
	public void setEmergencyContacts(Collection<EmergencyContact> emergencyContacts) {
		this.emergencyContacts = emergencyContacts;
	}
	
	@Valid
	@OneToOne(optional = true)
	public Finder getFinder() {
		return finder;
	}
	public void setFinder(Finder finder) {
		this.finder = finder;
	}

	@NotNull
	@Valid
	@OneToMany(mappedBy = "explorer")
	public Collection<Story> getStories() {
		return stories;
	}
	public void setStories(Collection<Story> stories) {
		this.stories = stories;
	}
	
	@NotNull
	@Valid
	@OneToMany(mappedBy = "explorer")
	public Collection<ApplyFor> getApplications() {
		return applications;
	}
	public void setApplications(Collection<ApplyFor> applications) {
		this.applications = applications;
	}
	
	@NotNull
	@Valid
	@ManyToMany(mappedBy = "explorers")
	public Collection<SurvivalClass> getSurvivalClasses() {
		return survivalClasses;
	}
	public void setSurvivalClasses(Collection<SurvivalClass> survivalClasses) {
		this.survivalClasses = survivalClasses;
	}	
}