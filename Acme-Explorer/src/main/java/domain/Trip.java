
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Trip extends DomainEntity {

	// Constructor

	public Trip() {
		super();
	}


	// Attributes

	private String	ticker;
	private String	title;
	private String	description;
	private double	price;
	private String	requirements;
	private Date	publicationDate;
	private Date	startDate;
	private Date	endDate;
	private String	cancellationReason;


	@Column(unique = true)
	public String getTicker() {
		return this.ticker;
	}
	public void setTicker(final String ticker) {
		this.ticker = ticker;
	}

	@NotBlank
	public String getTitle() {
		return this.title;
	}
	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	public String getDescription() {
		return this.description;
	}
	public void setDescription(final String description) {
		this.description = description;
	}

	@Range(min = 0)
	public double getPrice() {
		return this.price;
	}
	public void setPrice(final double price) {
		this.price = price;
	}

	@NotBlank
	public String getRequirements() {
		return this.requirements;
	}
	public void setRequirements(final String requirements) {
		this.requirements = requirements;
	}

	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getPublicationDate() {
		return this.publicationDate;
	}
	public void setPublicationDate(final Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	public Date getStartDate() {
		return this.startDate;
	}
	public void setStartDate(final Date startDate) {
		this.startDate = startDate;
	}

	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	public Date getEndDate() {
		return this.endDate;
	}
	public void setEndDate(final Date endDate) {
		this.endDate = endDate;
	}

	public String getCancellationReason() {
		return this.cancellationReason;
	}
	public void setCancellationReason(final String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}


	// Relationships

	private Ranger					ranger;
	private Manager					manager;
	private Category				category;
	private Collection<TagValue>	tags;
	private Collection<Stage>		stages;
	private LegalText				legalText;


	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Ranger getRanger() {
		return this.ranger;
	}
	public void setRanger(final Ranger ranger) {
		this.ranger = ranger;
	}

	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Manager getManager() {
		return this.manager;
	}
	public void setManager(final Manager manager) {
		this.manager = manager;
	}

	@Valid
	@ManyToOne(optional = true)
	public Category getCategory() {
		return this.category;
	}
	public void setCategory(final Category category) {
		this.category = category;
	}

	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public LegalText getLegalText() {
		return this.legalText;
	}
	public void setLegalText(final LegalText legalText) {
		this.legalText = legalText;
	}

	@NotNull
	@Valid
	@ManyToMany
	public Collection<TagValue> getTags() {
		return this.tags;
	}
	public void setTags(final Collection<TagValue> tags) {
		this.tags = tags;
	}

	@Valid
	@OneToMany(mappedBy = "trip", cascade = CascadeType.ALL)
	public Collection<Stage> getStages() {
		return this.stages;
	}
	public void setStages(final Collection<Stage> stages) {
		this.stages = stages;
	}
}
