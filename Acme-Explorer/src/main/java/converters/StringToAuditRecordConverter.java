package converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import repositories.AuditRecordRepository;
import domain.AuditRecord;

@Component
@Transactional
public class StringToAuditRecordConverter implements Converter<String, AuditRecord>{
	@Autowired AuditRecordRepository auditRecordRepository;

	@Override
	public AuditRecord convert(String text) {
		AuditRecord result; 
		int id;
		
		try {
			if(StringUtils.isEmpty(text)){
				result = null;
			} else{
				id= Integer.valueOf(text);
				result = auditRecordRepository.findOne(id);
			}
		} catch (Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return result;
	}
	
		
}
