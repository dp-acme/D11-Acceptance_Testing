
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.MiscellaneousRepository;
import domain.Miscellaneous;

@Component
@Transactional
public class StringToMiscellaneousConverter implements Converter<String, Miscellaneous> {

	@Autowired
	MiscellaneousRepository	miscellaneousRepository;


	@Override
	public Miscellaneous convert(String source) {
		Miscellaneous result;
		int id;

		try {
			if (StringUtils.isEmpty(source))
				result = null;
			else {
				id = Integer.valueOf(source);
				result = miscellaneousRepository.findOne(id);
			}
		} catch (Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
