package converters;

import java.net.URLDecoder;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Status;

@Component
@Transactional
public class StringToStatusConverter implements Converter<String, Status>{
	
	@Override
	public Status convert(String source) {
		Status result;
		String parts[];
		
		if(source==null)
			result = null;
		else{
			try{
				parts = source.split("\\|");
				result = new Status();
				result.setStatus(URLDecoder.decode(parts[0], "UTF-8"));
			}catch(Throwable oops){
				throw new IllegalArgumentException(oops);
				}
		}
		return result;
	}

}
