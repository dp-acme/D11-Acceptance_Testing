package repositories;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Trip;

@Repository
public interface TripRepository extends JpaRepository<Trip, Integer> {
	
	//Si no funciona hay que poner comillas simples
	 @Query("select t from Trip t where (t.title like ?1 OR t.description like ?1 OR t.ticker like ?1) AND ?2 <= t.startDate AND t.endDate <= ?3 AND ?4 <= t.price AND t.price <= ?5 AND t.publicationDate <= ?6")	
	Collection<Trip> filterTripsByCriteria(String keyWord, Date date1, Date date2, Double minPrice, Double maxPrice, Date currentDate);
	
	 @Query("select t from Trip t where (t.title like ?1 OR t.description like ?1 OR t.ticker like ?1) AND ?2 <= t.startDate AND t.endDate <= ?3 AND ?4 <= t.price AND t.price <= ?5 AND t.publicationDate > ?6 AND t.manager.id = ?7")	
	Collection<Trip> filterTripsByCriteriaManager(String keyWord, Date date1, Date date2, Double minPrice, Double maxPrice, Date currentDate, int managerId);
	 
	 @Query("select t from Trip t where (t.title like ?1 OR t.description like ?1 OR t.ticker like ?1) AND ?2 <= t.startDate AND t.endDate <= ?3 AND ?4 <= t.price AND t.price <= ?5 AND t.publicationDate <= ?6")	
	Page<Trip> filterTripsByCriteriaFinder(String keyWord, Date date1, Date date2, Double minPrice, Double maxPrice, Date currentDate, Pageable pageable);
	 
	@Query("select t from Trip t where t.manager.id = ?1")
	Collection<Trip> findByManager(int managerId);
	
	//Obtener Trips con fecha de publicación posterior a otra fecha
	@Query("select t from Trip t where t.publicationDate > ?1")
	Collection<Trip> getTripsPublishedAfter(Date date);
	
	//Obtener Trip por ticker
	@Query("select t from Trip t where t.ticker = ?1")
	Trip getTripByTicker(String ticker);
	
	//Obtener Trips publicados
	@Query("select t from Trip t where t.publicationDate <= ?1")
	Collection<Trip> getTripsPublished(Date date);
	
	//Obtener Trips publicados
	@Query("select t from Trip t where t.category.id = ?1 AND t.publicationDate <= ?2")
	Collection<Trip> getTripsByCategory(int categoryId, Date currentDate);
	
}
