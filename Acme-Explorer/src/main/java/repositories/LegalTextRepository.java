package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import domain.LegalText;

public interface LegalTextRepository extends JpaRepository<LegalText, Integer>{
	
	//Obtener Trip por ticker
		@Query("select l from LegalText l where l.finalMode = true")
		Collection<LegalText> getLegalTextFinalMode();
	
}
