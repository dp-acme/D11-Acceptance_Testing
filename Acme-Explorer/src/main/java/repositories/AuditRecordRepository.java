package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.AuditRecord;

@Repository
public interface AuditRecordRepository extends JpaRepository<AuditRecord, Integer>{

	@Query("select a from AuditRecord a where a.auditor.userAccount.id = ?1")
	 Collection<AuditRecord> findAllByPrincipal(int userAccountId);
	
	@Query("select a from AuditRecord a where a.trip.id = ?1 AND a.auditor.userAccount.id = ?2")
	 Collection<AuditRecord> findAllByTripAndAuditor(int tripId, int auditorId);
	
	@Query("select a from AuditRecord a where a.trip.id = ?1 AND a.draft = false")
	 Collection<AuditRecord> findAllDraftedByTrip(int tripId);

	
}
