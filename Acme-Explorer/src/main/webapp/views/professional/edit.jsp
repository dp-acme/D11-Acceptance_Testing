<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<jstl:choose>
	<jstl:when test="${param.professionalId==null}">
		<h1>
			<spring:message code="professional.ranger.create" />
		</h1>
		<jstl:set var="disable" value="false" />
	</jstl:when>
	<jstl:otherwise>
		<h1>
			<spring:message code="professional.ranger.edit" />
		</h1>
		<jstl:set var="disable" value="true" />
	</jstl:otherwise>
</jstl:choose>


<form:form action="professional/ranger/edit.do"
	modelAttribute="professional">

	<!-- ATRIBUTOS QUE NO SE VAN A MODIFICAR -->
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="curriculum" />

	<!-- ATRIBUTOS QUE VAMOS A MODIFICAR -->

	<!-- COMPANYNAME -->
	<form:label path="companyName">
		<spring:message code="professional.companyName" />:
	</form:label>
	<form:input path="companyName" />
	<form:errors cssClass="error" path="companyName" />

	<br />
	<br />
		<!-- PROFESSIONALSTART -->
	<form:label path="professionalStart">
		<spring:message code="professional.professionalStart" />:
	</form:label>
	<form:input path="professionalStart" />
	<form:errors cssClass="error" path="professionalStart" />

	<br />
	<br />
		<!-- PROFESSIONALEND -->
	<form:label path="professionalEnd">
		<spring:message code="professional.professionalEnd" />:
	</form:label>
	<form:input path="professionalEnd" />
	<form:errors cssClass="error" path="professionalEnd" />

	<br />
	<br />
		<!-- ROLE -->
	<form:label path="role">
		<spring:message code="professional.role" />:
	</form:label>
	<form:input path="role" />
	<form:errors cssClass="error" path="role" />

	<br />
	<br />
		<!-- ATTACHMENT -->
	<form:label path="attachment">
		<spring:message code="professional.attachment" />:
	</form:label>
	<form:input path="attachment" />
	<form:errors cssClass="error" path="attachment" />

	<br />
	<br />
		<!-- COMMENTS -->
	<form:label path="comments">
		<spring:message code="professional.comments" />:
	</form:label>
	<form:input path="comments" />
	<form:errors cssClass="error" path="comments" />

	<br />
	<br />
	<!-- PARA EL SALTO DE LINEA -->

	<!-- BOTONES -->

	<!-- BOTON DE SAVE -->
	<input type="submit" name="save"
		value="<spring:message code="professional.save"/>" />
	<!-- PREGUNTAMOS POR LOS CASOS QUE TENEMOS -->
	<!-- VAMOS A EDITARLO -->
	<jstl:if test="${professional.id !=0}">
		<!-- QUIERE DECIR QUE HAY OBJETOS  -->
		<!-- PODEMOS PONER BOTON DE BORRAR -->
		<input type="submit" name="delete"
			value="<spring:message code="professional.delete" />"
			onclick="javascript: return confirm('<spring:message code="professional.confirm.delete" />')" />
		&nbsp;<!-- separacion a la derecha -->
	</jstl:if>

	<input type="button" name="cancel"
		value="<spring:message code="professional.cancel" />"
		onclick="javascript: <('professional/ranger/list.do');" />
	<!-- RELATIVEREDIR ES QE ME REDIRIJE A LA URL QUE HAYA PUESTO  -->
	<br />

</form:form>
<!-- FIN DE CREACION DE FORMULARIO -->