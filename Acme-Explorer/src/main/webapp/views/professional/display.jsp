<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!-- COMPANY NAME -->
<spring:message code="professional.display.companyName" />:
<jstl:out value="${professional.companyName}"></jstl:out>
<br />
<br />

<!-- PROFESSIONAL START -->
<spring:message code="professional.display.professionalStart" />
:
<jstl:out value="${professional.professionalStart}"></jstl:out>
<br />
<br/>

<!-- PROFESSIONAL END -->
<spring:message code="professional.display.professionalEnd" />
:
<jstl:out value="${professional.professionalEnd}"></jstl:out>

<!-- ROLE -->
<spring:message code="professional.display.role" />
:
<jstl:out value="${professional.role}"></jstl:out>
<!-- ATTACHMENT -->
<spring:message code="professional.display.attachment" />
:
<jstl:out value="${professional.attachment}"></jstl:out>
<!-- COMMENTS -->
<spring:message code="professional.display.comments" />
:
<jstl:out value="${professional.comments}"></jstl:out>



<!-- IF WE ARE AUTHENTICATE DIFERENT URL TO BACK -->
<security:authorize access="hasRole('RANGER')">
<spring:url var="urlDisplayCurriculum" value="professional/ranger/list.do" />
</security:authorize>

<security:authorize access="isAnonymous()">
<spring:url var="urlDisplayCurriculum" value="professional/list.do?curriculumId=${curriculumId}" />
</security:authorize>
<br />
<br />
<!-- BACK -->
<input type="button" name="back"
	value="<spring:message code="professional.display.back" />"
	onclick="javascript: relativeRedir('${urlDisplayCurriculum}');" />

