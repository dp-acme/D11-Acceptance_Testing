<%--
 * display.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!-- MOSTRAR ATRIBUTOS -->

<spring:message code="endorser.display.fullName" />
:
<jstl:out value="${endorser.fullName}"></jstl:out>

<br />
<br />
<spring:message code="endorser.display.email" />
:
<jstl:out value="${endorser.email}"></jstl:out>

<br />
<br />
<spring:message code="endorser.display.phone" />
:
<jstl:out value="${endorser.phone}"></jstl:out>

<br />
<br />
<spring:message code="endorser.display.profile" />
:
<jstl:out value="${endorser.profile}"></jstl:out>

<br />
<br />
<spring:message code="endorser.display.comments" />
:
<jstl:out value="${endorser.comments}"></jstl:out>
<br />
<br />
<!--Button Back-->
<!-- UNAUTHENTICATE -->
<security:authorize access="isAnonymous()">
<%-- 	<jstl:choose> --%>
<%-- 		<jstl:when test="${empty row.id}"> --%>
<%-- 			<spring:url var="urlEndorser" value="trip/list.do" /> --%>
<%-- 		</jstl:when> --%>
<%-- 		<jstl:otherwise> --%>
			<spring:url var="urlEndorser" value="endorser/list.do?curriculumId=${curriculumId}" />
<%-- 		</jstl:otherwise> --%>
<%-- 	</jstl:choose> --%>


</security:authorize>

<!-- AUTHENTICATE AS RANGER -->

<security:authorize access="hasRole('RANGER')">
	<spring:url var="urlEndorser"
		value="endorser/ranger/list.do?curriculumId=${curriculumId}" />
</security:authorize>

<input type="button" name="back"
	value="<spring:message code="endorser.display.back" />"
	onclick="javascript: relativeRedir('${urlEndorser}');" />

<br />