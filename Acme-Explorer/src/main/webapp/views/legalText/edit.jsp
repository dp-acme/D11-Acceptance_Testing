<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<security:authorize access="hasRole('ADMINISTRATOR')">

	<form:form method="POST" action="legalText/administrator/edit.do" modelAttribute="legalText">
	
		<form:hidden path="id" />
		<form:hidden path="version" />
		<form:hidden path="moment" />
		
		
		<form:label path="title">
			<spring:message code="legalText.edit.titleLabel"/>
		</form:label>
		<form:input path="title"/><br>
		<form:errors path="title" cssClass="error"/><br>
		

		<form:label path="body">
			<spring:message code="legalText.edit.body"/>
		</form:label>
		<form:input path="body"/><br>
			<form:errors path="body" cssClass="error" /><br>
		
		
		<form:label path="laws">
			<spring:message code="legalText.edit.laws"/>
		</form:label>
		<form:input path="laws"/><br>
			<form:errors path="laws" cssClass="error"/><br>
		
		
		<spring:message code="legalText.edit.draftMode" var="draftMode"/>
		<form:radiobutton path="finalMode" value="false" label="${draftMode}"/>
		
		<spring:message code="legalText.edit.finalMode" var="finalMode"/>
		<form:radiobutton path="finalMode" value="true" label="${finalMode}"/><br>
		
		<spring:message code="legalText.edit.save" var="editSave"/>
		<input type="submit" name="save" value="${editSave}" />
		
		<jstl:if test="${not empty param.legalTextId}">
		<spring:message code="legalText.edit.delete" var="editDelete"/>
		<input type="submit" name="delete" value="${editDelete}" />
		</jstl:if>
		<spring:message code="legalText.edit.cancel" var="editCancel"/>
		<input type="button" value="${editCancel}" 
			onClick="javascript: relativeRedir('legalText/list.do');" />
	
	</form:form>
</security:authorize>