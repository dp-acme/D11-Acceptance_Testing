<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<jstl:choose>
	<jstl:when test="${param.educationId==null}">
		<h1>
			<spring:message code="education.ranger.create" />
		</h1>
		<jstl:set var="disable" value="false" />
	</jstl:when>
	<jstl:otherwise>
		<h1>
			<spring:message code="education.ranger.edit" />
		</h1>
		<jstl:set var="disable" value="true" />
	</jstl:otherwise>
</jstl:choose>


<form:form action="education/ranger/edit.do"
	modelAttribute="education">

	<!-- ATRIBUTOS QUE NO SE VAN A MODIFICAR -->
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="curriculum" />

	

	<!-- TITLE -->
	<form:label path="title">
		<spring:message code="education.title" />
	</form:label>
	<form:input path="title" />
	<form:errors cssClass="error" path="title"></form:errors>

	<br />
<br />
	<!-- INSTITUTION -->
	<form:label path="institution">
		<spring:message code="education.institution" />
	</form:label>
	<form:input path="institution" />
	<form:errors cssClass="error" path="institution"></form:errors>
	<br />

	<br />

	<!-- STUDIESSTART -->
	<form:label path="studiesStart">
		<spring:message code="education.studiesStart" />
	</form:label>
	<form:input path="studiesStart" />
	<form:errors cssClass="error" path="studiesStart"></form:errors>
	<br />

	<br />
	
	<!-- STUDIESEND -->
	<form:label path="studiesEnd">
		<spring:message code="education.studiesEnd" />
	</form:label>
	<form:input path="studiesEnd" />
	<form:errors cssClass="error" path="studiesEnd"></form:errors>
	<br />

	<br />
	
	<!-- ATTACHMENT -->
	<form:label path="attachment">
		<spring:message code="education.attachment" />
	</form:label>
	<form:input path="attachment" />
	<form:errors cssClass="error" path="attachment"></form:errors>
	<br />

	<br />
	
	<!-- COMMENTS -->
	
	<form:label path="comments">
		<spring:message code="education.comments" />
	</form:label>
	<form:input path="comments" />
	<form:errors cssClass="error" path="comments"></form:errors>
	<br />

	<br />

	<!-- BOTONES -->

	<!-- BOTON DE SAVE -->
	<input type="submit" name="save"
		value="<spring:message code="education.save"/>" />
	<!-- PREGUNTAMOS POR LOS CASOS QUE TENEMOS -->
	<!-- VAMOS A EDITARLO -->
	<jstl:if test="${education.id !=0}">
		<!-- QUIERE DECIR QUE HAY OBJETOS  -->
		<!-- PODEMOS PONER BOTON DE BORRAR -->
		<input type="submit" name="delete"
			value="<spring:message code="education.delete" />"
			onclick="javascript: return confirm('<spring:message code="education.confirm.delete" />')" />
		&nbsp;<!-- separacion a la derecha -->
	</jstl:if>

	<input type="button" name="cancel"
		value="<spring:message code="education.cancel" />"
		onclick="javascript: <('education/ranger/list.do');" />
	<!-- RELATIVEREDIR ES QE ME REDIRIJE A LA URL QUE HAYA PUESTO  -->
	<br />

</form:form>
<!-- FIN DE CREACION DE FORMULARIO -->