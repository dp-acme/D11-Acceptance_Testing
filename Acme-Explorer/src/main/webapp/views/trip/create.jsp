<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<form:form action="trip/manager/edit.do" modelAttribute="trip">
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="ticker" />
	<form:hidden path="manager" />
	<form:hidden path="tags" />
	<form:hidden path="stages" />

	<form:label path="title">
		<spring:message code="trip.create.title" />
	</form:label>
	<form:input path="title" />
	<form:errors path="title" cssClass="error" />
	
	<br/>
	<form:label path="description">
		<spring:message code="trip.create.description" />
	</form:label>
	<form:input path="description" />
	<form:errors path="description" cssClass="error" />
	
	<br/>
	<form:label path="requirements">
		<spring:message code="trip.create.requirements" />
	</form:label>
	<form:input path="requirements" />
	<form:errors path="requirements" cssClass="error"/>
	
	<br/>
	<form:label path="publicationDate">
		<spring:message code="trip.create.publicationDate" />
	</form:label>
	<form:input path="publicationDate" placeholder="dd/MM/yyyy HH:mm"/>
	<form:errors path="publicationDate" cssClass="error"/>
	
	<br/>
	<form:label path="startDate">
		<spring:message code="trip.create.startDate" />
	</form:label>
	<form:input path="startDate" placeholder="dd/MM/yyyy"/>
	<form:errors path="startDate" cssClass="error"/>

	<form:label path="endDate">
		<spring:message code="trip.create.endDate" />
	</form:label>
	<form:input path="endDate" placeholder="dd/MM/yyyy"/>
	<form:errors path="endDate" cssClass="error"/>
	
	<br/>
	<form:label path="ranger">
		<spring:message code="trip.create.ranger" />
	</form:label>
	<form:select path="ranger">
		<form:option label="------" value="0" />
		<form:options items="${rangers}" itemLabel="name" itemValue="id" />
	</form:select>
	<form:errors path="ranger" cssClass="error"/>
	
	<br/>
	<form:label path="category">
		<spring:message code="trip.create.category" />
	</form:label>
	<form:select path="category">
		<form:option label="------" value="0" />
		<form:options items="${categories}" itemLabel="name" itemValue="id" />
	</form:select>
	<form:errors path="category" cssClass="error"/>
	
	<br/>
	<form:label path="legalText">
		<spring:message code="trip.create.legalText" />
	</form:label>
	<form:select path="legalText">
		<form:option label="------" value="0" />
		<form:options items="${legalTexts}" itemLabel="title" itemValue="id" />
	</form:select>
	<form:errors path="legalText" cssClass="error"/>

	<br/>
	<spring:message code= "trip.create.save" var="saveButton"/>
	<input type="submit" name="save"
		value="${saveButton}" />
	
	<jstl:if test="${trip.getId()!=0}">
		<spring:message code= "trip.create.delete" var="deleteButton"/>
		<input type="submit" name="delete"
			value="${deleteButton}" />
	</jstl:if>
	
	<spring:message code= "trip.create.cancel" var="cancelButton"/>
	<input type="button" name="cancel"
		value="${cancelButton}"
		onClick="javascript: relativeRedir('trip/manager/list.do');" />


</form:form>
