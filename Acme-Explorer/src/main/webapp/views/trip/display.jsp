<%--
 * display.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- TITULO -->
<jsp:useBean id="currentDate" class="java.util.Date" />

<security:authorize access="hasRole('MANAGER')">
	<security:authentication property="principal.username" var="principal" />
	<jstl:if
		test="${trip.getManager().getUserAccount().getUsername() == principal}">
		<jstl:if test="${trip.getPublicationDate().getTime() > currentDate.getTime()}">
			<a href="trip/manager/edit.do?tripId=${trip.id}"><spring:message
					code="trip.display.edit.trip" /></a>
			<br />
		</jstl:if>
	</jstl:if>
	
	<jstl:if
		test="${trip.getManager().getUserAccount().getUsername() == principal}">
		<jstl:if
			test="${trip.getPublicationDate().getTime() < currentDate.getTime() && trip.getStartDate() > currentDate && (trip.getCancellationReason() == null)}">
			<a href="trip/manager/cancel.do?tripId=${trip.id}"><spring:message
					code="trip.display.cancel.trip" /></a>
			<br />
		</jstl:if>
	</jstl:if>

</security:authorize>

<security:authorize access="hasRole('EXPLORER')">
	<jstl:choose>
		<jstl:when test="${empty applyfor}">
			<a href="applyfor/explorer/create.do?tripId=${trip.id}"><spring:message
					code="trip.display.application" /></a>
			<br />
		</jstl:when>
		<jstl:otherwise>
			<a href="applyfor/explorer/list.do"><spring:message
					code="trip.display.myapplications" /></a>
			<br />
		</jstl:otherwise>
	</jstl:choose>
	<br/>
</security:authorize>

<!-- MOSTRAR ATRIBUTOS -->

<!-- BANNER -->

<jstl:if test="${not empty bannerSponsor}">
	<br />
	<jstl:url value="${infoSponsor}" var="infoSp"></jstl:url>
	<a href="${infoSp}"><img src="${bannerSponsor}" style="width:300px"/></a>
	<br />
</jstl:if>


<jstl:if test="${trip.getCancellationReason() != null}">
	<spring:message code="cancelled" />
	<br />
	<spring:message code="trip.display.cancellationReason" />:
		<jstl:out value="${trip.cancellationReason}"></jstl:out>
	<br />
</jstl:if>

<spring:message code="trip.display.ticker" />
:
<jstl:out value="${trip.ticker}"></jstl:out>

<br />

<jstl:if test="${principal == trip.getManager().getUserAccount().getUsername()}">
	<spring:message code="trip.display.publicationDate" />
	<spring:message code="trip.list.formatTime" var="formatDate" />
	<fmt:formatDate value="${trip.getPublicationDate()}" pattern="${formatDate}" var="publicationDateFmt" />
	:
	<jstl:out value="${publicationDateFmt}"></jstl:out>
	
	<br />
</jstl:if>

<spring:message code="trip.display.title" />
:
<jstl:out value="${trip.title}"></jstl:out>

<br />

<spring:message code="trip.display.description" />
:
<jstl:out value="${trip.description}"></jstl:out>

<br />

<spring:message code="trip.display.requirements" />
:
<jstl:out value="${trip.requirements}"></jstl:out>

<br />

<jstl:if test="${trip.category.name != null}">
	<spring:message code="trip.display.category" />
	:
	<jstl:out value="${trip.category.name}"></jstl:out>
	
	<br />
</jstl:if>

<spring:message code="trip.display.startDate" />
<spring:message code="trip.list.formatDate" var="formatDate" />
<fmt:formatDate value="${trip.getStartDate()}" pattern="${formatDate}" var="startDateFmt" />
:
<jstl:out value="${startDateFmt}"></jstl:out>

<br />

<spring:message code="trip.display.endDate" />
<spring:message code="trip.list.formatDate" var="formatDate" />
<fmt:formatDate value="${trip.getEndDate()}" pattern="${formatDate}" var="endDateFmt" />
:
<jstl:out value="${endDateFmt}"></jstl:out>

<br />

<fmt:formatNumber value="${trip.getPrice()}"  pattern="#,##0.00" var="pricefmt"/>
<spring:message code="trip.display.price" />
:
<jstl:out value="${pricefmt}"></jstl:out>

<br />

<!-- DEPENDIENDO.. -->
<!-- NO AUTENTICADO -->


<a href="ranger/display.do?rangerId=${trip.ranger.id}"><spring:message
		code="trip.display.ranger" /></a>

<br />

<a href="story/list.do?tripId=${trip.id}"><spring:message
		code="trip.display.story" /></a>

<br />

<a href="legalText/display.do?tripId=${trip.id}"><spring:message
		code="trip.display.legalText" /></a>

<br />

<security:authorize access = "hasAnyRole('ADMINISTRATOR','RANGER','AUDITOR','SPONSOR','EXPLORER') or isAnonymous()">
	<a href="tagvalue/list.do?tripId=${trip.id}"><spring:message
			code="trip.display.tagValue" /></a>
	
	<br />
</security:authorize>
<security:authorize access="!hasRole('AUDITOR')">

	<a href="auditrecord/list.do?tripId=${trip.id}"><spring:message
			code="trip.display.audit.record" /></a>
	<br>
</security:authorize>

<spring:url var="urlTrip" value="trip/list.do" />



<!-- SPONSOR -->
<security:authorize access="hasRole('SPONSOR')">
	<jstl:if test="${trip.getPublicationDate().getTime() < currentDate.getTime() && trip.getStartDate() > currentDate && trip.getCancellationReason() == null}">
		<a href="sponsorship/sponsor/create.do?tripId=${trip.id}"><spring:message
				code="trip.display.create.sponsorship" /></a>
		<br>
	</jstl:if>
		<spring:url var="urlTrip" value="trip/list.do" />

</security:authorize>

<!-- MANAGER -->


<security:authorize access="hasRole('MANAGER')">
	<jstl:if
		test="${trip.getManager().getUserAccount().getUsername() == principal}">
		<jstl:if test="${trip.getPublicationDate().getTime() > currentDate.getTime()}">
			<a href="stage/manager/create.do?tripId=${trip.id}"><spring:message
					code="trip.display.stage" /></a>
		<br />
		</jstl:if>
	</jstl:if>
	
	<jstl:if
		test="${trip.getManager().getUserAccount().getUsername() == principal}">
		<jstl:if test="${trip.getPublicationDate().getTime() > currentDate.getTime()}">
			<a href="tagvalue/manager/create.do?tripId=${trip.id}"><spring:message
					code="trip.display.createTagValue" /></a>
		<br />
		</jstl:if>
	</jstl:if>

	<jstl:if
		test="${trip.getManager().getUserAccount().getUsername() == principal}">
			<jstl:if test="${trip.getStartDate() >= currentDate}">
			<a href="survivalClass/manager/create.do?tripId=${trip.id}"><spring:message
					code="trip.display.createSurvivalClass" /></a>
		<br />
		</jstl:if>
	</jstl:if>
	
	<jstl:if
		test="${trip.getManager().getUserAccount().getUsername() == principal}">
	<a href="tagvalue/manager/list.do?tripId=${trip.id}"><spring:message
			code="trip.display.tagValue" /></a>
	
	<br />
	</jstl:if>
	
	<jstl:if
		test="${trip.getManager().getUserAccount().getUsername() == principal}">
	<a href="stage/manager/list.do?tripId=${trip.id}"><spring:message
			code="trip.display.listStage" /></a>

	<br />
	</jstl:if>
	
	<jstl:if
		test="${trip.getManager().getUserAccount().getUsername() == principal}">
	<a href="sponsorship/manager/list.do?tripId=${trip.id}"><spring:message
			code="trip.display.sponsorship" /></a>

	<br />
	</jstl:if>
	<security:authentication property="principal.username" var="principal" />
	<jstl:if
		test="${trip.getManager().getUserAccount().getUsername() == principal}">
		<a href="note/manager/list.do?tripId=${trip.id}"><spring:message
				code="trip.display.notes" /></a>

		<br />
	</jstl:if>
	
	<jstl:if
		test="${trip.getManager().getUserAccount().getUsername() == principal}">
	<a href="explorer/manager/list.do?tripId=${trip.id}"><spring:message
			code="trip.display.explorers" /></a>
	<br />
	</jstl:if>
	<spring:url var="urlTrip" value="trip/manager/list.do" />
</security:authorize>

<!-- EXPLORER -->
<security:authorize access="hasRole('EXPLORER')">

	<a href="survivalClass/explorer/list.do?tripId=${trip.id}"><spring:message
			code="trip.display.survivalclass" /></a>
	<br />
	
	<jstl:if test="${applyfor.status.status == 'ACCEPTED' }">
	<a href="story/explorer/create.do?tripId=${trip.id}"><spring:message
			code="trip.story.create" /></a>
		<br />
	</jstl:if>
	<spring:url var="urlTrip" value="trip/explorer/list.do" />
	
</security:authorize>

<!-- AUDITOR -->
<security:authorize access="hasRole('AUDITOR')">
	<a href="auditrecord/auditor/list.do?tripId=${trip.id}"><spring:message
			code="trip.display.audit.record" /></a>
	<br>
	<a href="note/auditor/create.do?tripId=${trip.id}"><spring:message
			code="trip.display.create.note" /></a>
	<br />

	<a href="auditrecord/auditor/create.do?tripId=${trip.id}"><spring:message
			code="trip.display.create.audit.record" /></a>
	<br />
	<spring:url var="urlTrip" value="trip/list.do" />
</security:authorize>
<br />
<!-- BACK -->
<input type="button" name="back"
	value="<spring:message code="backText" />"
	onclick="javascript: relativeRedir('${urlTrip}');" />

<br />

