<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<b><spring:message code="message.display.subject" /></b> <jstl:out value="${myMessage.getSubject()}" />

<br />
<br />

<spring:message code="message.display.formatDate" var="formatDate" />
<fmt:formatDate value="${myMessage.getMoment()}" pattern="${formatDate}" var="moment" />
<b><spring:message code="message.display.moment" /></b> <jstl:out value="${moment}" />

<br />
<br />

<b><spring:message code="message.display.priority" /></b> 
<jstl:choose>
	<jstl:when test="${myMessage.getPriority().getPriority() == \"HIGH\"}">
		<spring:message code="message.display.priority.high" />
	</jstl:when>
	<jstl:when test="${myMessage.getPriority().getPriority() == \"NEUTRAL\"}">
		<spring:message code="message.display.priority.neutral" />
	</jstl:when>
	<jstl:when test="${myMessage.getPriority().getPriority() == \"LOW\"}">
		<spring:message code="message.display.priority.low" />
	</jstl:when>
</jstl:choose>

<br />
<br />

<b><spring:message code="message.display.sender" /></b> 
<jstl:out value="${myMessage.getSender().getUserAccount().getUsername()} | ${myMessage.getSender().getName()} ${myMessage.getSender().getSurname()}" />

<br />
<br />

<b><spring:message code="message.display.recipients" /></b> 
<ul>
<jstl:forEach items="${myMessage.getRecipients()}" var="recipient">
	<li>
		<jstl:out value="${recipient.getUserAccount().getUsername()} | ${recipient.getName()} ${recipient.getSurname()}" />
	</li>
</jstl:forEach>
</ul>
<br />

<b><spring:message code="message.display.body" /></b> <jstl:out value="${myMessage.getBody()}" />

<br />
<br />

<b><spring:message code="message.display.folder" /></b> <jstl:out value="${currentFolder.getName()}" />

<br />
<br />
<br />

<form method="post" action="message/edit.do">

	<input type="hidden" name="messageId" value="${myMessage.getId()}" />

	<spring:message code="message.display.newFolder" />
	<select name="newFolder">
		<option value="${currentFolder.getId()}"><jstl:out value="${currentFolder.getName()}" /></option>
		<jstl:forEach items="${userFolders}" var="item">
			<option value="${item.getId()}"><jstl:out value="${item.getName()}" /></option>
		</jstl:forEach>
	</select>
	
	<br />
	<br />
	
	<spring:message code="message.display.moveToFolder" var="moveToFolder" />
	<input type="submit" value="${moveToFolder}" name="moveToFolder"
	onClick="javascript: return confirm('<spring:message code="message.display.moveToFolderConfirm" />');" />
	
	<security:authentication property="principal.id" var="principalId" />
	<jstl:if test="${myMessage.getSender().getUserAccount().getId() != principalId}">
		<spring:message code="message.display.reply" var="reply" />
		<input type="button" value="${reply}" name="reply"
		onClick="javascript: relativeRedir('message/reply.do?messageId=${myMessage.getId()}');" />
	</jstl:if>

</form>

<form:form method="POST" action="message/edit.do" modelAttribute="myMessage" >

	<form:hidden path="id"/>
	<form:hidden path="version"/>
	<form:hidden path="moment"/>
	<form:hidden path="subject"/>
	<form:hidden path="body"/>
	<form:hidden path="priority"/>
	<form:hidden path="isSpam"/>
	<form:hidden path="sender"/>
	<form:hidden path="recipients"/>
	
	<spring:message code="message.display.back" var="backButton" />
	<input type="button" value="${backButton}" 
	onClick="javascript: relativeRedir('folder/list.do?folderId=${currentFolder.getId()}');" />
	
	<spring:message code="message.display.delete" var="deleteButton" />
	<input type="submit" value="${deleteButton}" name="delete"
	onClick="javascript: return confirm('<spring:message code="message.display.deleteConfirm" />');" />

</form:form>
