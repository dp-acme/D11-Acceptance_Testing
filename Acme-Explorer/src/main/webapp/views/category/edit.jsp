<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<jstl:if test="${category.getId() == 0}">
	<jstl:set var="actionURI" value="category/administrator/create.do?parentCategoryId=${parentCategory.getId()}" />
</jstl:if>

<jstl:if test="${category.getId() != 0}">
	<jstl:set var="actionURI" value="category/administrator/edit.do?categoryId=${category.getId()}" />
</jstl:if>

<form:form modelAttribute="category" action="${actionURI}">
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="childrenCategories" />

	<form:label path="name"> 
		<spring:message code="category.edit.name"/>:	
	</form:label>
	<form:input path="name" />
	
	<spring:message code="category.edit.save" var="saveButton"/>
	<input type="submit" name="save" value="${saveButton}"/>
	
	<jstl:if test="${category.getId() != 0}"> 
		<spring:message code="category.edit.delete" var="deleteButton"/>	
		<spring:message code="category.edit.deleteMessage" var="deleteMessage"/>	
		<input type="submit" name="delete" value="${deleteButton}"  onclick="return confirm('${deleteMessage}')"/>
	</jstl:if>

	<spring:message code="category.edit.cancel" var="backButton"/>
	
	<jstl:if test="${category.getId() == 0}">
		<input type="button" value="${backButton}" onclick="javascript: relativeRedir('category/list.do?categoryId=${parentCategory.getId()}');">
	</jstl:if>
	
	<jstl:if test="${category.getId() != 0}">
		<input type="button" value="${backButton}" onclick="javascript: relativeRedir('category/list.do?categoryId=${category.getId()}');">
	</jstl:if>
	<br>
	<form:errors path="name" cssClass="error"/>
</form:form>
