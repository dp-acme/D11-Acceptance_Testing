<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<security:authorize access="hasRole('AUDITOR')">
	<form:form method="POST" action="auditrecord/auditor/edit.do" modelAttribute="auditRecord">
	
		<form:hidden path="id" />
		<form:hidden path="version" />
		<form:hidden path="trip" />
		<form:hidden path="auditor" />
		<form:hidden path="time"/>
		
		<form:label path="title">
			<spring:message code="auditRecord.edit.titleLabel"/>
		</form:label>
		<form:input path="title"/><br>
		<form:errors path="title" cssClass="error"/><br>
		

		<form:label path="description">
			<spring:message code="auditRecord.edit.description"/>
		</form:label>
		<form:input path="description"/><br>
			<form:errors path="description" cssClass="error"/><br>
		
		
		<form:label path="attachments">
			<spring:message code="auditRecord.edit.attachments"/>
		</form:label>
		<form:textarea path="attachments" placeholder="http://www.url.com, http://www.url2.com" /><br>
			<form:errors path="attachments" cssClass="error"/><br>
		
		
		<spring:message code="auditRecord.edit.draftMode" var="draftMode"/>
		<form:radiobutton path="draft" value="true" label="${draftMode}"/>
		
		<spring:message code="auditRecord.edit.finalMode" var="finalMode"/>
		<form:radiobutton path="draft" value="false" label="${finalMode}"/><br>
		
		<spring:message code="auditRecord.edit.save" var="editSave"/>
		<input type="submit" name="save" value="${editSave}" />
		
		<jstl:if test="${not empty param.auditRecordId}">
		<spring:message code="auditRecord.edit.delete" var="editDelete"/>
		<input type="submit" name="delete" value="${editDelete}" />
		</jstl:if>
		<spring:message code="auditRecord.edit.cancel" var="editCancel"/>
		<input type="button" value="${editCancel}" 
			onClick="javascript: relativeRedir('auditrecord/auditor/list.do');" />
	
	</form:form>
</security:authorize>