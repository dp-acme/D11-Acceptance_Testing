<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- TITULO -->

<jstl:choose>
	<jstl:when test="${param.storyId==null}">
		<h1>
			<spring:message code="story.explorer.create" />
		</h1>
	</jstl:when>
	<jstl:otherwise>
		<h1>
			<spring:message code="story.explorer.edit" />
		</h1>
	</jstl:otherwise>
</jstl:choose>

<form:form action="story/explorer/edit.do" modelAttribute="story">

	<!-- ATRIBUTOS QUE NO VAMOS A MODIFICAR -->
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="explorer" />
	<form:hidden path="trip" />

	<!-- BANNER -->
	<form:label path="title">
		<spring:message code="story.title" />
	</form:label>
	<form:input path="title" />
	<form:errors cssClass="error" path="title"></form:errors>

	<br />

	<!-- INFO PAGE -->
	<form:label path="text">
		<spring:message code="story.text" />
	</form:label>
	<form:input path="text" />
	<form:errors cssClass="error" path="text"></form:errors>

	<br />
	
	<form:label path="attachments">
		<spring:message code="story.attachments" />
	</form:label>
	<form:textarea path="attachments" style="width:800px ; height:50px"/>
	<form:errors cssClass="error" path="attachments"></form:errors>

	<br />
	<!-- BOTONES -->
	<!-- SAVE -->
	<input type="submit" name="save"
		value="<spring:message code="story.save"/>" />
	&nbsp;
	&nbsp;
	
	<!-- CANCEL -->

	<jstl:choose>
		<jstl:when test="${param.storyId!=null}">
			<input type="button" name="cancel"
				value="<spring:message code="story.cancel" />"
				onclick="javascript: relativeRedir('story/explorer/list.do');" />
			<br />
		</jstl:when>
		<jstl:otherwise>
		<input type="button" name="cancel"
				value="<spring:message code="story.cancel" />"
				onclick="javascript: relativeRedir('trip/explorer/list.do');" />
			<br />
		</jstl:otherwise>
	</jstl:choose>




</form:form>
