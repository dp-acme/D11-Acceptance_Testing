/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CommentService;
import services.RendezvousService;
import controllers.AbstractController;
import domain.Comment;

@Controller
@RequestMapping("/comment/admin")
public class CommentAdminController extends AbstractController {

	// Services -----------------------------------------------------------

	@Autowired
	RendezvousService rendezvousService;
	
	@Autowired
	CommentService commentService;
	
	@Autowired
	ActorService actorService;
	
	// Constructors -----------------------------------------------------------

	public CommentAdminController() {
		super();
	}

	// Index ------------------------------------------------------------------		

	@RequestMapping(value = "/delete")
	public ModelAndView delete(@RequestParam(value = "rendezvousId", required = true) int rendezvousId,
								@RequestParam(value = "commentId", required = true) int commentId) {
		ModelAndView result;
		Comment comment;
		
		comment = commentService.findOne(commentId);
		
		Assert.notNull(comment);

		result = new ModelAndView("redirect:/rendezvous/display.do?rendezvousId="+rendezvousId);
		
		try{
			commentService.flagAsDeleted(comment);
			
		} catch (Throwable oops) {
			result.addObject("message", "rendezvous.commit.error");
		}
		
		return result;
	}
}
