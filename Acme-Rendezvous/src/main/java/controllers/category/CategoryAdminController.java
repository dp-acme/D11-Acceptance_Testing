/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CategoryService;
import controllers.AbstractController;
import domain.Category;
import forms.CategoryEditionForm;

@Controller
@RequestMapping("/category/admin")
public class CategoryAdminController extends AbstractController {

	// Services -----------------------------------------------------------

	@Autowired
	CategoryService categoryService;
	
	// Constructors -----------------------------------------------------------

	public CategoryAdminController() {
		super();
	}

	// Index ------------------------------------------------------------------		
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, params = "create")
	public ModelAndView create(@ModelAttribute("newCategory") Category category, BindingResult binding) {
		ModelAndView result;
		
		category = categoryService.reconstruct(category, null, binding);
				
		if(binding.hasErrors()){			
			result = new ModelAndView("category/list");

			result.addObject("newCategory", category);
			
		} else{
			try{
				categoryService.save(category);

				result = new ModelAndView("redirect:/category/list.do");

				result.addObject("newCategory", categoryService.create(null));				
				
			} catch (Throwable oops) {
				result = new ModelAndView("category/list");

				result.addObject("newCategory", category);				
				result.addObject("message", "category.commit.error");				
			}
		}

		result.addObject("newCategoryWithParent", categoryService.create(null));
		result.addObject("categories", categoryService.findParentCategories());
		result.addObject("categoryEditionForm", new CategoryEditionForm());

		return result;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, params = "createWithParent")
	public ModelAndView createWithParent(@RequestParam(value = "parentId", required = true) Integer parentId, 
										 @ModelAttribute("newCategoryWithParent") Category category, BindingResult binding) {
		ModelAndView result;
		
		category = categoryService.reconstruct(category, parentId, binding);
				
		if(binding.hasErrors()){			
			result = new ModelAndView("category/list");

			result.addObject("newCategoryWithParent", category);
			
		} else{
			try{
				categoryService.save(category);

				result = new ModelAndView("redirect:/category/list.do");

				result.addObject("newCategoryWithParent", categoryService.create(null));				
				
			} catch (Throwable oops) {				
				result = new ModelAndView("category/list");

				result.addObject("newCategoryWithParent", category);				
				result.addObject("message", "category.commit.error");				
			}
		}

		result.addObject("newCategory", categoryService.create(null));
		result.addObject("categories", categoryService.findParentCategories());
		result.addObject("categoryEditionForm", new CategoryEditionForm());
			
		return result;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, params = "edit")
	public ModelAndView edit(@RequestParam(value = "categoryId", required = true) Integer categoryId, 
										 @ModelAttribute("categoryEditionForm") CategoryEditionForm categoryEditionForm, BindingResult binding) {
		ModelAndView result;
		
		Category category;
		
		category = categoryService.reconstruct(categoryEditionForm, categoryId, binding);
				
		if(binding.hasErrors()){			
			result = new ModelAndView("category/list");

			result.addObject("categoryEditionForm", categoryEditionForm);

		} else{
			try{
				categoryService.save(category);

				result = new ModelAndView("redirect:/category/list.do");

				result.addObject("categoryEditionForm", new CategoryEditionForm());
				
			} catch (Throwable oops) {				
				result = new ModelAndView("category/list");

				result.addObject("categoryEditionForm", categoryEditionForm);
				result.addObject("message", "category.commit.error");				
			}
		}

		result.addObject("newCategory", categoryService.create(null));
		result.addObject("categories", categoryService.findParentCategories());
		result.addObject("newCategoryWithParent", categoryService.create(null));
			
		return result;
	}
	
	@RequestMapping(value = "/delete")
	public ModelAndView delete(@RequestParam(value = "categoryId", required = true) Integer categoryId) {
		ModelAndView result;
						
		try{
			Category category; 
			
			category = categoryService.findOne(categoryId);
			
			categoryService.delete(category);
			
			result = new ModelAndView("redirect:/category/list.do");
			
		} catch (Throwable oops) {	
			result = new ModelAndView("category/list");

			result.addObject("message", "category.commit.error");				
		}
		
		result.addObject("categories", categoryService.findParentCategories());
		result.addObject("newCategory", categoryService.create(null));
		result.addObject("newCategoryWithParent", categoryService.create(null));		
		result.addObject("categoryEditionForm", new CategoryEditionForm());

		return result;
	}
	
	@RequestMapping(value = "/move")
	public ModelAndView move(@RequestParam(value = "categoryId", required = true) Integer categoryId,
							   @RequestParam(value = "newParentId", required = false) Integer newParentId) {
		ModelAndView result;
						
		try{			
			categoryService.moveCategory(categoryId, newParentId);
			
			result = new ModelAndView("redirect:/category/list.do");
			
		} catch (Throwable oops) {	
			result = new ModelAndView("category/list");

			result.addObject("message", "category.commit.error");				
		}
		
		result.addObject("categories", categoryService.findParentCategories());
		result.addObject("newCategory", categoryService.create(null));
		result.addObject("newCategoryWithParent", categoryService.create(null));		
		result.addObject("categoryEditionForm", new CategoryEditionForm());

		return result;
	}
}
