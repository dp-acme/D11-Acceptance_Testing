/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.CategoryService;
import controllers.AbstractController;
import forms.CategoryEditionForm;

@Controller
@RequestMapping("/category")
public class CategoryController extends AbstractController {

	// Services -----------------------------------------------------------

	@Autowired
	CategoryService categoryService;
	
	// Constructors -----------------------------------------------------------

	public CategoryController() {
		super();
	}

	// Index ------------------------------------------------------------------		

	@RequestMapping(value = "/list")
	public ModelAndView list() {
		ModelAndView result;
				
		result = new ModelAndView("category/list");
		
		result.addObject("categories", categoryService.findParentCategories());
		result.addObject("newCategory", categoryService.create(null));
		result.addObject("newCategoryWithParent", categoryService.create(null));
		result.addObject("categoryEditionForm", new CategoryEditionForm());
		
		return result;
	}
}
