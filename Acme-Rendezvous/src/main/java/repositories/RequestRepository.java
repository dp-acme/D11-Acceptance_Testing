package repositories;


import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Request;

@Repository
public interface RequestRepository extends JpaRepository<Request, Integer>{
	
	@Query("select request from Request request where request.rendezvous.id = ?1 AND request.service.id = ?2")
	Request getRequestFromRendezvousAndService(int rendezvousId, int serviceId);
	
	@Query("select request from Request request where request.rendezvous.id = ?1")
	Collection<Request> getRequestsFromRendezvous(int rendezvousId);
	
	@Query("select request from Request request where request.service.id = ?1")
	Collection<Request> getRequestsFromService(int serviceId);

	@Query("select request from Request request where request.service.manager.id = ?1")
	Collection<Request> findRequestsByManager(int managerId);
}