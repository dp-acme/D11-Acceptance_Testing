
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import domain.Rendezvous;

public interface RendezvousRepository extends JpaRepository<Rendezvous, Integer> {

	//Obtener los Rendezvous que estan en finalMode (==true)
	@Query("select r from Rendezvous r where r.draft = true")
	Collection<Rendezvous> getRendezvousDraft();

	//Obtener los Rendezvous que no estan en finalMode (==fasle)
	@Query("select r from Rendezvous r where r.draft = false")
	Collection<Rendezvous> getRendezvousNoDraft();
	
	//Obtener los Rendezvous que pueden ser linkeados como similares
	@Query("select r from Rendezvous r where r.draft = false AND r.deleted = false")
	Collection<Rendezvous> getRendezvousNoDraftNoDeleted();
	
	@Query("select r from Rendezvous r where r.draft = false AND r.deleted = false AND r.creator.id = ?1")
	Collection<Rendezvous> getMyRendezvousNoDraftNoDeleted(int userId);

	@Query("select r from Rendezvous r where r.adultsOnly = false AND r.draft = false")
	Collection<Rendezvous> getRendezvousNoDraftNoAdultsOnly();
	
	@Query("select r from Rendezvous r where ((select u from User u where u.id = ?1) member of r.users OR r.creator.id = ?1)")
	Collection<Rendezvous> getRSVPSAndMyRendezvous(int userId);
	
	@Query("select r from Rendezvous r where r.draft = false AND ((select u from User u where u.id = ?1) member of r.users OR r.creator.id = ?1)")
	Collection<Rendezvous> getRSVPSWithoutDraft(int userId);
	
	@Query("select r from Rendezvous r where r.draft = false AND r.deleted = false AND ((select u from User u where u.id = ?1) member of r.users OR r.creator.id = ?1)")
	Collection<Rendezvous> getRSVPSWithoutDraftOrDeleted(int userId);
	
	@Query("select r from Rendezvous r where r.draft = false AND r.deleted = false AND r.adultsOnly = false AND ((select u from User u where u.id = ?1) member of r.users OR r.creator.id = ?1)")
	Collection<Rendezvous> getRSVPSWithoutDraftOrDeletedOrAdultsOnly(int userId);
	
	@Query("select r from Rendezvous r where r.draft = true OR r.deleted = true")
	Collection<Rendezvous> getRendezvousDraftOrDeleted();
	
	@Query("select r from Rendezvous r where r.draft = false AND r.deleted = false AND r.moment > CURRENT_TIMESTAMP AND r.creator.id = ?1 AND r NOT IN (select rq.rendezvous from Request rq where rq.service.id = ?2)")
	Collection<Rendezvous> getMyRendezvousNoDraftNoDeletedNoServiceNoPast(int userId, int serviceId);

	@Query("select distinct rq.rendezvous from Request rq where ?1 member of rq.service.categories AND rq.rendezvous.deleted = false AND rq.rendezvous.draft = false")
	Collection<Rendezvous> getRendezvousNoDraftNoDeletedByCategory(int categoryId);
	
	@Query("select distinct rq.rendezvous from Request rq where ?1 member of rq.service.categories AND rq.rendezvous.deleted = false AND rq.rendezvous.draft = false AND rq.rendezvous.adultsOnly = false")
	Collection<Rendezvous> getRendezvousNoDraftNoDeletedNoAdultsOnlyByCategory(int categoryId);
	
}
