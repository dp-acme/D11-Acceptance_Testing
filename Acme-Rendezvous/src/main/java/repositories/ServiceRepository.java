package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Request;
import domain.Service;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Integer>{

	@Query("select r from Request r where r.service.id = ?1")
	Collection<Request> getRequestsOfService(Integer serviceId);
	
	@Query("select s from Service s where ?1 member of s.categories")
	Collection<Service> findServicesWithCategory(Integer categoryId);

	@Query("select rq.service from Request rq where rq.rendezvous.id = ?1")
	Collection<Service> getServicesByRendezvous(int rendezvousId);
}
