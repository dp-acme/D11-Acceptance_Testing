<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<security:authorize access="hasRole('MANAGER')">

	<!-- Abrimos el formulario -->

	<form:form action="service/manager/edit.do" modelAttribute="service">

		<!-- Indicamos los campos ocultos -->

		<form:hidden path="id" />

		<!-- Indicamos los campos de los formularios -->

		<acme:textbox code="service.name" path="name" required="required" />
		<br />
		<br />

		<acme:textbox code="service.description" path="description"
			required="required" />
		<br />
		<br />

		<form:label path="categories">
			<spring:message code="service.edit.category" />
		</form:label>
		<form:select path="categories">
			<form:options items="${allCategories}" itemLabel="name" itemValue="id" />
		</form:select>
		<form:errors path="categories" cssClass="error" />
		<br />
		<br />

		<acme:textbox code="service.picture" path="picture" />
		<br />
		<br />

		<!-- Botones del formulario -->
		<acme:submit name="save" code="service.save" />
		<jstl:if test="${service.id != 0 && empty requests}">
			<spring:message var="confirmDeleteMessage"
				code="service.edit.ConfirmDelete" />
			<acme:submit name="delete" code="service.delete"
				onClickCallback="javascript: return confirm('${confirmDeleteMessage}');" />
		</jstl:if>

<%-- 		<jstl:if test="${service.id == 0}"> --%>
			<acme:cancel url="service/manager/list.do" code="service.cancel" />
<%-- 		</jstl:if> --%>

<%-- 		<jstl:if test="${service.id != 0}"> --%>
<%-- 			<acme:cancel url="service/display.do?serviceId=${param.serviceId}" code="service.cancel" /> --%>
<%-- 		</jstl:if> --%>

	</form:form>
</security:authorize>