<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<security:authorize access="hasRole('USER')">


	<jstl:choose>
		<jstl:when test="${param.rendezvousId==null}">
			<jstl:set var="disable" value="false" />
		</jstl:when>
		<jstl:otherwise>
			<jstl:set var="disable" value="true" />
		</jstl:otherwise>
	</jstl:choose>

	<!-- Abrimos el formulario -->
	
	<form:form action="rendezvous/user/edit.do" modelAttribute="rendezvous">

		<!-- Indicamos los campos ocultos -->
		
		<form:hidden path="id" />

		<!-- Indicamos los campos de los formularios -->

		<acme:textbox code="rendezvous.name" path="name" required="required" />
		<br />
		<br />

		<acme:textbox code="rendezvous.description" path="description"
			required="required" />
		<br />
		<br />

		<acme:textbox code="rendezvous.edit.moment" path="moment"
			required="required"
			placeholderCode="rendezvous.edit.placeholderMoment" />
			
		<jstl:if test="${pastDateError != null && !pastDateError.equals(\"\")}">
			<span class="error"><spring:message code="${pastDateError}" /></span>
		</jstl:if>
		<br />
		<br />

		<acme:textbox code="rendezvous.picture" path="picture" />
		<br />
		<br />

		<acme:textbox code="rendezvous.latitude" path="latitude" />
		<br />
		<br />

		<acme:textbox code="rendezvous.longitude" path="longitude" />
		<br />
		<br />


		<jstl:choose>
			<jstl:when test="${adult==true}">
				<form:label path="adultsOnly">
					<spring:message code="rendezvous.adultsOnly" />
				</form:label>
				<form:checkbox path="adultsOnly" />
				<br />
				<br />
			</jstl:when>

			<jstl:otherwise>
				<form:label path="adultsOnly">
					<spring:message code="rendezvous.adultsOnly" />
				</form:label>
				<form:checkbox path="adultsOnly" disabled="true" />
				<br />
				<br />
			</jstl:otherwise>
		</jstl:choose>

		<!-- Botones del formulario -->
		<acme:submit name="save" code="rendezvous.save" />
		<jstl:if test="${rendezvous.getId() != 0}">
			<spring:message var="confirmDeleteMessage" code="rendezvous.edit.ConfirmDelete" />
			<acme:submit name="delete" code="rendezvous.delete" 
				onClickCallback="javascript: return confirm('${confirmDeleteMessage}');" />
		</jstl:if>

		<jstl:if test="${rendezvous.getId() == 0}">
			<acme:cancel url="rendezvous/user/list.do" code="rendezvous.cancel" />
		</jstl:if>
		
		<jstl:if test="${rendezvous.getId() != 0}">
			<acme:cancel url="rendezvous/display.do?rendezvousId=${rendezvous.getId()}" code="rendezvous.cancel" />
		</jstl:if>		

	</form:form>
</security:authorize>