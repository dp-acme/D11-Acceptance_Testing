<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ page import="java.text.DecimalFormat" %>

<h2><spring:message code="dashboard.statistics" /></h2>

<ul>

	<li>	
		<b><spring:message code="dashboard.ratioCreatorsOverNonCreators" />:</b>
		<jstl:out value="${ratioCreatorsOverNonCreators}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.avgServicesPerCategory" />:</b>
		<jstl:out value="${avgServicesPerCategory}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.avgCategoriesPerRendezvous" />:</b>
		<jstl:out value="${avgCategoriesPerRendezvous}" />
	</li>
</ul>

<table> 
	<tr style="background-color: yellow;">
		<th><spring:message code="dashboard.table.property" /></th>
		<th><spring:message code="dashboard.table.average" /></th>
		<th><spring:message code="dashboard.table.sd" /></th>
		<th><spring:message code="dashboard.table.min" /></th>
		<th><spring:message code="dashboard.table.max" /></th>
		
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdRendezvousPerUser" /></td>
		<td><jstl:out value="${avgSdRendezvousPerUser[0]}" /></td>
		<td><jstl:out value="${avgSdRendezvousPerUser[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdUsersPerRendezvous" /></td>
		<td><jstl:out value="${avgSdUsersPerRendezvous[0]}" /></td>
		<td><jstl:out value="${avgSdUsersPerRendezvous[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdRSVPsPerUser" /></td>
		<td><jstl:out value="${avgSdRSVPsPerUser[0]}" /></td>
		<td><jstl:out value="${avgSdRSVPsPerUser[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdAnnoucementsPerRendezvous" /></td>
		<td><jstl:out value="${avgSdAnnoucementsPerRendezvous[0]}" /></td>
		<td><jstl:out value="${avgSdAnnoucementsPerRendezvous[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdRepliesPerComment" /></td>
		<td><jstl:out value="${avgSdRepliesPerComment[0]}" /></td>
		<td><jstl:out value="${avgSdRepliesPerComment[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdQuestionsPerRendezvous" /></td>
		<td><jstl:out value="${avgSdQuestionsPerRendezvous[0]}" /></td>
		<td><jstl:out value="${avgSdQuestionsPerRendezvous[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdAnswersPerRendezvous" /></td>
		<td><jstl:out value="${avgSdAnnoucementsPerRendezvous[0]}" /></td>
		<td><jstl:out value="${avgSdAnnoucementsPerRendezvous[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgMaxMinSdRequestsPerRendezvous" /></td>
		<td><jstl:out value="${avgMaxMinSdRequestsPerRendezvous[0]}" /></td>
		<td><jstl:out value="${avgMaxMinSdRequestsPerRendezvous[3]}" /></td>
		<td><jstl:out value="${avgMaxMinSdRequestsPerRendezvous[1]}" /></td>
		<td><jstl:out value="${avgMaxMinSdRequestsPerRendezvous[2]}" /></td>
	</tr>
</table>

<br>
<h2><spring:message code="dashboard.10RendezvousesOrderedByUsers" /></h2>

<spring:message code="dashboard.table.rendezvous.moment" var="moment" />

<display:table id="row" name="10RendezvousesOrderedByUsers" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="name" titleKey="dashboard.table.rendezvous.title" sortable="true" />
	<display:column property="description" titleKey="dashboard.table.rendezvous.description" sortable="true" />
	<spring:message code="dashboard.dateTimeFormat" var = "dateTimePattern" />
	<spring:message code="dashboard.dateFormat" var = "datePattern" />
	<display:column property="moment" titleKey="dashboard.table.rendezvous.moment" format="{0,date,${dateTimePattern}}" sortable="true" /> 
</display:table>

<br>


<h2><spring:message code="dashboard.rendezvousWithOver75percentAnnoucements" /></h2>

<display:table id="row" name="rendezvousWithOver75percentAnnoucements" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="name" titleKey="dashboard.table.rendezvous.title" sortable="true" />
	<display:column property="description" titleKey="dashboard.table.rendezvous.description" sortable="true" />
	<display:column property="moment" titleKey="dashboard.table.rendezvous.moment" format="{0,date,${dateTimePattern}}" sortable="true" /> 
</display:table>

<br>

<h2><spring:message code="dashboard.rendezvousLinkedOver10percentRendezvouses" /></h2>

<display:table id="row" name="rendezvousLinkedOver10percentRendezvouses" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="name" titleKey="dashboard.table.rendezvous.title" sortable="true" />
	<display:column property="description" titleKey="dashboard.table.rendezvous.description" sortable="true" />
	<display:column property="moment" titleKey="dashboard.table.rendezvous.moment" format="{0,date,${dateTimePattern}}" sortable="true" /> 
</display:table>

<br>

<h2><spring:message code="dashboard.bestSellingServices" /></h2>

<display:table id="row" name="bestSellingServices" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="name" titleKey="dashboard.table.rendezvous.title" sortable="true" />
	<display:column property="description" titleKey="dashboard.table.rendezvous.description" sortable="true" />
</display:table>

<h2><spring:message code="dashboard.topSellingServices" /></h2>

<display:table id="row" name="topSellingServices" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="name" titleKey="dashboard.table.rendezvous.title" sortable="true" />
	<display:column property="description" titleKey="dashboard.table.rendezvous.description" sortable="true" />
</display:table>

<h2><spring:message code="dashboard.managersWithMoreServicesThanAvg" /></h2>

<display:table id="row" name="managersWithMoreServicesThanAvg" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="name" titleKey="dashboard.table.manager.name" sortable="true" />
	<display:column property="surname" titleKey="dashboard.table.manager.surname" sortable="true" />
	<display:column property="vat" titleKey="dashboard.table.manager.vat" sortable="true" />
	<display:column property="birthday" titleKey="dashboard.table.manager.birthdate" format="{0,date,${datePattern}}" sortable="true" /> 
</display:table>

<h2><spring:message code="dashboard.managersWithMoreServicesCancelled" /></h2>

<display:table id="row" name="managersWithMoreServicesCancelled" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="name" titleKey="dashboard.table.manager.name" sortable="true" />
	<display:column property="surname" titleKey="dashboard.table.manager.surname" sortable="true" />
	<display:column property="vat" titleKey="dashboard.table.manager.vat" sortable="true" />
	<display:column property="birthday" titleKey="dashboard.table.manager.birthdate" format="{0,date,${datePattern}}" sortable="true" /> 
</display:table>

<br>
