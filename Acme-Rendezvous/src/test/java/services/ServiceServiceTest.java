package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Category;
import domain.Manager;
import domain.Service;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class ServiceServiceTest extends AbstractTest {

	@Autowired
	private ServiceService serviceService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ActorService actorService;

	// ----------------------------------------------------------------------------------------------------------------------

	/*
	 * An actor who is registered as a manager must be able to: 2. Manage his or
	 * her services, which includes listing them, creating them, updating them,
	 * and deleting them as long as they are not required by any rendezvouses.
	 * CREATE
	 */
	@Test
	public void driverCreateWithoutCategory() {
		Object testingData[][] = {
				// Test v�lido: Todos los campos estan rellenos
				{ "manager3", "Repartidor de pizzas",
						"Servicio encargado de repartir las pizzas",
						"https://www.google.es", null },
				// Test v�lido: Picture opcional
				{
						"manager1",
						"Zumo de pi�a",
						"Servicio para repartir zumo de pi�a a los asistentes al Rendezvous",
						"", null },
				// Test inv�lido: Un campo obligatorio (nombre)
				{
						"manager2",
						"",
						"Servicio encargado de gestionar el numero de personas que acuden a las citas",
						"https://www.recordrentacar.com/images/beforefooter/destinos/alquiler-coches-madrid.jpg",
						ConstraintViolationException.class },
				// Test inv�lido: Url inv�lida
				{
						"manager1",
						"Zumo de pi�a",
						"Servicio para repartir zumo de pi�a a los asistentes al Rendezvous",
						"urlInv�lida", ConstraintViolationException.class },
				// Test inv�lido: No es un manager
				{
						"user1",
						"Comedor",
						"Persona encargada del comedor",
						"https://uatae.org/wp-content/uploads/2018/02/madrid.jpg",
						IllegalArgumentException.class },
				// Test inv�lido: Usuario no autenticado
				{
						null,
						"Taxi",
						"Servicio encargado de recoger a las personas para sus citas",
						"http://www.fcbola.com/wp-content/uploads/2015/04/bola_tilcara-600x600.png",
						IllegalArgumentException.class }, };

		for (int i = 0; i < testingData.length; i++) {
			templateCreateWithoutCategory((String) testingData[i][0],
					(String) testingData[i][1], (String) testingData[i][2],
					(String) testingData[i][3], (Class<?>) testingData[i][4]);
		}
	}

	protected void templateCreateWithoutCategory(String username, String name,
			String description, String picture, Class<?> expected) {
		Class<?> caught;
		caught = null;
		Service result;

		try {
			super.authenticate(username);

			result = this.serviceService.create();

			result.setName(name);
			result.setDescription(description);
			result.setPicture(picture);

			serviceService.save(result);
			serviceService.flush();

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	// ----------------------------------------------------------------------------------------------------------------------

	/*
	 * An actor who is registered as a manager must be able to: 2. Manage his or
	 * her services, which includes listing them, creating them, updating them,
	 * and deleting them as long as they are not required by any rendezvouses.
	 * CREATE
	 */
	@Test
	public void driverCreateWithCategory() {
		Object testingData[][] = {
				// Test v�lido: Todos los campos estan rellenos
				{ "manager3", "Repartidor de pizzas",
						"Servicio encargado de repartir las pizzas",
						"https://www.google.es", "category1", null },
				// Test v�lido: Picture opcional
				{
						"manager1",
						"Zumo de pi�a",
						"Servicio para repartir zumo de pi�a a los asistentes al Rendezvous",
						"", "category2", null },
				// Test inv�lido: Un campo obligatorio (nombre)
				{
						"manager2",
						"",
						"Servicio encargado de gestionar el numero de personas que acuden a las citas",
						"https://www.recordrentacar.com/images/beforefooter/destinos/alquiler-coches-madrid.jpg",
						"category1", ConstraintViolationException.class },
				// Test inv�lido: Url inv�lida
				{
						"manager1",
						"Zumo de pi�a",
						"Servicio para repartir zumo de pi�a a los asistentes al Rendezvous",
						"urlInv�lida", "category1",
						ConstraintViolationException.class },
				// Test inv�lido: No es un manager
				{
						"user1",
						"Comedor",
						"Persona encargada del comedor",
						"https://uatae.org/wp-content/uploads/2018/02/madrid.jpg",
						"category1", IllegalArgumentException.class },
				// Test inv�lido: Usuario no autenticado
				{
						null,
						"Taxi",
						"Servicio encargado de recoger a las personas para sus citas",
						"http://www.fcbola.com/wp-content/uploads/2015/04/bola_tilcara-600x600.png",
						"category1", IllegalArgumentException.class }, };

		for (int i = 0; i < testingData.length; i++) {
			templateCreateWithCategory((String) testingData[i][0],
					(String) testingData[i][1], (String) testingData[i][2],
					(String) testingData[i][3], (String) testingData[i][4],
					(Class<?>) testingData[i][5]);
		}
	}

	protected void templateCreateWithCategory(String username, String name,
			String description, String picture, String categoryBeanId,
			Class<?> expected) {
		Class<?> caught;
		caught = null;
		Service result;
		Collection<Category> categories;
		int categoryId;

		try {
			super.authenticate(username);

			result = this.serviceService.create();
			categoryId = this.getEntityId(categoryBeanId);

			categories = new ArrayList<Category>();
			categories.add(this.categoryService.findOne(categoryId));

			result.setName(name);
			result.setDescription(description);
			result.setPicture(picture);
			result.setCategories(categories);

			serviceService.save(result);
			serviceService.flush();

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	// ----------------------------------------------------------------------------------------------------------------------

	/*
	 * An actor who is registered as a manager must be able to: 2. Manage his or
	 * her services, which includes listing them, creating them, updating them,
	 * and deleting them as long as they are not required by any rendezvouses.
	 * EDIT
	 */

	@Test
	public void driverEdit() {

		Object testingData[][] = {
				// Test v�lido:
				{
						"manager1",
						"service1",
						"Repartidor de pizzas",
						"Servicio encargado en repartir las pizzas, version 2",
						"http://www.fcbola.com/wp-content/uploads/2015/04/bola_tilcara-600x600.png",
						null },
				// Test v�lido: Picture opcional
				{ "manager1", "service1", "Repartidor de pizzas",
						"Servicio encargado en repartir las pizzas, version 2",
						"", null },
				// Test inv�lido: El servicio no es del manager 3
				{
						"manager3",
						"service3",
						"Repartidor de pizzas",
						"Servicio encargado en repartir las pizzas, version 2",
						"http://www.fcbola.com/wp-content/uploads/2015/04/bola_tilcara-600x600.png",
						IllegalArgumentException.class },
				// Test inv�lido: El nombre esta vac�a
				{
						"manager1",
						"service1",
						"",
						"Nueva descripcion para servicio 1",
						"http://www.fcbola.com/wp-content/uploads/2015/04/bola_tilcara-600x600.png",
						ConstraintViolationException.class },
				// Test inv�lido: La descripcion esta vac�a
				{
						"manager1",
						"service1",
						"service1",
						"",
						"http://www.fcbola.com/wp-content/uploads/2015/04/bola_tilcara-600x600.png",
						ConstraintViolationException.class },
				// Test inv�lido: Un admin no puede editar
				{
						"admin",
						"service1",
						"service1",
						"Nueva descripcion para servicio 1",
						"http://www.fcbola.com/wp-content/uploads/2015/04/bola_tilcara-600x600.png",
						IllegalArgumentException.class },
				// Test inv�lido: Usuario no autenticado
				{
						null,
						"service1",
						"service1",
						"Nueva descripcion para servicio 1",
						"http://www.fcbola.com/wp-content/uploads/2015/04/bola_tilcara-600x600.png",
						IllegalArgumentException.class }, };

		for (int i = 0; i < testingData.length; i++) {
			templateEdit((String) testingData[i][0],
					(String) testingData[i][1], (String) testingData[i][2],
					(String) testingData[i][3], (String) testingData[i][4],
					(Class<?>) testingData[i][5]);
		}
	}

	protected void templateEdit(String username, String serviceIdBean,
			String name, String description, String picture, Class<?> expected) {
		Class<?> caught;
		Service service;
		int serviceId;
		caught = null;

		try {
			super.authenticate(username);

			serviceId = super.getEntityId(serviceIdBean);
			service = this.serviceService.findOne(serviceId);

			service.setName(name);
			service.setDescription(description);
			service.setPicture(picture);

			serviceService.save(service);
			serviceService.flush();

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	// ----------------------------------------------------------------------------------------------------------------------

	/*
	 * An actor who is registered as a manager must be able to: 2. Manage his or
	 * her services, which includes listing them, creating them, updating them,
	 * and deleting them as long as they are not required by any rendezvouses.
	 * DELETE
	 */
	@Test
	public void driverDelete() {

		Object testingData[][] = {
				// Test v�lido: Borrado servicio cuando no tiene rendezvous
				// asociado
				{ "manager2", "service3", null },
				// Test inv�lido: Servicio con rendezvous asociado
				{ "manager1", "service1", IllegalArgumentException.class },
				// Test inv�lido: Intentando borrar servicio 3 con manager que
				// no existe
				{ "manager1212", "service3", IllegalArgumentException.class },
				// Test inv�lido: Admin intentando elimar un servicio
				{ "admin", "service1", IllegalArgumentException.class },
				// Test inv�lido: Manager intentando eliminar servicio que no
				// existe
				{ "manager1", "service2123", NumberFormatException.class }, };

		for (int i = 0; i < testingData.length; i++) {
			templateDelete((String) testingData[i][0],
					(String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void templateDelete(String username, String serviceIdBean,
			Class<?> expected) {
		Class<?> caught;
		Service service;
		int serviceId;
		caught = null;

		try {
			super.authenticate(username);

			serviceId = super.getEntityId(serviceIdBean);
			service = this.serviceService.findOne(serviceId);

			serviceService.delete(service);

			serviceService.flush();

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	// ----------------------------------------------------------------------------------------------------------------------

	/*
	 * An actor who is authenticated as an administrator must be able to: 1.
	 * Cancel a service that he or she finds inappropriate. Such services cannot
	 * be re-quested for any rendezvous. They must be flagged appropriately when
	 * listed. CANCEL
	 */
	@Test
	public void driverCancel() {

		Object testingData[][] = {
				// Test v�lido:
				{ "admin", "service1", null },
				// Test inv�lido: Intentando cancelar un servicio que ya est�
				// cancelado
				{ "admin", "service2", IllegalArgumentException.class },
				// Test inv�lido: User no puede cancelar
				{ "manager3", "service1", IllegalArgumentException.class },
				// Test inv�lido: Solo el admin puede cancelar servicios
				{ "user1", "service1", IllegalArgumentException.class },
				// Test inv�lido: Intentando cancelar un servicio que no existe
				{ "admin", "service121312312312", NumberFormatException.class }, };

		for (int i = 0; i < testingData.length; i++) {
			templateCancel((String) testingData[i][0],
					(String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void templateCancel(String username, String serviceIdBean,
			Class<?> expected) {
		Class<?> caught;
		Service service;
		int serviceId;
		caught = null;

		try {
			super.authenticate(username);

			serviceId = super.getEntityId(serviceIdBean);
			service = this.serviceService.findOne(serviceId);

			this.serviceService.cancel(service);

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * 4. An actor who is authenticated as a user must be able to: 2. List the
	 * services that are available in the system.
	 */
	
	/*
	 * 5. An actor who is registered as a manager must be able to: 1. List the
	 * services that are available in the system.
	 */

	@Test
	public void driverListServicesSystem() {

		Object testingData[][] = {

				// Test v�lido:
				{ "user2", "service1", null },
				// Test v�lido:
				{ "manager1", "service2", null },
				// Test inv�lido:
				{ "admin", "service2", IllegalArgumentException.class },

		};

		for (int i = 0; i < testingData.length; i++) {
			templateListServicesSystem((String) testingData[i][0],
					(String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void templateListServicesSystem(String username,
			String serviceBean, Class<?> expected) {
		Class<?> caught;
		Service service;
		int serviceId;
		
		Actor actor;
		int actorId;
		
		caught = null;

		try {
			super.authenticate(username);
			
			actorId = this.getEntityId(username);
			actor = this.actorService.findOne(actorId);
			
			
			serviceId = this.getEntityId(serviceBean);
			service = this.serviceService.findOne(serviceId);
			//Comprobamos que el usuario logeado sea user
			Assert.isTrue(actor instanceof User || actor instanceof Manager);
			
			Assert.isTrue(this.serviceService.findAll().contains(service) && this.serviceService.findAll().size() ==3);
			
			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

}
